# Introduction to ARPA2 LETS

> *Local Exchange Trading Systems, or LETSystems for short, are about
> communities that have a measure for the internal sharing of wealth.
> Wealth as in, human effort being applied to benefit others in the
> community.  Domains often form a community, and so ARPA2 facilitates
> LETSystems under every domain.*

Central in a LETSystem is an agreed-upon currency.  Or several, if you
like.  For instance, a representation for an hour of work or a unit
that is equivalent to the national currency, but retained in the local
system.

The idea is, first and foremost, to concentrate wealth inside the
community, rather than letting it seep out.  The result of such
schemes is a motivation to engage in this community.  To share what
is easily given, and receive what is difficult to obtain.  Since value
assessment is highly personal, it is possible for everyone to benefit,
and continue to want to give because what comes back is so much more —
for everyone.  The more diverse a community, the more likely this
is to work.  This is true economic growth; not growth in terms of currency
amount, but in terms of work getting done and benefits being spread.

In a LETSystem, money is intentionally not scarce.  Everyone can create
money, and this will simply lower their balance.  The result is that the
"bottom line" disappears; there is no special meaning to a zero balance,
and no urge to stop spending there; this is even a functional requirement,
because about half of the balances must sink below zero to allow the
other balances to rise above zero.  As long as trading continues,
this can change all the time.  It is normal community care in a LETSystem
to avoid that anyone drifts too far off in either direction.

In InternetWide LETS, we eradicate the zero completely by not showing
absolute balances; instead, we present others in the community with their disbalance
relative to you.  The term disbalance is not meant to be judgemental;
It is something that can change at any time, and it helps you
estimate which people are most prone to do some communal work because
they already benefited, as well as to understand that someone asking for
your help is reasonably doing so, and may be trying to pull you back
into the middle.  This works to self-regulate the balances to not
drift too far apart.  Such drifting-apart is not so much a problem
because of the plentyful currency, but it means that someone may be
loosing out and could use some help to re-integrate into the community.

[Read more about LETSystem Design](https://manual.letsa.net/)
or
[Watch Explanation Videos](https://www.youtube.com/watch?v=_vPobjqA1yE).


## ARPA2 and Accounting

Every user has a login identity, and this is used to account their
balance under.  Aliases and pseudonyms will have the same balance,
and so does a group membership.  (This however, is a privacy concern;
we may well solve that later, using some part of the balance to move
between the alternative identities.)

Transactions are zero-sum games as in any other trading system.  The
difference is that payments become appreciations, and they are not
rejected when they drop the balance below zero.  In our system there
is not even a visible zero, because all value is relative —
depicting how much a user has brought into the community, relative
to the one who is looking.  Einstein would have loved it.

**Einsteinium.**
We celebrate the relative nature of our currency with a default
currency `XES`, modelled after `XAU` for gold Au and `XAG` for
silver Ag.  Following this pattern, `XES` is the element einsteinium Es.
You may pronounce it as "ein Stein", which is German for "one
stone".  Multiple units would be "Steine" or "stones".  We use
it as a prefix to an integer amount, as in `XES 123`, and prefer
to refrain from commas and dots to separate thousands or millions.
(Such amounts may in fact be too high to be useful).  Negative
values are printed as `XES -456`.  The space included in these
formats is ideally a non-breakable space, so `&nbsp;` in HTML
or the code point U+00A0 in Unicode, represented as 0xC2 0xA0
in UTF-8.  The string `"XES\xC2\xA0%d"` could be used for
POSIX-style formatted printing.


## Border Patrol

The currency must not leave the domain, that is an essential choice
in LETSystems.  As a result, there are no exchanges, though there is nothing
wrong with people organising an exchange by cancelling out differences
in two currencies (possibly under differing domains), if they agree
on a mutually beneficial trade that they are both happy to engage
into.  But it is not organised as a financial service, not an
intrinsic part of the system.

It would be awkward to have plentyful money that can leave a system,
because that could end up dictating others.  Before you know it,
you'd have a rat race of inflation, where each LETSystem tries to
generate as much currency as it can to exchange to other LETSystems
where it would buy goods and services.  (That problem also occurs in
government-issued currencies, especially in times of crisis.)

Boundless currency generation in a sacrificed community may make a user
look more communally active than is really the case.  The self-regulation
would not work in the sacrificed community if all the currency leaves
it; and so, currency needs to remain local, where peers can review
others and choose their peers accordingly.  This reasonable logic would
still be applicable between two people decide to cancel out the unbalance in
one currency against an opposite unbalance in another currency.

A side-effect of this "border patrol" is that many domains can use the
same currency name; its meaning will be local to the domain.  So it
is not right to assume that `XES` means the same thing everywhere;
it has a domain-local meaning.  We could write an longer identity for
a currency in the ARPA2 Identity style to show this clearly, such as
`+lets+xes@orvelte.nep` or `+lets+xes@elsewhere.dom` which is
obviously not the same.


## InternetWide LETS uses Realm Crossover

A central idea of the InternetWide Architecture, for which ARPA2 software
gives implementations, is that users can authenticate everywhere with an
identity managed under their own domain.  The relation with Border Patrol
needs some explanation.

Currencies and their balances are confined to a domain, but that does not
mean that users cannot be spread across the Internet.  Any accounting should
be founded on balances, not users.  (Given some defense against abuses, like
denial-of-service, where one user holds numerous accounts.)  Balances do have
a user identity attached to them, but these might be local or remote.

It is always possible to have a local alias under a domain for a remote
user identity.  So, one might authenticate as `john@example.net` and choose
to represent as `john@example.com` under the `example.com` domain, and then
use the `XES` currency of the `example.com` domain.

Technically, Realm Crossover technology is needed to accommodate this.  The
remote user identity is validated with a callback from the relying domain
(`example.com` in the example) to the user domain (`example.net` in the example),
and there are a few ways of doing this, all worked out in the principles of
[InternetWide Realm Crossover](https://datatracker.ietf.org/doc/html/draft-vanrein-internetwide-realm-crossover).


## Money, Value and Ethics

Money has no value of its own.  Any focus of people on obtaining fiat money
is the result of its (deliberately caused) scarcity.  This is why people
end up denying each other the access to money that they themselves
eagerly strive for.  Scarcity makes us competetitive instead of merely
collaborative.

What money is really meant to do, is communicate.  Money you hold
tells us that someone paid you (their respect for services rendered).
Spending that money lets you in turn pay your respect to others.
Acrued money is simply an intermediate value that sites between
having been useful and making use of others; and in a LETSystem,
the negative value communicates services used by others without
having given back yet.

One might say that in such a system, accumulated money is a statue
of respectfulness.  But this is only true if it were obtained
ethically.  This is why our LETS system does not include the
typical signs of financial exploitation, namely inflation and interest.
These are the mechanisms by which fiat currencies traditionally cause
a growing gap between rich and poor, resulting in accumulation of
money which is no longer an indication of respectful trading.

Government-issued money is also called "fiat currency", which is
an interesting pun on "fiat lux", which means "there be light".
It is a hint that fiat money is created at a whim.  But unlike
in a LETSystem, not everyone can do this; only those who
control the money printing press and receive what comes off of it
can benefit from extra money being printed.  This is how inflation
drives increasing disparity between rich and poor.  Interest is
a similar pump to drive money from poor to rich.

Both inflation and interest cause money to flow upstream, from
poor to rich people.  Interest is difficult to ban, until a
system is freed from inflation.  The normal reasoning is that
interest puts a price on money, but this line of thought relies
on the idea that money has a value of its own.  LETSystems break
with that idea by allowing anyone to create currency without
a need for interest.

One might say that a system where everyone creates their own
currency has inflation everywhere.  The big difference is that
anyone who creates currency is visibly doing this.  There is a
mechanism of community self-control to combat problems caused
by this effect.

Not unethical, but still a possible hurdle to users of LETSystems,
is a fee for system maintenance or hosting.  By incorporating the
LETSystem in an already-existing hosting environment, we can
ignore the slight extra effort needed for the CPUs.  Also note
that the currencies are bound to stay relative small, and not
likely to grow to megalamaniacal sizes.


## Money is Communciation

The purpose of money is an abstract connector between supply and
demand.  Its benefit over barter trading is that it can keep those
two pieces separated in time and space, and balance the two halves
with money as an intermediate.
Due to this, it is not necessary to swap goods or services with
the same person, but rather it would be possible to do each with
another.

In the ARPA2 framework, we have a mechanism for Access Control
dedicated to Communication Access.  This is independent of the
protocol used; it might be for email, chat, telephony or, as in
this case, the passing of money.  I suppose there is no stronger
way to express money as a means of communication, without use
of its own; the ARPA2 LETSystem treats it just like passing data.

As part of our Communication Access system, we sometimes show
a role to another party; namely, when speaking to another member
of a shared group, we generally assume our group member name;
in other cases, we may prefer to use an alias or pseudonym.
This shows the identity that we choose to use towards the other
party, but the balance may be shared between multiple identities.
From the perspective of the group, there has just been an trade
inside the group, and that is also how it appears in our list of
transactions.


## Inactivity Periods

User balances may be inactive for a prolonged period.  This may be an
explicit choice, or a reflection by other balance users, or detected
in software.  In any such situation, it is possible to render a user's
balance inactive, so it does not have an impact on the balances of
others.  At any later time it is possible to reactivate the user's
balance; this may or may not involve administrative action.

Inactive balances make it less painful if people consume without
giving back, and then leave the system.  This is not detrimental
in itself, given a basic willingness to share and invest in a
community; but when new balances start at zero, they would face
active users with a much higher (average) balance.  This may feel
daunting.  By setting the active balances aside from those who were
inactivated, it is possible to compute an average for those active
balances, and use that to prime a new balance.

When balances are first activated, they should enter at the average
level of other active balances.  While inactive, the divergence of
a balance from the average is stabilised, and for new users this
divergence will be set to 0.  When a new or inactive balance is activated,
it is increased with the average active balance for the currency;
when a balance is inactivated, it is decreased with the average
active balance for the currency; during both operations, the balance
that flips between active and inactive is not incorporated into the
average.

Now consider a user who deactivates.  Their departure makes their
balance invisible.  Others might leave and enter, affecting what
constitutes the average active balance.  When an earlier inactive
user is re-activated, their balance has effectively followed the
active average at that time.  On average, they would experience
the same differences to active users as before, and they too
would appear at the same difference from active users.  There is
no change to the relations between active users, except as caused
by the trades that occurred between those active users during the
inactive period.

Also consider a new account.  This is created inactive, with zero
balances.  This allows the user the choice when to become active
in a currency for their domain.  Once they activate, their balance
is set somewhere in the middle of the other active balances, no
matter when they choose to get active.  This also means that no
disparity arises between older and newer users.  All new users
start off on a basis of fairness and equality.

Systems that prefer users to deactivate their balances after
departing may find it useful to limit the posting of trading messages
to users with active balances.  This would present a mild incentive to
deactivate any balance that feels less appropriate to keep active.


## Structures

The software in this repository consists of a few basic elements:

  * SQLite3 database to hold domains, users, currencies, balances,
    transactions.  When privacy or other reasons demands it,
    transactions can be removed because the actual value lies in
    the balances.

  * FastCGI program to query and update the database.  Output is
    subjected to ARPA2 Communication Access Control, which may involve
    renaming identities to group member names, aliases or pseudonyms.

    These programs may also be started outside of the FastCGI
    framework, in which case they work under old-school CGI-BIN.
    When run without `.fcgi` extension, the program operates in
    shell command mode.

  * HTML and JavaScript pages with a rudimentary interface.  The
    API to the FastCGI scripts can also be used in custom scripting.

  * HTTP-SASL or other authentication is assumed to be done in
    the webserver calling the FastCGI scripts to enforce a few
    of their contextual aspects.  At the very least, this covers
    an authenticated `REMOTE_USER` and a configured `ARPA2_DOMAIN`
    to work under.  There may be an `ARPA2_LETS_CURRENCY`, perhaps
    derived from a path name.  Other parameters are passed in as
    parameters to a GET or POST query.

    When HTTP-SASL is used with Realm Crossover, it is possible for
    remote users to authenticate.  It then depends on Access Control
    settings what abilities such users have; the ARPA2 framework is
    able to grant remote users rights similar to local users, either
    in the form of a generic pattern or individual setting.

These components form a basic LETSystem that can enrich every domain
with a community-building token money system.  Use it wisely, and be
sure to enjoy the leading documentation of Michael Linton's
[LETSystem Design Manual](https://manual.letsa.net/) and
[Explanatory Video's](https://www.youtube.com/watch?v=_vPobjqA1yE).


## Copyright Notice

```
SPDX-License-Identifier: AGPL-3.0-or-later
SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
```
