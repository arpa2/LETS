-- Setup test data in the database
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
-- SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl


INSERT INTO dommap (domain) VALUES ('example.com');
INSERT INTO dommap (domain) VALUES ('example.net');
INSERT INTO curmap (domid,currency,format) VALUES (1,'COM', 'COM %d'),(2,'KOM', 'KOM %d');
INSERT INTO curmap (domid,currency,format) VALUES (2,'NET', '%d.NET'),(2,'KOM', '%d.KOM');
INSERT INTO idmap VALUES (1,1, 'johann', 'cook+john');
INSERT INTO idmap VALUES (2,1, 'marietje', 'cook+maria');
INSERT INTO idmap VALUES (3,2, 'johannes', 'john');
INSERT INTO idmap VALUES (4,2, 'johannes', 'johanna');
INSERT INTO history VALUES (1,1,1,2,123,1681943821,'Tikkie vooruit');
INSERT INTO history VALUES (2,1,2,1,100,1681943827,'Tikkie terug');
INSERT INTO balance VALUES (1,1,-23,'Y');
INSERT INTO balance VALUES (1,2,23,'Y');
