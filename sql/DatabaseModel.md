# Database Model for ARPA2 LETS

## Table structure

### Domain map

The `dommap` table is a lookup structure that translates a domain name into a `domid` integer value.
It is independent of any currencies.

In case of internationalisation, the `domain` must be represented in UTF-8, rather than the DNS-specific notation (punycode).


### Currency Map

The `curmap` table assignes a `curid` integer value to a named `currency` under a `domid`-identified domai.
This means that a currency name such as `XES` may occur under different domains, and would then translate to different `curid` values.
It also holds a `format` string that includes `%d` for the integer amount of a currency; an example format string would be `"XES %d"` to display values such as `XES 123`; an alternative format could be `%d Stein` for values such as `1 Stein`.


### User Map

The `usrmap` table assigns a `usrid` integer value to a `username` in UTF-8 under a `domid`-identified domain.
This translation is made specifically for a currency `curid`, and therefore points to a `balid`-identified balance that must reside under the same currency.

Multiple usernames may share the same balance, also for the same currency; this allows users to show up in multiple places with the same balance; this is not enforced, so one logged-in user may have separate balances for separate `username` values, but not for the same `username`.
Finally, users may subscribe to any number of currencies `curid` they like.


### Balance

This models current balances, by way of `balid` numbered account.
Balances are always created for one `curid` currency that must not change over time.
It may be identitied in a few special places in the `lets+CUR+BALID@DOMAIN.NAME` format.

The balance can exist independently of user identities that may or may not (or no longer) be connected to it.
Users have one `balid` at any time.

Balances are created at a zero level and must not be removed without zeroing them first.
The enforcement of this rule avoids a non-zero total balance for any currency.
Zeroing may be accomplished with a booking from one or more other accounts.
Currencies with a total balance at zero can always remove their last balances, because they can always be reduced to zero by such transfers.

The `activity` character for a balance has the following meaning:

  * `Y` is short for "yes, active"
  * `N` is short for "no, inactive"
  * `B` is short for "blocked by admin"

Note that this setting is per-user and also per-currency.


### History

The `history` table stores transactions from the past that have led to the current balances.
It depends on local policy whether all historic transactions are kept, or just a subset.
The balances are leading and history may be added to deduce past balances, but when it has been cut short it would not go all the way back to zero.


### Requests

The `request` table models offered work, and may include payment requests.
It is meant to make it easy for a user to select an offer as a prepared transaction.
It may be answered to with a concrete transaction.

The `reqid` identifies a request, and can be added as a reference in the `history` of transactions.
For payment requests, the `reqid` can be useful for reconciliation.
For generic offers, the `reqid` can be useful for grouping into products.
Note however, that the use of the `reqid` is not enforced by ARPA2 LETS; an application built around it may however be more stringent about it.

The `prod_usrid` indicates the intended recipient, so the party offering.

The `cons_usrid` may be set to a particular consumer, but it may also be NULL to make an offer open to anyone.

The `offer` character is a flag:

  * `Y` is short for "yes, on offer" (to the `cons_usrid` or, if that is NULL, to all)
  * `N` is short for "no, not currently being offered"
  * `B` is short for "blocked by admin"
  * `C` is short for "closed", usually by the requesting party or an admin.


## Copyright Notice

```
SPDX-License-Identifier: AGPL-3.0-or-later
SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
```
