-- Upgrade the database from v0.8 to v0.9 scheme.
--
-- Please run this in one transaction, so failure means nothing is
-- done.  Make sure to backup the database before running this code.
--
-- Old tables are left behind marked _v0_8 and these may be removed.
--
--   * Add  a   reqid      attribute to   history
--   * Add  a   cons_balid attribute to   history
--   * Add  a   prod_balid attribute to   history
--   * Set  the AUTOINCREMENT   flag on   usrmap
--   * Drop the admin      attribute from curmap (never used)
--   * Drop the activity   attribute from curmap (never used)
--   * Create a request table
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
-- SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl


ALTER TABLE curmap RENAME TO curmap_v0_8;

CREATE TABLE curmap (
	curid INTEGER NOT NULL,
	domid INTEGER NOT NULL,
	currency VARCHAR(32) NOT NULL,
	format VARCHAR(32) NOT NULL,
	PRIMARY KEY (curid));

INSERT INTO curmap
SELECT curid,domid,currency,format
FROM curmap_v0_8;


ALTER TABLE usrmap RENAME TO usrmap_v0_8;

CREATE TABLE IF NOT EXISTS usrmap (
        usrid INTEGER PRIMARY KEY AUTOINCREMENT,
        balid INTEGER NOT NULL,
        domid INTEGER NOT NULL,
        username VARCHAR(64) NOT NULL);

INSERT INTO usrmap
SELECT usrid,balid,domid,username
FROM usrmap_v0_8;


CREATE TABLE request (
	reqid INTEGER NOT NULL,
	prod_usrid INTEGER NOT NULL,
	cons_usrid INTEGER,
	offer CHAR NOT NULL,
	curid INTEGER NOT NULL,
	value INTEGER NOT NULL,
	descr VARCHAR(250) NOT NULL,
	PRIMARY KEY (reqid,prod_usrid));

ALTER TABLE history RENAME TO history_v0_8;

CREATE TABLE IF NOT EXISTS history (
	txnid INTEGER NOT NULL,
	curid INTEGER NOT NULL,
	cons_balid INTEGER NOT NULL,
	prod_balid INTEGER NOT NULL,
	cons_usrid INTEGER NOT NULL,
	prod_usrid INTEGER NOT NULL,
	value INTEGER NOT NULL,
	unixtime INTEGER NOT NULL,
	descr VARCHAR(250) NOT NULL,
	reqid INTEGER DEFAULT NULL,
	PRIMARY KEY (txnid));

INSERT INTO history
SELECT h.txnid, h.curid, IFNULL(c.balid,-1), IFNULL(p.balid,-1), h.cons_usrid, h.prod_usrid, h.value, h.unixtime, h.descr, NULL
FROM history_v0_8 h, usrmap c, usrmap p
WHERE c.usrid = h.cons_usrid
AND   p.usrid = h.prod_usrid;

CREATE UNIQUE INDEX IF NOT EXISTS dommap_domain
ON dommap (domain);

CREATE UNIQUE INDEX IF NOT EXISTS curmap_currency
ON curmap (domid, currency);

CREATE INDEX IF NOT EXISTS usrmap_balid
ON usrmap (balid);

CREATE UNIQUE INDEX IF NOT EXISTS usrmap_username
ON usrmap (domid, username);

