-- Upgrade the database from v0.9 to v0.10 scheme.
--
-- Please run this in one transaction, so failure means nothing is
-- done.  Make sure to backup the database before running this code.
--
-- Old tables are left behind marked _v0_9 and these may be removed.
--
--   * Add an access attribute to dommap
--   * Add an access attribute to curmap
--   * Add an access attribute to balance
--   * NULL access in dommap  shall mean '%OADCF admin@${DOMAIN} %C @${DOMAIN}'
--   * NULL access in curmap  shall mean '%ODCXAF admin@${DOMAIN} %C @${DOMAIN}'
--   * NULL access in balance shall mean '%ODFRW admin@${DOMAIN} %C @${DOMAIN}'
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
-- SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl


ALTER TABLE dommap  ADD COLUMN access TEXT;
ALTER TABLE curmap  ADD COLUMN access TEXT;
ALTER TABLE balance ADD COLUMN access TEXT;

