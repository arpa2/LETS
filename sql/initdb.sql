-- Initialise the database
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
-- SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl

CREATE TABLE IF NOT EXISTS dommap (
	domid INTEGER NOT NULL,
	domain VARCHAR(255) NOT NULL,
	access TEXT,
	PRIMARY KEY (domid));

CREATE TABLE IF NOT EXISTS curmap (
	curid INTEGER NOT NULL,
	domid INTEGER NOT NULL,
	currency VARCHAR(32) NOT NULL,
	format VARCHAR(32) NOT NULL,
	access TEXT,
	PRIMARY KEY (curid));

CREATE TABLE IF NOT EXISTS usrmap (
        usrid INTEGER PRIMARY KEY AUTOINCREMENT,
        balid INTEGER NOT NULL,
        domid INTEGER NOT NULL,
        username VARCHAR(64) NOT NULL);

CREATE TABLE IF NOT EXISTS balance (
        balid INTEGER NOT NULL,
	curid INTEGER NOT NULL,
	balance INTEGER NOT NULL,
	activity CHAR DEFAULT 'N',
	access TEXT,
        PRIMARY KEY (balid));

CREATE TABLE IF NOT EXISTS history (
	txnid INTEGER NOT NULL,
	curid INTEGER NOT NULL,
	cons_balid INTEGER NOT NULL,
	prod_balid INTEGER NOT NULL,
	cons_usrid INTEGER NOT NULL,
	prod_usrid INTEGER NOT NULL,
	value INTEGER NOT NULL,
	unixtime INTEGER NOT NULL,
	descr VARCHAR(250) NOT NULL,
	reqid INTEGER DEFAULT NULL,
	PRIMARY KEY (txnid));

CREATE TABLE IF NOT EXISTS request (
	reqid INTEGER NOT NULL,
	prod_usrid INTEGER NOT NULL,
	cons_usrid INTEGER,
	offer CHAR NOT NULL,
	curid INTEGER NOT NULL,
	value INTEGER NOT NULL,
	descr VARCHAR(250) NOT NULL,
	PRIMARY KEY (reqid,prod_usrid,curid));

CREATE UNIQUE INDEX IF NOT EXISTS dommap_domain
ON dommap (domain);

CREATE UNIQUE INDEX IF NOT EXISTS curmap_currency
ON curmap (domid, currency);

CREATE INDEX IF NOT EXISTS usrmap_balid
ON usrmap (balid);

CREATE UNIQUE INDEX IF NOT EXISTS usrmap_username
ON usrmap (domid, username);

