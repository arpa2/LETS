-- Upgrade the database from v0.7 to v0.8 format in one transaction.
--
-- Old tables are left behind marked _v0_7 and these may be removed.
-- Please run this in one transaction, so failure means nothing is
-- done.  Make sure to backup the database before running this code.
--
--   * Stop using idmap, name it usrmap
--   * Restructure balance with its own balid
--   * Not princ_id and actor_id, but just username
--   * Balances have their own balid, no longer usrid
--   * Users have a unique usrid and can share a balid
--   * Initial balid values are just the usrid
--   * History keep usrid, so username, not balid
-    * History should 1/2 work after usrid removal
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
-- SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl


ALTER TABLE balance RENAME TO balance_v0_7;

CREATE TABLE balance (
	balid INTEGER NOT NULL,
	curid INTEGER NOT NULL,
	balance INTEGER NOT NULL,
	activity CHAR DEFAULT 'N',
	PRIMARY KEY (balid));

INSERT INTO balance
SELECT usrid,curid,balance,activity
FROM balance_v0_7;


ALTER TABLE idmap RENAME TO idmap_v0_7;

CREATE TABLE usrmap (
	usrid INTEGER NOT NULL,
	balid INTEGER NOT NULL,
	domid INTEGER NOT NULL,
	username VARCHAR(64) NOT NULL,
	PRIMARY KEY (usrid));

INSERT INTO usrmap
SELECT usrid,usrid,domid,princ_id
FROM idmap_v0_7;


ALTER TABLE curmap ADD COLUMN activity CHAR DEFAULT 'N';
ALTER TABLE curmap ADD COLUMN admin VARCHAR(255);

