-- Upgrade the database from v0.10 to TODO scheme.
--
-- Please run this in one transaction, so failure means nothing is
-- done.  Make sure to backup the database before running this code.
--
-- Old tables are left behind marked _v0_10 and these may be removed.
--
--   * Add  a   youth      attribute to   balance
--   * Add  a   heat       attribute to   balance
--   * Add  a   lava       attribute to   history
--   * TODO:MAYBE: Add a remote_domid attribute to usrmap
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
-- SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl


ALTER TABLE balance ADD COLUMN youth;	--TODO: What precisely is needed?
ALTER TABLE balance ADD COLUMN heat REAL DEFAULT 0.0;
ALTER TABLE history ADD COLUMN heat REAL;

ALTER TABLE usrmap  ADD COLUMN remote_domid INTEGER;   --TODO: Exceptional, not fun
