#!/bin/ash
#
# Add a domain to the system.
#
# Usage: lets-add-domain.sh example.com
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl


SQLDB=${ARPA2_LETS_DB:-/tmp/LETS.db}

if [ $# -ne 1 -o -z "$1" ]
then
	echo >&2 "Usage: $0 domain.name"
	exit 1
fi

DOMAIN="$1"

sqlite3 "$SQLDB" "BEGIN TRANSACTION; INSERT INTO dommap (domain) VALUES ('$DOMAIN'); INSERT INTO curmap (domid,currency,format) SELECT d.domid, 'XES', 'XES %d' FROM dommap d WHERE d.domain='$DOMAIN'; COMMIT"
