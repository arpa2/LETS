#!/bin/ash
#
# Add a domain currency to the system.
#
# Usage: lets-add-currency.sh example.com XAU
# Usage: lets-add-currency.sh example.com XAU 'XAU %d'
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl


SQLDB=${ARPA2_LETS_DB:-/tmp/LETS.db}

if [ $# -lt 2 -o $# -gt 3 -o -z "$1" ]
then
	echo >&2 "Usage: $0 domain.name XES 'XES %d'"
	exit 1
fi

DOMAIN="$1"
CURNAME="$2"
CURFORM="${3:-$2 %d}"

sqlite3 "$SQLDB" "BEGIN TRANSACTION; INSERT INTO curmap (domid,currency,format) SELECT d.domid, '$CURNAME', '$CURFORM' FROM dommap d WHERE d.domain='$DOMAIN'; COMMIT"
