#!/bin/ash
#
# Create the database if it doesn't exist yet
#
# Usage: lets-have-db.sh
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl


SQLDB=${ARPA2_LETS_DB:-/tmp/LETS.db}

if [ $# -ne 1 -o ! -r "$1" ]
then
	echo >&2 "Usage: $0 initdb.sql"
	exit 1
fi

SCRIPT="$1"

if [ ! -r "$SQLDB" ]
then
	sqlite3 -init "$SCRIPT" "$SQLDB" < /dev/null
fi
