# Management Scripts for ARPA2 LETS

> *Operational scripts for managing a Local Exchange Trading System.*


## Environment Variables

Use `$ARPA2_LETS_DB` as the filename for a SQLite3 database that
holds the domains, currencies, users, balances and payments.
If not set, it defaults to `/tmp/LETS.db`


## Operations

Run `lets-have-db.ash` to ensure the existence of the LETS database.

Run `lets-add-domain.ash domain.name` to add a domain name.

Run `lets-add-currency.ash domain.name CUR` with optionally an extra
argument for a format, like `"CUR %d"`, to add a currency.

Run 'lets-add-balance.ash' domain.name CUR user` with optionally an
extra argument for an actor name, to add a zero balance for a `user`
in the `CUR` currency under `domain.name`.  The balance is inactive,
and upon activation it will be pulled up or down to the average of
existing active accounts (the initial zero balance constitutes a
zero deviation from that moving average).


## Copyright Notice

```
SPDX-License-Identifier: AGPL-3.0-or-later
SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
```
