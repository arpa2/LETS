#!/bin/ash
#
# Add a new domain user or add an actorid to a known user.
# This is separately done for each currency.  When a new
# domain user is added to a currency, its balance is 0 and
# activation is 'N' for inactive; activation is complex and
# left to the FastCGI programs.
#
# Usage: lets-add-balance.sh example.com XAU username
# Usage: lets-add-balance.sh example.com XAU username actorid
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl


SQLDB=${ARPA2_LETS_DB:-/tmp/LETS.db}

if [ $# -lt 3 -o $# -gt 4 -o -z "$1" ]
then
	echo >&2 "Usage: $0 domain.name CUR user [actor+name]"
	exit 1
fi

DOMAIN="$1"
CURRENCY="$2"
USER="$3"
if [ $# -eq 3 ]
then
	ACTOR="NULL"
	#
	# Add a new user/currency combo under the domain.
	#  1. Create a new balance
	#  2. Create a new user for that balance
	#
	sqlite3 "$SQLDB" "BEGIN TRANSACTION; INSERT INTO balance (curid,balance,activity) SELECT c.curid,0,'N' FROM curmap c,dommap d WHERE d.domain='$DOMAIN' and c.currency='$CURRENCY' and d.domid=c.domid; INSERT INTO usrmap (balid,domid,username) SELECT b.balid,d.domid,'$USER' FROM balance b, curmap c, dommap d LEFT JOIN usrmap u ON u.balid=b.balid WHERE d.domid=c.domid AND c.currency='$CURRENCY' AND b.curid=c.curid AND u.username IS NULL; COMMIT"
else
	ACTOR="$4"
	#
	# Extend an existing user/currency combo under the domain.
	#
	#  1. Find the existing balance
	#  2. Create a new usrmap for that balance
	#
	echo "$CURRENCY: $USER@$DOMAIN --> $ACTOR@$DOMAIN"
	sqlite3 "$SQLDB" "BEGIN TRANSACTION; INSERT INTO usrmap (balid,domid,username) SELECT b.balid,d.domid,'$ACTOR' FROM balance b,curmap c,dommap d,usrmap u WHERE d.domid=c.domid AND d.domain='$DOMAIN' AND c.currency='$CURRENCY' AND u.username='$USER' AND u.balid=b.balid; COMMIT"
fi

