# This is ARPA2 LETS, a Local Exchange Trading System to run under every domain
#
# Install the empty.db file, but only if it does not exist yet
#
# Called as: cmake -P InstallEmptyDB.cmake "${CMAKE_BINARY_DIR}" "$ENV{DESTDIR}" "${LETS_DB}"
#
#   SPDX-License-Identifier: AGPL-3.0-or-later
#   SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl

set (CMAKE_BINARY_DIR "${CMAKE_ARGV3}")
set (DESTDIR          "${CMAKE_ARGV4}")
set (LETS_DB          "${CMAKE_ARGV5}")

if (NOT EXISTS "${DESTDIR}/${LETS_DB}")
	get_filename_component (LETS_DB_DIR  "${LETS_DB}" DIRECTORY)
	get_filename_component (LETS_DB_FILE "${LETS_DB}" NAME     )
	file (INSTALL
		"${CMAKE_BINARY_DIR}/empty.db"
		RENAME "${LETS_DB_FILE}"
		DESTINATION "${LETS_DB_DIR}"
	)
else ()
	message (STATUS "Keeping: ${DESTDIR}${LETS_DB} (because it may hold existing data)")
endif ()
