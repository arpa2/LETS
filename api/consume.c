/* consume.c -- Switching code for the API to ARPA2 LETS.
 *
 * Consumer Operations:
 *
 * a2lets consume new|add|sub domain a2domain
 *	currency currency ( balance balid | owner username )
 *	other username [ request reqid ]
 *	value tokens [ descr comment ]
 *
 * a2lets consume del|get domain a2domain
 *	currency currency ( balance balid | owner username )
 *	other username request reqid
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 * SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <time.h>

#include <sqlite3.h>

#include <arpa2/identity.h>
#include <arpa2/access.h>
#include <arpa2/util/cmdparse.h>

#include "shared.h"
#include "switch.h"



bool do_consume (sqlite3 *db, opcode_t opcode, struct cmdparser *prs) {
	bool ok = true;
	//
	// Static storage for prepared SQL
	static struct prepsql ps_new, ps_add, ps_add_cons, ps_add_prod, ps_get;
	//
	// Find the authenticated user
	access_rights domxs = 0;
	access_rights curxs = 0;
	access_rights balxs = 0;
	a2sel_t authn_user;
	ok = ok && authenticate_user (&authn_user);
	//
	// Map the domain name to a domain identity
	int64_t domid = domain2domid (db, prs->VAL_DOMAIN, NULL, NULL);
	ok = ok && (domid >= 0);
	//
	// Map the domain and currency to a currency identity
	int64_t curid = ok ? domaincurrency2curid (db, prs->VAL_DOMAIN, prs->VAL_CURRENCY, NULL, NULL) : -1;
	ok = ok && (curid >= 0);
	//
	// (a) If the owner's username is given, derive the ownui from that
	int64_t ownui = -1;
	int64_t ownbi = -1;
	if (prs->VAL_OWNER != NULL) {
		/* Note: ownui < 0 propagates with ownbi < 0 */
		ownui = domainuser2usrid (db, prs->VAL_DOMAIN, prs->VAL_OWNER, -1);
		ownbi = usridcurid2balid (db, ownui, curid, true, &balxs, &authn_user);
	}
	//
	// (b) Alternatively, parse the balance identity
	if (prs->VAL_BALANCE != NULL) {
		ownbi = string2xxxid (prs->VAL_BALANCE);
		//TODO// No balxs... that would trigger ACL conditions below
	}
	//
	// In any case, we now want to have a balance
	ok = ok && (ownbi >= 0);
	//
	// Find the other user's usrid and balid (will propagate -1)
	int64_t othui = domainuser2usrid (db, prs->VAL_DOMAIN, prs->VAL_OTHER, -1);
	int64_t othbi = usridcurid2balid (db, othui, curid, true, NULL, NULL);
	if (prs->VAL_OTHER != NULL) {
		ok = ok && (othbi >= 0);
	}
	//
	// Find the optional request identity, set -1 if not given
	int64_t reqid = string2xxxid (prs->VAL_REQUEST);
	//
	// Find the optional transaction identity, set -1 if not given
	int64_t txnid = string2xxxid (prs->VAL_TXNID);
	//
	// Find the value as a number of tokens -- if required, demand > 0
	int64_t amt = 0;
	if (prs->VAL_VALUE != NULL) {
		amt = string2xxxid (prs->VAL_VALUE);
		ok = ok && (amt > 0);
		if (opcode == OPCODE_SUB) {
			amt = -amt;
			opcode = OPCODE_ADD;
		}
	}
	//
	// Fetch the current UNIX time
	int64_t now = time (NULL);
	//
	// Split into the various queries
	bool xs = true;
	switch (opcode) {
	case OPCODE_NEW:
		/* we initially set value==0 so we can continue into OPCODE_ADD */
		xs = xs && ((!ok) || maydo (balxs, ACCESS_WRITE));
		ok = ok && xs;
		ok = have_prepsql (&ps_new, db, "INSERT INTO history (curid,cons_balid,prod_balid,cons_usrid,prod_usrid,value,unixtime,descr,reqid) VALUES (?1,?2,?3,?4,?5,?6,?7,?8,CASE WHEN ?9 >= 0 THEN ?9 ELSE NULL END)", "iiiiiiisi", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_new, curid,ownbi,othbi,ownui,othui,0,now,prs->VAL_DESCR,reqid));
		txnid = ok ? sqlite3_last_insert_rowid (db) : -1;
		/* continue into OPCODE_ADD */
#if 0
	case OPCODE_DEL:
		xs = xs && ((!ok) || maydo (balxs, ACCESS_WRITE));
		ok = ok && xs;
		break;
#endif
	case OPCODE_ADD:
		/* history.value += amt, producer.balance += amt, consumer.balance -= amt */
		/* OPCODE_SUB was prepared as OPCODE_ADD with amt < 0 */
		ok = ok && (txnid > 0);
		xs = xs && ((!ok) || maydo (balxs, ACCESS_WRITE));
		ok = ok && xs;
		ok = have_prepsql (&ps_add, db,
				"UPDATE history SET value = value + ?1\n"
				"WHERE txnid=?2"
				"  AND value + ?1 >= 0"
				"  AND ((?1 <= 0 AND prod_balid = ?3) OR"
				"       (?1 >= 0 AND cons_balid = ?3))",
							"iii", "") && ok;
		ok = have_prepsql (&ps_add_cons, db,
				"UPDATE balance SET balance = balance - ?1\n"
				"WHERE balid=("
				"  SELECT cons_balid"
				"  FROM history"
				"  WHERE txnid=?2)",
							"ii", "") && ok;
		ok = have_prepsql (&ps_add_prod, db,
				"UPDATE balance SET balance = balance + ?1\n"
				"WHERE balid=("
				"  SELECT prod_balid"
				"  FROM history"
				"  WHERE txnid=?2)",
							"ii", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_add,      amt,txnid,ownbi));
		ok = ok && (sqlite3_changes (db) == 1);
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_add_cons, amt,txnid));
		ok = ok && (sqlite3_changes (db) == 1);
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_add_prod, amt,txnid));
		ok = ok && (sqlite3_changes (db) == 1);
		break;
	case OPCODE_GET:
		ok = have_prepsql (&ps_get, db,
				"SELECT"
				"  ?1 AS domain,"
				"  c.currency AS currency,"
				"  c.format AS format,"
				"  IFNULL(cons.username,'+lets+'||c.currency||'+'||CAST(hist.cons_balid AS VARCHAR(64))) AS consumer,"
				"  IFNULL(prod.username,'+lets+'||c.currency||'+'||CAST(hist.prod_balid AS VARCHAR(64))) AS producer,"
				"  hist.txnid AS txnid,"
				"  hist.reqid AS reqid,"
				"  hist.unixtime AS timestamp,"
				"  hist.value,"
				"  hist.descr AS description\n"
				"FROM"
				"  history hist,"
				"  curmap c\n"
				"LEFT JOIN usrmap cons ON cons.usrid = hist.cons_usrid\n"
				"LEFT JOIN usrmap prod ON prod.usrid = hist.prod_usrid\n"
				"LEFT JOIN balance bal ON cons.balid = bal.balid\n"
				"WHERE hist.curid = ?2"
				"  AND c.curid = ?2"
				"  AND hist.cons_balid = ?3"
				"  AND"
				"    CASE"
				"    WHEN ?5 IS NOT NULL"
				"    THEN hist.prod_balid = ?4"
				"    ELSE 1=1"
				"    END\n"
				"  AND maydo (?6,bal.access,?7)\n"
				"ORDER BY consumer ASC, producer ASC, timestamp DESC, txnid DESC",
						"siiisbi", "sssssiiiis") && ok;
		if (ok) {
			int dbstatus = bindstep_prepsql (&ps_get, prs->VAL_DOMAIN, curid, ownbi, othbi, prs->VAL_OTHER, &authn_user, sizeof (authn_user), ACCESS_READ);
			table_prepsql (&ps_get, dbstatus, ",,,{{[,,,,");
		}
		break;
	default:
		ok = false;
	}
	//
	// Return the result
	if (!xs) {
		wrap_status (403, "ARPA2 Access Denied");
	}
	return ok;
}

