# API for ARPA2 LETS

> *This is the webinterface for acting on the LETSystem.*

This is a collection of FastCGI programs to operate a rudimentary,
but fully functional LETSystem.  It extends into the logic of
ARPA2 Identities and regulates account sharing and visibility
via protocol-independent ARPA2 Communication Access Control.

**Note:** The API is being redefined.  Instead of separate `.fcgi`
programs per function, there is one integrated binary with mostly
shared code.  The results below have been good learning experiences,
but the new API is much more complete.


## activation.fcgi

This program can toggle the activity of a balance on or off under
a given user and currency.  This only works when activity is not
blocked by an administrator; if that happens, an operator will
have to intervene to unblock the user's balance for that currency.

Environment variables and parameters:

  * `QUERY_STRING` is set by the webserver to GET parameters.
    Settings of interst are:

      - `cur` is the currency to use, for instance `XES`.

      - `set` is the desired activity 'Y' or 'N' for yes/active
        or no/inactive.

      - `toi` may be used by administrators to set an account
        owned by a mere user.  There are two extra activity
        settings, namely 'B' and 'U' for block and unblock.
        (TODO: Administration is not yet implemented.)

  * `HTTP_ACCEPT` is set by the webserver.  Values of interest
    are `text/html`, `text/csv` and `application/json`.

  * `REMOTE_USER` is set by the webserver after successful
    authentication.  It MUST NOT be set in other situations.
    The form is `user@domain.name` and it MAY reside under
    another domain than the webserver's.

  * `ARPA2_DOMAIN` is the domain name under which Access Control
    will search, and under which the currency resides.  It is
    set as an environment variable in the webserver, because it
    varies with the hostname under which it is addressed.

  * `ARPA2_LETS_CURRENCY` overrides the `cur` parameter, and
    may be set by the webserver from sources such as the URL
    pathname (a virtual directory may hold the currency, and
    allow references to disable user choice).



## disbalance.fcgi

This program lists the balance of requested other(s) relative to
one's own balance.  Note that there is no notion of an absolate
balance, disbalances are considered relative between people.

Also note that disbalances imply hints as to where to turn our
attention, and the `?use' parameter below sorts accordingly,
with the socially best suggestions on top.  This is simply a
matter of consuming more from low balances, and producing more
for high balances.

Environment variables and parameters:

  * `QUERY_STRING` is set by the webserver to GET parameters.
    Settings of interst are:

      - `toi` is "the other" (relative to `moi` which is not a
        parameter value) user identity local-part (no domain).

      - `cur` is the currency to use, for instance `XES`.  The
        history will always be specific for one currency, so as
        to provide a compartmentalised user experience.

      - `use` is either `prod[uce]` or `cons[ume]` to indicate
        what market use should be compensated with LETS currency.
        This changes the order in which records are listed,
        mildly suggesting to reduce disbalance.

  * `HTTP_ACCEPT` is set by the webserver.  Values of interest
    are `text/html`, `text/csv` and `application/json`.

  * `REMOTE_USER` is set by the webserver after successful
    authentication.  It MUST NOT be set in other situations.
    The form is `user@domain.name` and it MAY reside under
    another domain than the webserver's.

  * `ARPA2_DOMAIN` is the domain name under which Access Control
    will search, and under which the currency resides.  It is
    set as an environment variable in the webserver, because it
    varies with the hostname under which it is addressed.

  * `ARPA2_LETS_CURRENCY` overrides the `cur` parameter, and
    may be set by the webserver from sources such as the URL
    pathname (a virtual directory may hold the currency, and
    allow references to disable user choice).


## appreciate.fcgi

This program books money from the logged-in user to another.
This reduces the consumer's balance by the given value, for all
to see.  It also increases the producer's balance by the same
value, for all to see.  To any other parties, this looks
quite normal; to the consumer and producer, their distance widens
by twice the value, which may be confusing at first.

Environment variables and parameters:

  * `QUERY_STRING` is set by the webserver to GET parameters.
    Settings of interst are:

      - `toi` is "the other" (relative to `moi` which is not a
        parameter value) user identity local-part (no domain).

      - `cur` is the currency to use, for instance `XES`.


      - `use` is either `prod[uce]` or `cons[ume]` to indicate
        what market use should be compensated with LETS currency.
        This is an optional parameter, for now raising an error
        on value `prod[uce]`.

      - `val` is the value as a positive integer number of
        currency tokens to pass from `moi` to `toi`.

      - `why` is a short descriptive string to be stored with
        the transaction record.

  * `HTTP_ACCEPT` is set by the webserver.  Values of interest
    are `text/html` and `application/json`.

  * `REMOTE_USER` is set by the webserver after successful
    authentication.  It MUST NOT be set in other situations.
    The form is `user@domain.name` and it MAY reside under
    another domain than the webserver's.

  * `ARPA2_DOMAIN` is the domain name under which Access Control
    will search, and under which the currency resides.  It is
    set as an environment variable in the webserver, because it
    varies with the hostname under which it is addressed.

  * `ARPA2_LETS_CURRENCY` overrides the `cur` parameter, and
    may be set by the webserver from sources such as the URL
    pathname (a virtual directory may hold the currency, and
    allow references to disable user choice).


## historydig.fcgi

This program digs up historic transactions, possibly limited
to the other(s) given.  Depending on the contact in which the
transaction took place, your username may look different;
it may be an Alias or Actor Identity, or you may have acted
in a Group context acting as a Group Member.  Likewise, the
other party will be shown with their contextual identity.
To make sense of this all, your role in the transaction will
be marked as prod[ucer] or cons[umer].  Consumers are the
ones who decrease their balance, while producers increase
theirs.

It may happen that a username was deregistered with the
domain.  In this case, the transaction remains but the
username is removed from the report.  This is both in the
interest of the "right to be forgotten", which is a privacy
right, and it also avoids confusion if a username is ever
recycled.  A new owner of the same username should not be
mistaken for an old owner of the same username and this
approach assures just that.

Environment variables and parameters:

  * `QUERY_STRING` is set by the webserver to GET parameters.
    Settings of interst are:

      - `toi` is "the other" to query (relative to `moi`
        which is not a parameter value).  Leave empty to
        query every user.

      - `cur` is the currency to query, for instance `XES`.

      - `use` was `prod[uce]`, `cons[ume]` or `both` to limit
        your role in the transactions to be listed.  If this
        value is absent, then `both` will be assumed.

      - *FUTURE EXTENSION:* Timestamps, description words.

  * `HTTP_ACCEPT` is set by the webserver.  Values of interest
    are `text/html`, `text/csv` and `application/json`.

  * `REMOTE_USER` is set by the webserver after successful
    authentication.  It MUST NOT be set in other situations.
    The form is `user@domain.name` and it MAY reside under
    another domain than the webserver's.

  * `ARPA2_DOMAIN` is the domain name under which Access Control
    will search, and under which the currency resides.  It is
    set as an environment variable in the webserver, because it
    varies with the hostname under which it is addressed.

  * `ARPA2_LETS_CURRENCY` overrides the `cur` parameter, and
    may be set by the webserver from sources such as the URL
    pathname (a virtual directory may hold the currency, and
    allow references to disable user choice).


## Access Control

Since money in a LETS is little more than communication, we allow
it to be sent from a (remote) sender to a (local) recipient.  But
we also allow currencies to be created by an administrator.

 1. To target `activation.fcgi` at our own account, we require
    authentication only, but no authorisation.  Changing the
    activity status fails if the account is blocked.  The
    activity of one's own balance can always be requested,
    possibly without `?set=` parameter.  It may show `B` for
    an administratively blocking, `Y` for active and `N` for
    inactive.

 2. To target `activation.fcgi` at another user, we require
    administrative control over the currency.  This means that
    we need Communication Access to the currency's identity
    `+lets+xxx@domain.name` for currency `xxx`.

 3. To target `disbalance.fcgi` or `appreciate.fcgi` at an
    individual user, we require white-listed Communication Access.

 4. To target `disbalance.fcgi` at a group, we want to iterate
    that group, which is only possible for members that allow
    it via the `GROUP_KNOW` Access Right.  Group iteration may
    reveal Group Member identities, but not their underlying
    delivery addresses.

 5. To target `historydig.fcgi` at our own account, we require
    authentication only, but no authorisation.  The others may
    be shown by their username if they are still known to the
    system, based on prior rights to see those, as apparently
    was the case for any transaction that was entered.

 6. *FUTURE OPTION:*
    Create a new currency.
    This requires `GROUP_CREATE` on `+lets@domain.name` or so.


## Copyright Notice

```
SPDX-License-Identifier: AGPL-3.0-or-later
SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
```
