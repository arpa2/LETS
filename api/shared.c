/* shared.c -- Shared code for the API to ARPA2 LETS.
 *
 * Routines for repeating SQLite3 queries and Access Control.
 *
 * Note the need to treat the domain separately for the currency
 * and the user; although mostly the same, the domains may differ
 * due to authentication with Realm Crossover, but the database
 * only deals with them after mapping to an Actor Identity, which
 * must share the currency's domain.
 *
 * Prepared objects are stored internally, and cleaned up on_exit().
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 * SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
 */


#include <fcgi_stdio.h>

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include <assert.h>

#include <ctype.h>
#include <string.h>

#include <time.h>

#include <sqlite3.h>

#define LOG_STYLE LOG_STYLE_SYSLOG
#include <arpa2/except.h>
#include <arpa2/identity.h>
#include <arpa2/group.h>
#include <arpa2/access.h>
#include <arpa2/access_comm.h>
#include <arpa2/rules.h>
#include <arpa2/rules_db.h>
#include <arpa2/util/snoprintf.h>

#include "shared.h"
#include "wrapio.h"



/* Simple elements with either a string or an int64_t.
 */
struct colval {
	bool isnull;
	const char *strval;
	int64_t intval;
};


/* Parse a hexadecimal character to its value; return -1 if bad.
 */
int hex2value (char inchar) {
	if ((inchar >= '0') && (inchar <= '9')) {
		return inchar - '0';
	} else {
		inchar &= 0xdf;
		if ((inchar >= 'A') && (inchar <= 'F')) {
			return inchar - 'A' + 10;
		} else {
			return -1;
		}
	}
}


/* Print directly on stdout.
 */
int stdio_vprintf (const char *fmt, va_list ap) {
	return vdprintf (1, fmt, ap);
}


/* Retrieve a Service Key from an environment variable.
 *  - svckey receives the output value
 *  - envvar is the name of the environment variable
 * Return success.
 */
bool envvar2svckey (rules_dbkey svckey, const char *envvar) {
	char *hexkey = getenv (envvar);
	if (hexkey == NULL) {
		log_error ("Missing Service Key variable: %s\n", envvar);
		wrap_status (500, "Server Configuration Error, see logfile");
		return false;
	}
	int hexkeylen = strlen (hexkey);
	if (!rules_dbkey_parse (svckey, hexkey, hexkeylen)) {
		log_error ("Invalid: %s=\"%.*s\"", envvar, hexkeylen, hexkey);
		wrap_status (500, "Server Configuration Error, see logfile");
		return false;
	}
	return true;
}


/* Selector callback accumulation structure, used between the local functions
 * rule_selector_cb() and arpa2_lets_access() below.
 */
struct lets_request {
	struct rules_request rr;
	access_rights reqflags;
	const a2sel_t *authn_user;
	uint16_t abstractlevel;
	int retval;
	//TODO// remote user?
};


/* Selector callback, processing previously set %FLAGS in such a manner that
 * the most specific @SELEC.TOR decides.
 */
static bool rule_selector_cb (struct rules_request *self, const a2sel_t *selector) {
	struct lets_request *req = (struct lets_request *) self;
	uint16_t steps;
	if (a2sel_abstractions (req->authn_user, selector, &steps)) {
		if (steps <= req->abstractlevel) {
			//
			// Replace - This is the most concrete match yet
			req->reqflags = req->rr.flags;
			req->abstractlevel = steps;
		} else if (steps == req->abstractlevel) {
			//
			// Enhance - this is just as concrete as what we had
			req->reqflags |= req->rr.flags;
		} else {
			//
			// Skip - this is more abstract than what we have now
			;
		}
	}
	//
	// Always signal continuation
	return true;
}


/* For a given authn_user, process a rule/setlen to retrieve rights.
 * Return false for processing failure or true for success.
 */
bool lets_rule_process (const a2sel_t *authn_user, const char *rule, int rulesetlen, access_rights *rights) {
	struct lets_request req;
	memset (&req, 0, sizeof (req));
	req.rr.optcb_selector = rule_selector_cb;
	req.abstractlevel = ~0;
	req.authn_user = authn_user;
	log_debug ("Access Control for %s from \"%.*s\"", authn_user->txt, rulesetlen-1, rule);
	bool ok = rules_process (&req.rr, rule, rulesetlen, true);
	*rights = ok ? req.reqflags : ACCESS_VISITOR;
	return ok;
}


/* The LETSystem database holds policy rules, used to delegate Access Control.
 * The format is simple enough, setting %FLAGS followed by @SELECT.ORS where
 * the most specific selector decides.
 *
 * TODO: Group processing: Find an Actor Identity ot use on a domain currency.
 */
static access_rights arpa2_lets_access (void *prepsql, int ruleidx, const a2sel_t *authn_user) {
	struct prepsql *ps = (struct prepsql *) prepsql;
	//
	// Retrieve and validate parameters.
	// Note that sqlite3_column_text() adds NUL terminator
	// which is not counted by sqlite3_value_bytes()
	const char *rule    = sqlite3_column_text  (ps->stmt, ruleidx);
	unsigned rulesetlen = sqlite3_column_bytes (ps->stmt, ruleidx) + 1;
	//
	// Evaluate the Policy Rule (treat it as a singleton Ruleset)
	access_rights rights = ACCESS_VISITOR;
	if (!lets_rule_process (authn_user, rule, rulesetlen, &rights)) {
		//TODO// Consider using com_err here
		log_error ("Rules processing error: \"%.*s\"", rulesetlen-1, rule);
		rights = ACCESS_VISITOR;
	}
	//
	// Return the rule-request processing output
	return rights;
}


/* Cleanup for a prepared statement.
 */
static void onexit_prepsql (int status, void *prepsql) {
	struct prepsql *ps = (struct prepsql *) prepsql;
	sqlite3_finalize (ps->stmt) - SQLITE_OK;
}


/* Have a prepared statement.  Only active when psql->stmt is NULL.
 * Registers the prepared statement for cleanup on exit.
 * Return success.
 */
bool have_prepsql (struct prepsql *ps, sqlite3 *db, const char *sql, const char *isis, const char *osos) {
	//
	// Correctness checks
	assert (db != NULL);
	assert (sql != NULL);
	assert (isis != NULL);
	//
	// Return if we already filled the structure
	if (ps->stmt != NULL) {
		assert (db == ps->db);
		return true;
	}
	//
	// Fill the structure with the desired query
	int dberr = sqlite3_prepare_v2 (db, sql, -1, &ps->stmt, NULL);
	if (dberr != SQLITE_OK) {
		log_error ("SQL error in have_prepsql: %s", sqlite3_errmsg (db));
		wrap_printf ("SQL error in have_prepsql: %s\n", sqlite3_errmsg (db));
		ps->stmt = NULL;
		wrap_status (500, NULL);
		return false;
	}
	//
	// Setup the desired response values
	assert (ps->stmt != NULL);
	ps->db = db;
	ps->isis = isis;
	ps->osos = osos;
	//
	// Best-effort registration for cleanup on exit
	on_exit (onexit_prepsql, ps);
	//
	// Return success
	return true;
}


/* Bind and step a prepared statement.  Take values from varargs, using the
 * isis format string to determine how many and whether each is "s"tring or
 * "i"nteger.  For "b"lob inputs, a pointer and unsigned length are expected.
 * TODO: Only this supports "b"blob in the isis strings (the rest ignores it).
 *
 * Return the sqlite3_step() output, or an error if anything fails.
 */
int bindstep_prepsql (struct prepsql *ps, ...) {
	bool ok = true;
	//
	// Correctness checks
	assert (ps != NULL);
	assert (ps->stmt != NULL);
	//
	// Reset query execution and clear previous bindings
	sqlite3_reset (ps->stmt);
	sqlite3_clear_bindings (ps->stmt);
	//
	// Iterate over args to fill the isis values
	va_list ap;
	va_start (ap, ps);
	int dbres = SQLITE_OK;
	for (int i = 0; dbres == SQLITE_OK; i++) {
		char *sarg;
		int64_t iarg;
		void *barg;
		unsigned blen;
		switch (ps->isis [i]) {
		case '\0':
			goto lastdone;
		case 's':
			sarg = va_arg (ap, char *);
			dbres = sqlite3_bind_text  (ps->stmt, i + 1, sarg,   -1, SQLITE_STATIC);
			break;
		case 'i':
			iarg = va_arg (ap, int64_t);
			dbres = sqlite3_bind_int64 (ps->stmt, i + 1, iarg                     );
			break;
		case 'b':
			barg = va_arg (ap, void *);
			blen = va_arg (ap, unsigned);
			dbres = sqlite3_bind_blob  (ps->stmt, i + 1, barg, blen, SQLITE_STATIC);
			break;
		default:
			log_debug ("Unknown character '%c' in prepsql->isis string", ps->isis [i]);
			dbres = SQLITE_ERROR;
		}
	}
lastdone:
	va_end (ap);
	//
	// Execute the prepared and bound SQL statement
	if (dbres == SQLITE_OK) {
		dbres = sqlite3_step (ps->stmt);
	} else {
		log_error ("LETSystem database error %d after prepared statement binding", dbres);
	}
	//
	// Return the SQLite3 return value
	return dbres;
}


/* Variant of readstep_prepsql() using varargs.
 */
static void vreadstep_prepsql (struct prepsql *ps, int rowc, struct colval *rowv) {
	//
	// Correctness checks
	assert (ps != NULL);
	assert (ps->stmt != NULL);
	assert (ps->osos != NULL);
	//
	// Iterate over args to fill the osos values
	int dbres = SQLITE_OK;
	for (int i = 0; i < rowc; i++) {
		rowv [i].isnull = (SQLITE_NULL == sqlite3_column_type (ps->stmt, i));
		if (ps->osos [i] == 's') {
			rowv [i].strval = sqlite3_column_text  (ps->stmt, i);
		} else {
			rowv [i].intval = sqlite3_column_int64 (ps->stmt, i);
		}
	}
}

/* Fetch an output value from an SQLITE_ROW in the prepared SQL statement.
 * This uses the osos format string.  Then take another step.
 *
 * Return the sqlite3_step() output, or an error if anything fails.
 */
int readstep_prepsql (struct prepsql *ps, ...) {
	//
	// Prepare the output array
	int rowc = strlen (ps->osos);
	struct colval rowv [rowc];
	memset (rowv, 0, sizeof (rowv));
	//
	// Read values into the array
	vreadstep_prepsql (ps, rowc, rowv);
	//
	// Store each value into arguments
	va_list datastore;
	va_start (datastore, ps);
	for (int rowi = 0; rowi < rowc; rowi++) {
		if (ps->osos [rowi] == 's') {
			* (va_arg (datastore, const char **)) = rowv [rowi].strval;
		} else {
			* (va_arg (datastore, int64_t     *)) = rowv [rowi].intval;
		}
	}
	va_end (datastore);
	//
	// Try to fetch the next output row
	return sqlite3_step (ps->stmt);
}


/* Variant of table_prepsql() with CSV output.
 */
static void table_prepsql_csv (struct prepsql *ps, int lasterr, char *fmt, int rowc, struct colval *rowv) {
	//
	// Output the header line in uppercase
	char *comma = "";
	for (int idx = 0; fmt [idx] != '\0'; idx++) {
		wrap_printf ("%s%s", comma, sqlite3_column_name (ps->stmt, idx));
		comma = ",";
	}
	wrap_printf ("\r\n");
	//
	// Repeat 0 or more times, simply printing each row
	while (lasterr == SQLITE_ROW) {
		//
		// Iterate over the row and output fields
		char *comma = "";
		for (int rowi = 0; rowi < rowc; rowi++) {
			if (rowv [rowi].isnull) {
				wrap_printf ("%snull", comma);
			} else if (ps->osos [rowi] == 's') {
				wrap_printf ("%s\"%s\"", comma,             rowv [rowi].strval);
			} else {
				wrap_printf ("%s%lld",   comma, (long long) rowv [rowi].intval);
			}
			comma = ",";
		}
		//
		// Terminate the data line
		wrap_printf ("\r\n");
		//
		// Fetch the next line (or not)
		lasterr = sqlite3_step (ps->stmt);
		if (lasterr == SQLITE_ROW) {
			vreadstep_prepsql (ps, rowc, rowv);
		}
	}
}


/* Variant of table_prepsql() with HTML output.
 */
static void table_prepsql_html (struct prepsql *ps, int lasterr, char *fmt, int rowc, struct colval *rowv) {
	//
	// Start the table
	wrap_printf ("<table class=\"ARPA2LETS_table\">\r\n");
	//
	// Output the header line in uppercase
	wrap_printf (" <tr class=\"ARPA2LETS_tr\">\r\n");
	for (int idx = 0; fmt [idx] != '\0'; idx++) {
		wrap_printf ("  <th class=\"ARPA2LETS_th\"><span class=\"ARPA2LETS_nom\">%s</th>\r\n", sqlite3_column_name (ps->stmt, idx));
	}
	wrap_printf (" </tr>\r\n");
	//
	// Repeat 0 or more times, simply printing each row
	while (lasterr == SQLITE_ROW) {
		//
		// Start the data line
		wrap_printf (" <tr class=\"ARPA2LETS_tr\">\r\n");
		//
		// Iterate over the row and output fields
		for (int rowi = 0; rowi < rowc; rowi++) {
			if (rowv [rowi].isnull) {
				wrap_printf ("  <td class=\"ARPA2LETS_td\"><span class=\"ARPA2LETS_nul\">&nbsp;</span></td>\r\n");
			} else if (ps->osos [rowi] == 's') {
				wrap_printf ("  <td class=\"ARPA2LETS_td\"><span class=\"ARPA2LETS_str\">%s</span></td>\r\n",
						            rowv [rowi].strval);
			} else {
				wrap_printf ("  <td class=\"ARPA2LETS_td\"><span class=\"ARPA2LETS_int\">%lld</span></td>\r\n",
						(long long) rowv [rowi].intval);
			}
		}
		//
		// Terminate the data line
		wrap_printf (" </tr>\r\n");
		//
		// Fetch the next line (or not)
		lasterr = sqlite3_step (ps->stmt);
		if (lasterr == SQLITE_ROW) {
			vreadstep_prepsql (ps, rowc, rowv);
		}
	}
	//
	// Close the table
	wrap_printf ("</table>\r\n");
}


/* JSON nesting structure for table_prepsql_json().
 */
struct jnest {
	struct jnest *prev;	/* Previous or outer jnest structure */
	int colnr;		/* Column 0, 1, ... */
	struct colval oldval;	/* Compare for future correspondance */
	bool dict;		/* Print column as dictionary element */
	bool fork;		/* Column is fit to fork structures */
	int64_t intval;		/* Storage if this is an int64_t column */
	const char *strval;	/* Storage if this is a  char *  column */
};


/* Variant of table_prepsql() with JSON output.
 *
 * Entered when a row is to be printed until the end.
 * Returned with the first column to not match prior
 * data, based on which recursion rolls back.  This is
 * only possible for forkable points, of course.
 *
 * Alternatively, returning -1 when done.
 */
static int table_prepsql_json (struct prepsql *ps, int lasterr, char *fmt, int rowc, struct colval *rowv, struct jnest *jouter) {
	//
	// Prepare the jnest structure
	struct jnest jinner;
	memset (&jinner, 0, sizeof (jinner));
	//
	// Special treatment for the initial call, without jnest
	if (jouter == NULL) {
		//
		// Produce an empty result for non-error empty output
		if (lasterr == SQLITE_DONE) {
			wrap_printf ("[]\r\n");
			return -1;
		}
		//
		// Produce null when an error was produced
		if (lasterr != SQLITE_ROW) {
			wrap_printf ("null\r\n");
			return -1;
		}
		//
		// Start the array of output holding the rows
		wrap_printf ("[");
		//
		// Setup non-zero fields in jinner
		jinner.dict = true;
		jinner.fork = true;
		//
		// Call recursively to print the first column of the first row
		int commence;
		do {
			commence = table_prepsql_json (ps, lasterr, fmt, rowc, rowv, &jinner);
printf ("And again...\n");
		} while (commence >= 0);
		//
		// End the array of output holding the rows
		wrap_printf ("]\r\n");
		//
		// Be done with it
		return commence;
	}
	//
	// Special treatment for the final call, when all is read
	if (fmt [jouter->colnr] == '\0') {
		//
		// First skip to the next row
		lasterr = sqlite3_step (ps->stmt);
		//
		// Close everything when there is no more data
		if (lasterr != SQLITE_ROW) {
			return -1;
		}
		//
		// Read fresh data store contents
		vreadstep_prepsql (ps, rowc, rowv);
		//
		// Then find the point to commence output
		int commence = jouter->colnr;
		for (struct jnest *jiter = jouter; jiter != NULL; jiter = jiter->prev) {
			//
			// Check if this is considered the new start
			if (commence == jiter->colnr) {
				//
				// Chcek if this point cannot fork structures
				if (!jiter->fork) {
					//
					// We are in the middle of something;
					// check again at the previous node
					commence--;
					continue;
				}
			}
			//
			// Check if this is an earlier mismatch
			if ((rowv [jiter->colnr].isnull ? 1 : 0) != (jiter->oldval.isnull ? 1 : 0)) {
				//
				// NULLs can match to NULL, but to nothing else
				commence = jiter->colnr;
			} else if (ps->osos [jiter->colnr] == 's') {
				//
				// If the string is off, assume matching below
				if (0 != strcmp (rowv [jiter->colnr].strval, jiter->oldval.strval)) {
					commence = jiter->colnr;
				}
			} else {
				//
				// If the integer is off, assume matching below
				if (rowv [jiter->colnr].intval != jiter->oldval.intval) {
					commence = jiter->colnr;
				}
			}
		}
		//
		// Now match points to point where to commence output (or is -1)
		return commence;
	}
	//
	// Require that osos offers the desired column
	char coltype = ps->osos [jouter->colnr];
	assertxt (((coltype == 's') || (coltype == 'i')),
			"Output format \"%s\" expects > %d columns \"%s\"",
				fmt,
				jouter->colnr,
				ps->osos);
	//
	// Begin to build the jinner structure
	jinner.prev = jouter;
	jinner.colnr = jouter->colnr + 1;
	//
	// Measure name length, learn structtype
	char structtype = fmt [jouter->colnr];
	switch (structtype) {
	case '{':
		//
		// This is a dictionary object
		jouter->dict = true;
		jouter->fork = true;
		break;
	case '[':
		//
		// This is an array object
		jouter->dict = false;
		jouter->fork = true;
		break;
	case ',':
		//
		// This is a continuation
		jouter->dict = jouter->dict;
		jouter->fork = false;
		break;
	case '\0':
		//
		// Syntax error in fmt, drop out
		return -1;
	}
	//
	// Now iterate over data values that match until my column
	int commence = jouter->colnr;
	bool comma = false;
	while (commence >= jouter->colnr) {
		//
		// In case we are looping, print a comma
		if (comma) {
			wrap_printf (",");
		} else {
			comma = true;
		}
		//
		// Are we entering?  Then (re)open the structure
		if ((structtype == '[') && (commence == jouter->colnr)) {
			wrap_printf ("%c", structtype);
		}
		//
		// Print and copy the value of the current column
		jouter->oldval.isnull = false;
		if (rowv [jouter->colnr].isnull) {
			//
			// Produce and copy NULL
			wrap_printf ("null");
			jouter->oldval.isnull = true;
		} else if (ps->osos [jouter->colnr] == 's') {
			//
			// Produce and copy a string
			wrap_printf ("\"%s\"",
				(jouter->oldval.strval =
					rowv [jouter->colnr].strval));
		} else {
			//
			// Produce and copy an integer
			wrap_printf ("%lld", (long long)
				(jouter->oldval.intval =
					rowv [jouter->colnr].intval));
		}
		//
		// Insert a separator when more fields will follow
		char post [2];
		if (jinner.colnr < rowc) {
			wrap_printf ("%c", (structtype == '{') ? ':' : ',');
			if (fmt [jinner.colnr] == ',') {
				post [0] = '\0';
			} else {
				wrap_printf ("%c", fmt [jinner.colnr]);
				post [0] = fmt [jinner.colnr] + 2;
				post [1] = '\0';
			}
		}
		//
		// Call recursively to print further fields
		// Returns the point to commence output, or -1 when done
		commence = table_prepsql_json (ps, lasterr, fmt, rowc, rowv, &jinner);
		//
		// Close the structure, as needed
		wrap_printf ("%s", post);
		//
		// Are we not nesting?  Then close the structure
		if ((structtype == '[') && (commence <= jouter->colnr)) {
			wrap_printf ("%c", structtype + 2);
		}
	}
	//
	// The match is before this column, or perhaps we are done
	return commence;
}


/* Output a data table from a prepared SQLite3 statement.
 * The return code from the SQLite3 statement is passed in.
 *
 * Use output formatting as desired, with support for:
 *  - text/json  with nested arrays and dictionary objects
 *  - text/html  with <table><tr><th>...<td>...</table>
 *  - text/plain with CSV notation
 *
 * Header names will be used where appropriate.  Formatting
 * can indicate desired nesting, which is used where supported.
 * When the return code passed in is an error, no table is
 * produced, but a special form is used where appropriate.
 *
 * The following format is used in the table format string:
 *  - "[" to enter      array output
 *  - "{" to enter dictionary output
 *  - "," to cause      value output
 * Where each may be preceded by the column name if needed.
 */
void table_prepsql (struct prepsql *ps, int lasterr, char *fmt) {
	//
	// Allow content_type negotiation
	enum wrap_content_type content_type = wrap_content_type (CONTENT_UNKNOWN);
	//
	// Response code is "200 OK" for any output, even empty
	// Response code is "500 Server Internal Error" on error
	if ((lasterr == SQLITE_ROW) || (lasterr == SQLITE_DONE)) {
		wrap_status (200, NULL);
	} else {
		wrap_status (500, "Database error: %s", sqlite3_errmsg (ps->db));
	}
	//
	// Setup for reading a row at a time
	int rowc = strlen (ps->osos);
	struct colval rowv [rowc];
	memset (rowv, 0, sizeof (rowv));
	//
	// Read the outcome from the last step (presumable the bindstep)
	vreadstep_prepsql (ps, rowc, rowv);
	//
	// Choose the right backend for the announced Content-Type
	switch (content_type) {
	case CONTENT_PLAIN:
	case CONTENT_CSV:
		//
		// Produce CSV output
		table_prepsql_csv  (ps, lasterr, fmt, rowc, rowv);
		break;
	case CONTENT_HTML:
		//
		// Produce HTML output
		table_prepsql_html (ps, lasterr, fmt, rowc, rowv);
		break;
	case CONTENT_JSON:
		//
		// Produce JSON output
		table_prepsql_json (ps, lasterr, fmt, rowc, rowv, NULL);
		break;
	default:
		//
		// Something went wrong...
		wrap_printf ("Unsupported data type\r\n");
		break;
	}
}


/* Begin a transaction.  Return success.
 */
bool txn_begin (sqlite3 *db) {
	bool ok = true;
	//
	// Be sure to have a BEGIN prepared statement
	static struct prepsql ps;
	ok = ok && have_prepsql (&ps, db, "BEGIN", "", "");
	//
	// Run the prepared statement
	sqlite3_reset (ps.stmt);
	ok = ok && (SQLITE_DONE == sqlite3_step (ps.stmt));
	//
	// Return the success result
	return ok;
}


/* Commit a transaction.  Return success.
 */
bool txn_commit (sqlite3 *db) {
	bool ok = true;
	//
	// Be sure to have a BEGIN prepared statement
	static struct prepsql ps;
	ok = ok && have_prepsql (&ps, db, "COMMIT", "", "");
	//
	// Run the prepared statement
	sqlite3_reset (ps.stmt);
	ok = ok && (SQLITE_OK == sqlite3_step (ps.stmt));
	//
	// Return the success result
	return ok;
}


/* Rollback a transaction.  Return success.
 */
bool txn_rollback (sqlite3 *db) {
	bool ok = true;
	//
	// Be sure to have a BEGIN prepared statement
	static struct prepsql ps;
	ok = ok && have_prepsql (&ps, db, "ROLLBACK", "", "");
	//
	// Run the prepared statement
	sqlite3_reset (ps.stmt);
	ok = ok && (SQLITE_OK == sqlite3_step (ps.stmt));
	//
	// Return the success result
	return ok;
}


/* Load the REMOTE_USER environment variable into the authn_user field.
 */
bool authenticate_user (a2sel_t *authn_user) {
	memset (authn_user, 0, sizeof (a2sel_t));
	const char *remote_user = getenv ("REMOTE_USER");
	if (remote_user == NULL) {
		log_error ("LETSystem Access denied due to missing REMOTE_USER environment variable");
		wrap_status (401, "No user");
		return false;
	}
	if (!a2sel_parse (authn_user, remote_user, 0)) {
		log_error ("LETSystem Access denied to a invalid REMOTE_USER=\"%s\"", remote_user);
		wrap_status (401, "Invalid User");
		return false;
	}
	log_debug ("LETSystem remote identity \"%s\"", authn_user->txt);
	return true;
}
//
static bool REMOTE_USER_isgiven (const a2sel_t *authn_user) {
	if (*authn_user->txt == '\0') {
		wrap_status (401, "No user");
		return false;
	} else {
		return true;
	}
}


/* Check if the authenticated user may be a system administrator, following
 * the environment variable ARPA2_LETS_ADMIN_RULE.  This function returns true
 * only if that variable exists, and in that case it sets the access rights,
 * which may or may not imply system administrator rights for the given
 * authenticated user.
 * Syntax error in the rule is reported, returns true and sets ACCESS_VISITOR.
 */
bool envvar_admin_rights (const a2sel_t *authn_user, access_rights *envrights) {
	bool ok = true;
	//
	// Try to fetch the rule
	const char *admin_rule = getenv ("ARPA2_LETS_ADMIN_RULE");
	if (admin_rule == NULL) {
		return false;
	}
	//
	// Process the single rule in the envvar
	ok = ok && lets_rule_process (authn_user, admin_rule, strlen (admin_rule) + 1, envrights);
	//
	// If processing failed, we should signal that, and set a negative result
	if (!ok) {
		*envrights = ACCESS_VISITOR;
		log_error ("Processing error in envvar ARPA2_LETS_ADMIN_RULE");
	}
	//
	// We did find a rule, so the envrights are valid output
	return true;
}


/* Check if the authenticated user is a member of a domain's admin group
 * and has been setup with %W or %F or %A privileges.
 *
 * This normally uses the ARPA2 RuleDB, but it may be overridden with an
 * envvar ARPA2_LETS_ADMIN_RULE for completely detached operation.
 *
 * This is a silent operation, as it is called without a need for an admin.
 *
 * Be conservative; if this cannot be determined, return false.
 */
static access_rights add_domain_admin (const a2sel_t *authn_user) {
	bool ok = true;
	group_marks marks = ACCESS_VISITOR;
	//
	// Switch between the envvar and the RuleDB
	if (envvar_admin_rights (authn_user, &marks)) {
		//
		// Consider the possibility of a syntax error
		ok = ok && (0 != (marks & ~ACCESS_VISITOR));
	} else {
		//
		// Process the RuleDB and look for group membership in admin@domain.name
		a2act_t member;
		ok = ok && a2act_parse (&member, authn_user->txt, authn_user->ofs [A2ID_OFS_END], 1);
		//TODO// What group are we searching here?!?  Find it first!!!
		ok = ok && group_hasmember (&member, NULL, 0, NULL, 0, &marks);
	}
	//
	// Post processing of results
	if (!ok) {
		return 0;
	}
	//
	// Reduce rights to those that an admin needs  TODO:MAYGO
	//NOT// marks &= (ACCESS_WRITE | ACCESS_READ | ACCESS_CONFIG | ACCESS_ADMIN);
	return marks;
}


/* Lookup the domid for a named domain.
 * Optionally determine the Access Rights, possibly useful in maydo().
 * Domain admins with %W or %F or %C rights are returned with ACCESS_ADMIN.
 * Return -1 on failure.
 */
int64_t domain2domid (sqlite3 *db, const char *domain, access_rights *opt_rights, const a2sel_t *authn_user) {
	bool ok = true;
	//
	// Be sure to have a domain query prepared statement
	static struct prepsql ps;
	ok = ok && have_prepsql (&ps, db,
			"SELECT"
			"	domid,"
			"	IFNULL (access, '%ODCF ~admin@'||?1||' %C ~@'||?1) "
			"FROM dommap "
			"WHERE domain = ?1",
			"s", "");
	//
	// Run the prepared statement
	//TODO// ok = ok && (SQLITE_ROW = bindstep_prepsql (&ps, domain));
	sqlite3_reset (ps.stmt);
	sqlite3_clear_bindings (ps.stmt);
	ok = ok && (SQLITE_OK  == sqlite3_bind_text  (ps.stmt, 1, domain, -1, SQLITE_STATIC));
	ok = ok && (SQLITE_ROW == sqlite3_step (ps.stmt));
	//
	// Optionally determine access rights
	if (opt_rights != NULL) {
		//
		// Programs must provide the authn_user to fetch optional rights
		ok = ok && REMOTE_USER_isgiven (authn_user);
		if (ok) {
			//
			// Determine optional non-admin rights from the inline Policy Rule
			*opt_rights = arpa2_lets_access (&ps, 1, authn_user);
			//
			// Consider additional rights from admin@ group membership
			*opt_rights = (*opt_rights & ~ACCESS_ADMIN) | add_domain_admin (authn_user);
			log_debug ("Optional rights set to 0x%08lx", *opt_rights);
		} else {
			//
			// Be impaired
			*opt_rights = ACCESS_VISITOR;
			log_debug ("Optional rights set to ACCESS_VISITOR");
		}
	}
	//
	// Return the desired values
	return ok ? sqlite3_column_int64 (ps.stmt, 0) : -1;
}


/* Lookup the usrid for a named domain and user.
 * Set usernamelen to -1 to have it derived with strlen().
 * Return -1 on failure.
 */
int64_t domainuser2usrid (sqlite3 *db, const char *domain, const char *username, int usernamelen) {
	bool ok = true;
	//
	// Be sure to have a balance query prepared statement
	static struct prepsql ps;
	ok = ok && have_prepsql (&ps, db,
			"SELECT usrmap.usrid "
			"FROM dommap, usrmap "
			"WHERE dommap.domain = ?1"
			"  AND usrmap.username = ?2"
			"  AND usrmap.domid = dommap.domid",
			"ss", "");
	//
	// Run the prepared statement
	sqlite3_reset (ps.stmt);
	sqlite3_clear_bindings (ps.stmt);
	ok = ok && (SQLITE_OK  == sqlite3_bind_text  (ps.stmt, 1, domain,            -1, SQLITE_STATIC));
	ok = ok && (SQLITE_OK  == sqlite3_bind_text  (ps.stmt, 2, username, usernamelen, SQLITE_STATIC));
	ok = ok && (SQLITE_ROW == sqlite3_step (ps.stmt));
	//
	// Return the desired value
	return ok ? sqlite3_column_int64 (ps.stmt, 0) : -1;
}


/* Lookup the usrid for a named a2id.
 * Return -1 on failure.
 */
int64_t a2id2usrid (sqlite3 *db, a2id_t *id) {
	return domainuser2usrid (db,
			&id->txt [id->ofs [A2ID_OFS_DOMAIN]],
			id->txt,
			id->ofs [A2ID_OFS_AT_DOMAIN]);
}


/* Lookup the curid for a named domain and currency.
 * Optionally determine the Access Rights, possibly useful in maydo().
 * Note that ACCESS_ADMIN at the domain level may enhance the opt_rights.
 * Return -1 on failure.
 */
int64_t domaincurrency2curid (sqlite3 *db, const char *domain, const char *CUR, access_rights *opt_rights, const a2sel_t *authn_user) {
	bool ok = true;
	//
	// Be sure to have a currency query prepared statement
	static struct prepsql ps;
	ok = ok && have_prepsql (&ps, db,
			"SELECT"
			"	curmap.curid,"
			"	IFNULL (curmap.access,"
			"		'%ODCXAF ~admin@'"
			"		||?1"
			"		||' %C ~@'"
			"		||?1) "
			"FROM dommap, curmap "
			"WHERE dommap.domain = ?1"
			"  AND curmap.currency = ?2"
			"  AND curmap.domid = dommap.domid",
			"ss", "");
	//
	// Run the prepared statement
	sqlite3_reset (ps.stmt);
	sqlite3_clear_bindings (ps.stmt);
	ok = ok && (SQLITE_OK  == sqlite3_bind_text  (ps.stmt, 1, domain, -1, SQLITE_STATIC));
	ok = ok && (SQLITE_OK  == sqlite3_bind_text  (ps.stmt, 2, CUR,    -1, SQLITE_STATIC));
	ok = ok && (SQLITE_ROW == sqlite3_step (ps.stmt));
	//
	// Optionally determine access rights
	if (opt_rights != NULL) {
		//
		// Programs must provide the authn_user to fetch optional rights
		ok = ok && REMOTE_USER_isgiven (authn_user);
		//
		// Determine optional rights from the inline Policy Rule
		*opt_rights = ok ? arpa2_lets_access (&ps, 1, authn_user) : ACCESS_VISITOR;
	}
	//
	// Return the desired value
	return ok ? sqlite3_column_int64 (ps.stmt, 0) : -1;
}


/* Lookup the balid for a given usrid and curid.
 * Inactive balances are only returned when the selection flag is false.
 * Optionally determine the Access Rights, possibly useful in maydo().
 * Return -1 on failure.
 */
int64_t usridcurid2balid (sqlite3 *db, int64_t usrid, int64_t curid, bool only_active, access_rights *opt_rights, const a2sel_t *authn_user) {
	//
	// Fail if the input is already failed
	if ((usrid < 0) || (curid < 0)) {
		return -1;
	}
	//
	// Be sure to have a balance query prepared statement
	static struct prepsql ps;
	bool ok = have_prepsql (&ps, db,
			"SELECT"
			"	balance.balid,"
			"	IFNULL (balance.access,"
			"		(SELECT"
			"			'%ODFRW ~admin@'||dommap.domain"
			"			|| GROUP_CONCAT (' ~'||ownmap.username||'@'||dommap.domain, '')"
			"			|| ' %R ~@'||dommap.domain"
			"		FROM curmap, dommap, usrmap ownmap"
			"		WHERE curmap.curid = ?1"
			"		  AND dommap.domid = curmap.domid"
			"                 AND ownmap.balid = balance.balid"
			"		  AND ownmap.domid = dommap.domid"
			"		GROUP BY dommap.domid"
			"		)"
			"	)"
			"FROM balance, usrmap "
			"WHERE usrmap.balid = balance.balid"
			"  AND balance.curid = ?1"
			"  AND usrmap.usrid = ?2"
			"  AND ( balance.activity = 'Y' OR ?3 = 0 )",
			"iiis", "");
	//
	// Run the prepared statement
	sqlite3_reset (ps.stmt);
	sqlite3_clear_bindings (ps.stmt);
	log_debug ("usridcurid2balid: curid=%d, usrid=%d, only_active=%d", curid, usrid, only_active);
	ok = ok && (SQLITE_OK  == sqlite3_bind_int64 (ps.stmt, 1, curid));
	ok = ok && (SQLITE_OK  == sqlite3_bind_int64 (ps.stmt, 2, usrid));
	ok = ok && (SQLITE_OK  == sqlite3_bind_int64 (ps.stmt, 3, only_active ? 1 : 0));
	ok = ok && (SQLITE_ROW == sqlite3_step (ps.stmt));
	//
	// Optionally determine access rights
	if (opt_rights != NULL) {
		//
		// Programs must provide the authn_user to fetch optional rights
		ok = ok && REMOTE_USER_isgiven (authn_user);
		//
		// Determine optional rights from the inline Policy Rule
		*opt_rights = ok ? arpa2_lets_access (&ps, 1, authn_user) : ACCESS_VISITOR;
	}
	//
	// Return the desired value
	return ok ? sqlite3_column_int64 (ps.stmt, 0) : -1;
}


/* Lookup the Access Rights for a balance with the given balid.
 * Return no rights when an error occurs.
 */
access_rights balid2balxs (sqlite3 *db, int64_t balid, const a2sel_t *authn_user) {
	//
	// Pass no rights if the input is already failed
	if (balid < 0) {
		return 0;
	}
	//
	// Be sure to have a balance query prepared statement
	static struct prepsql ps;
	bool ok = have_prepsql (&ps, db,
			"SELECT"
			"	IFNULL (balance.access,"
			"		(SELECT"
			"			'%ODFRW ~admin@'||dommap.domain"
			"			|| GROUP_CONCAT (' ~'||ownmap.username||'@'||dommap.domain, '')"
			"			|| ' %R ~@'||dommap.domain"
			"		)"
			"		FROM curmap, dommap, usrmap ownmap"
			"		WHERE curmap.curid = balance.curid"
			"		  AND dommap.domid = curmap.domid"
			"		  AND ownmap.balid = balance.balid"
			"		  AND ownmap.domid = dommap.domid"
			"		GROUP BY dommap.domid"
			"	)"
			"FROM balance "
			"WHERE balance.balid = ?1",
			"i", "");
	//
	// Run the prepared statement
	sqlite3_reset (ps.stmt);
	sqlite3_clear_bindings (ps.stmt);
	ok = ok && (SQLITE_OK  == sqlite3_bind_int64 (ps.stmt, 1, balid));
	ok = ok && (SQLITE_ROW == sqlite3_step (ps.stmt));
	//
	// Programs must provide the authn_user to fetch optional rights
	ok = ok && REMOTE_USER_isgiven (authn_user);
	//
	// Determine and return access rights from the inline Policy Rule
	return ok ? arpa2_lets_access (&ps, 0, authn_user) : ACCESS_VISITOR;
}


/* Parse a number to an int64_t identity value.
 * Return -1 on failure.
 */
int64_t string2xxxid (char *str) {
	char *endptr = NULL;
	long long int x = (str != NULL) ? strtoll (str, &endptr, 10) : -1;
	bool ok = (endptr != NULL) && (str != endptr) && (*endptr == '\0');
	ok = ok && (x >= 0);
	ok = ok && (x == (long long int) (int64_t) x);
	return ok ? (int64_t) x : -1;
}

