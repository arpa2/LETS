/* switch.h -- Switching code for the API to ARPA2 LETS.
 *
 * Generic (Fast)CGI handling, command parsing and backend invocation.
 *
 * These are calling headers for the various backends.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 * SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
 */



/*
 *
 * STRUCTURES AND DEFIITIONS
 *
 */



/* Set operation to perform on (a rule in) a ruleset.
 * We usually ADD, SET, DEL or GET.  Listing rules in a
 * ruleset can make use of the GET set operation.
 */
enum opcode {
	OPCODE_NEW,
	OPCODE_ADD,
	OPCODE_SET,
	OPCODE_DEL,
	OPCODE_GET,
	OPCODE_SUB,
	OPCODE_ASK,
	OPCODE_OTHER,           /* marker for unknown subcommand */
};

/* Though opcode_t is intended to carry an enum opcode
 * value, it is mapped to short for compatibility with
 * "cmdparse.h" generic definitions.
 */
typedef short opcode_t;


/* Variable numbering in the parser
 */
enum parse_variables {
	VAR_DOMAIN,		/* ARPA2 Domain that scopes LETS currencies */
	VAR_CURRENCY,		/* ARPA2 LETS currency, like "XES" */
	VAR_BALANCE,		/* ARPA2 LETS currency balance id, like 1234 */
	VAR_OWNER,		/* Balance owner's username (one of many) */
	VAR_ACTIVATION,		/* Balance activation: Y(es), N(o), B(lock) */
	VAR_NEWUSER,		/* Newly created user name */
	VAR_OTHER,		/* Other user's name */
	VAR_FORMAT,		/* Currency output format */
	VAR_VALUE,		/* Value as a currency token count */
	VAR_REQUEST,		/* Request or offer identifier */
	VAR_TXNID,		/* Transaction identifier */
	VAR_DESCR,		/* Description (of a transaction) */
	VAR_ACCESS,		/* Access Control via a Policy Rule */
	/* End marker */
	VAR_MARKER
};

#define VAR_OFFER VAR_ACTIVATION

/* Values to be quickly referenced in the parsed array
 */
#define VAL_DOMAIN	values [VAR_DOMAIN]
#define VAL_CURRENCY	values [VAR_CURRENCY]
#define VAL_BALANCE	values [VAR_BALANCE]
#define VAL_OWNER	values [VAR_OWNER]
#define VAL_ACTIVATION	values [VAR_ACTIVATION]
#define VAL_OFFER	values [VAR_OFFER]
#define VAL_NEWUSER	values [VAR_NEWUSER]
#define VAL_OTHER	values [VAR_OTHER]
#define VAL_FORMAT	values [VAR_FORMAT]
#define VAL_VALUE	values [VAR_VALUE]
#define VAL_REQUEST	values [VAR_REQUEST]
#define VAL_TXNID	values [VAR_TXNID]
#define VAL_DESCR	values [VAR_DESCR]
#define VAL_ACCESS	values [VAR_ACCESS]

/* Flags to compose in the combinations
 */
#define FLAG_DOMAIN	(1 << VAR_DOMAIN)
#define FLAG_CURRENCY	(1 << VAR_CURRENCY)
#define FLAG_BALANCE	(1 << VAR_BALANCE)
#define FLAG_OWNER	(1 << VAR_OWNER)
#define FLAG_ACTIVATION	(1 << VAR_ACTIVATION)
#define FLAG_OFFER	(1 << VAR_OFFER)
#define FLAG_NEWUSER	(1 << VAR_NEWUSER)
#define FLAG_OTHER	(1 << VAR_OTHER)
#define FLAG_FORMAT	(1 << VAR_FORMAT)
#define FLAG_VALUE	(1 << VAR_VALUE)
#define FLAG_REQUEST	(1 << VAR_REQUEST)
#define FLAG_TXNID	(1 << VAR_TXNID)
#define FLAG_DESCR	(1 << VAR_DESCR)
#define FLAG_ACCESS	(1 << VAR_ACCESS)



/*
 *
 * OPERATIONS
 *
 */



struct error {
	int http_status;
	char *http_error;
	char *explain;
	int line;
	char *file;
};


/* Print directly on stdout.
 */
int stdio_vprintf (const char *fmt, va_list ap);


bool do_domain     (sqlite3 *db, opcode_t opcode, struct cmdparser *prs);
bool do_currency   (sqlite3 *db, opcode_t opcode, struct cmdparser *prs);
bool do_balance    (sqlite3 *db, opcode_t opcode, struct cmdparser *prs);
bool do_consume    (sqlite3 *db, opcode_t opcode, struct cmdparser *prs);
bool do_produce    (sqlite3 *db, opcode_t opcode, struct cmdparser *prs);
bool do_disbalance (sqlite3 *db, opcode_t opcode, struct cmdparser *prs);
