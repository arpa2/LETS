/* wrapio.c -- FastCGI switching code for the API to ARPA2 LETS.
 *
 * Generic (Fast)CGI handling, basically FCGI_Accept() and FCGI_vprintf().
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 * SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
 */


#include <fcgi_stdio.h>

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <syslog.h>

#include <sqlite3.h>

#include <arpa2/util/cmdparse.h>

#include "switch.h"
#include "wrapio.h"



/* Static global flag to know whether or not the
 * wrap_accept() function was called.
 */
static bool wrap_accepted = false;

/* Static global with exit status for HTTP.
 * It can be set only once per call.
 */
static int http_status = 0;


/* Static global with content type for this request.
 * It can be set only once per call.
 */
enum wrap_content_type content_type = CONTENT_UNKNOWN;


/* Accept a FastCGI or CGI run.
 */
int wrap_accept (void) {
	http_status = 0;
	content_type = CONTENT_UNKNOWN;
	wrap_accepted = true;
	return FCGI_Accept ();
}


/* Provide Content-Type.  For CONTENT_UNKNOWN, negotiate with Accept:
 */
enum wrap_content_type wrap_content_type (enum wrap_content_type ctype) {
	//
	// Do not set the content_type if it is already set
	if (content_type != CONTENT_UNKNOWN) {
		goto already_done;
	}
	//
	// Possible choose a default content_type
	if (ctype == CONTENT_UNKNOWN) {
		char *accepted = getenv ("HTTP_ACCEPT");
		if (accepted == NULL) {
			ctype = CONTENT_PLAIN;
		} else if (strstr (accepted, "text/*"   ) != NULL) {
			ctype = CONTENT_CSV;
		} else if (strstr (accepted, "text/csv" ) != NULL) {
			ctype = CONTENT_CSV;
		} else if (strstr (accepted, "text/json") != NULL) {
			ctype = CONTENT_JSON;
		} else if (strstr (accepted, "text/html") != NULL) {
			ctype = CONTENT_HTML;
		} else {
			ctype = CONTENT_PLAIN;
		}
	}
	//
	// Set the requested content_type
	content_type = ctype;
	//
	// Produce the header if we are still in time
	if (wrap_accepted && (http_status == 0)) {
		wrap_printf ("Content-Type: %s; charset=utf-8\r\n",
			(content_type == CONTENT_JSON) ?
					"text/json" :
			(content_type == CONTENT_CSV)  ?
					"text/csv; header=present" :
			(content_type == CONTENT_HTML) ?
					"text/html"
			:
					"text/plain"
		);
				
	}
	//
	// Return the resulting content_type
already_done:
	return content_type;
}



/* Set a status code and an optional formatted error message.
 * The status code is used for HTTP, the error message
 * is used for log output.
 */
void wrap_status (int status, const char *fmt, ...) {
	if (http_status == 0) {
		http_status = status;
		FCGI_SetExitStatus (http_status);
	}
	if (fmt != NULL) {
		int prio = (status < 300) ? LOG_DEBUG : (status < 400 ) ? LOG_NOTICE : LOG_ERR;
		va_list ap;
		va_start (ap, fmt);
		if (wrap_accepted) {
			vsyslog (prio, fmt, ap);
		} else {
			stdio_vprintf (fmt, ap);
			stdio_vprintf ("\n", ap);
		}
		va_end (ap);
	}
}


/* Formatted print on the desired output channel, which may be
 * stdout or get sent away via FastCGI, as appropriate.
 *
 * Print is wrapped so we can hide FastCGI redefinitions.
 */
int wrap_printf (char *fmt, ...) {
	va_list ap;
	va_start (ap, fmt);
	int rv = ( wrap_accepted ? FCGI_vprintf : stdio_vprintf ) (fmt, ap);
	va_end (ap);
	return rv;
}


/* Exit the program, deriving the exit code from reported HTTP status.
 */
void wrap_exit (void) {
	int rv = (http_status < 300) ? 0 : 1;
	exit (rv);
}

