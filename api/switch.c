/* switch.c -- Switching code for the API to ARPA2 LETS.
 *
 * Generic (Fast)CGI handling, command parsing and backend invocation.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 * SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include <ctype.h>
#include <string.h>

#include <time.h>

#include <sqlite3.h>

#define LOG_STYLE LOG_STYLE_SYSLOG
#include <arpa2/except.h>
#include <arpa2/identity.h>
#include <arpa2/access.h>
#include <arpa2/access_comm.h>
#include <arpa2/rules.h>
#include <arpa2/rules_db.h>
#include <arpa2/util/snoprintf.h>
#include <arpa2/util/cmdparse.h>

#include "shared.h"
#include "switch.h"
#include "wrapio.h"




/*
 *
 * GLOBAL STATIC STORAGE
 *
 */
static sqlite3 *db = NULL;




/*
 *
 * OPERATIONS FUNCTIONS
 *
 */


bool domain_ops (opcode_t opcode, struct cmdparser *prs,
			const char *usage,
			int argc, char *argv []) {
	return do_domain (db, opcode, prs);
}

bool currency_ops (opcode_t opcode, struct cmdparser *prs,
			const char *usage,
			int argc, char *argv []) {
	return do_currency (db, opcode, prs);
}

bool balance_ops (opcode_t opcode, struct cmdparser *prs,
			const char *usage,
			int argc, char *argv []) {
	return do_balance (db, opcode, prs);
}

bool disbalance_ops (opcode_t opcode, struct cmdparser *prs,
			const char *usage,
			int argc, char *argv []) {
	return do_disbalance (db, opcode, prs);
}

bool consume_ops (opcode_t opcode, struct cmdparser *prs,
			const char *usage,
			int argc, char *argv []) {
	return do_consume (db, opcode, prs);
}

bool produce_ops (opcode_t opcode, struct cmdparser *prs,
			const char *usage,
			int argc, char *argv []) {
	return do_produce (db, opcode, prs);
}



/*
 *
 * COMMANDLINE GRAMMAR & USAGE
 *
 */


static const char a2lets_usage [] = "...  Manages ARPA2 Local Exchange Trading Systems\n"
	"domain ...  Manage LETS domains\n"
	"currency ...  Manage LETS currencies under a domain\n"
	"balance  ...  Manage LETS domain-local currency balances\n"
	"disbalance ...  Retrieves LETS relative balances\n"
	"consume ...  Acknowledge Consumption with LETS domain-local currency\n"
	"produce ...  Request or Offer Production for LETS domain-local currency"
	;

static const char domain_usage [] = "... Manages domains that can hold LETS currencies\n"
	"domain new ...  Create a domain with LETS currencies\n"
	"domain set ...  Update a domain with LETS currencies\n"
	"domain get ...  Show   a domain with LETS currencies\n"
	"domain del ...  Remove a domain with LETS currencies";

static const char currency_usage [] = "...  Manages LETS currencies under a domain\n"
	"currency new ...  Create a LETS currency\n"
	"currency set ...  Update a LETS currency\n"
	"currency get ...  Show   a LETS currency\n"
	"currency del ...  Remove a LETS currency"
	;

static const char balance_usage [] = "...  Manages LETS domain-local currency balances\n"
	"balance new ...  Create a LETS currency balance (not shared)\n"
	"balance add ...  Create a LETS currency balance user (shared)\n"
	"balance get ...  Review a LETS currency balance or user\n"
	"balance set ...  Update a LETS currency balance or user\n"
	"balance del ...  Remove a LETS currency balance or user"
	;

static const char disbalance_usage [] = "...  Retrieves LETS relative balances\n"
	"disbalance get ...  Retrieve a LETS relative balance"
	;

static const char consume_usage [] = "...  Consumption with LETS domain-local currency\n"
	"consume new ...  Transaction creation  (by consumer)"
	"\nconsume add ...  Transaction increment (by consumer)"
	"\nconsume sub ...  Transaction decrement (by producer)"
	// "\nconsume del ...  Transaction deletion  (by producer)"
	"\nconsume get ...  Transaction listing   (by consumer)"
	;

static const char produce_usage [] = "...  Production with LETS domain-local currency\n"
	"produce new ...  Request or Offer creation\n"
	"produce set ...  Request of Offer change\n"
	"produce del ...  Request of Offer remove\n"
	"produce get ...  Request of Offer review"
	;


/* a2lets domain new|set|del|get ... */
static const char domain_newset_usage [] = "domain A2DOMAIN [access RULE]";

static const uint32_t domain_newset_combis [] = {
	/* Only the domain always needs to be provided. */
	FLAG_DOMAIN, FLAG_DOMAIN,
	/* Do not require an access rule. Absense translates to NULL/default. */
	/* end marker */
	0
};

static const struct cmdparse_grammar domain_newset_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		NULL,		// CURRENCY,
		NULL,		// BALANCE,
		NULL,		// OWNER,
		NULL,		// ACTIVATION,
		NULL,		// NEWUSER,
		NULL,		// OTHER,
		NULL,		// FORMAT,
		NULL,		// VALUE
		NULL,		// REQUEST,
		NULL,		// TXNID,
		NULL,		// DESCR,
		"access",	// ACCESS,
	},
	.listwords = FLAG_FORMAT | FLAG_ACCESS,
	.combinations = domain_newset_combis,
};

static const char domain_del_usage [] = "domain A2DOMAIN";

static const uint32_t domain_del_combis [] = {
	/* Only the domain always needs to be provided. */
	FLAG_DOMAIN, FLAG_DOMAIN,
	/* end marker */
	0
};

static const struct cmdparse_grammar domain_del_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		NULL,		// CURRENCY,
		NULL,		// BALANCE,
		NULL,		// OWNER,
		NULL,		// ACTIVATION,
		NULL,		// NEWUSER,
		NULL,		// OTHER,
		NULL,		// FORMAT,
		NULL,		// VALUE
		NULL,		// REQUEST,
		NULL,		// TXNID,
		NULL,		// DESCR,
		NULL,		// ACCESS,
	},
	.listwords = FLAG_FORMAT,
	.combinations = domain_del_combis,
};

static const char domain_get_usage [] = "[domain A2DOMAIN]";

static const uint32_t domain_get_combis [] = {
	/* Even the domain name is optional (admin may list). */
	/* end marker */
	0
};

static const struct cmdparse_grammar domain_get_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		NULL,		// CURRENCY,
		NULL,		// BALANCE,
		NULL,		// OWNER,
		NULL,		// ACTIVATION,
		NULL,		// NEWUSER,
		NULL,		// OTHER,
		NULL,		// FORMAT,
		NULL,		// VALUE
		NULL,		// REQUEST,
		NULL,		// TXNID,
		NULL,		// DESCR,
		NULL,		// ACCESS,
	},
	.listwords = FLAG_FORMAT,
	.combinations = domain_get_combis,
};


/* a2lets currency add|set ... */
static const char currency_newset_usage [] = "domain A2DOMAIN currency CURRENCY\n"
	"format FMTSTRING [access RULE]"
	;


static const uint32_t currency_newset_combis [] = {
	/* New currencies must have the format string;
	 * it is also the only thing that can be updated. */
	FLAG_DOMAIN | FLAG_CURRENCY | FLAG_FORMAT,
	FLAG_DOMAIN | FLAG_CURRENCY | FLAG_FORMAT,
	/* Do not require access.  Set to default/NULL if absent. */
	/* end marker */
	0
};

static const struct cmdparse_grammar currency_newset_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		"currency",	// CURRENCY,
		NULL,		// BALANCE,
		NULL,		// OWNER,
		NULL,		// ACTIVATION,
		NULL,		// NEWUSER,
		NULL,		// OTHER,
		"format",	// FORMAT,
		NULL,		// VALUE
		NULL,		// REQUEST,
		NULL,		// TXNID,
		NULL,		// DESCR,
		"access",	// ACCESS,
	},
	.listwords = FLAG_FORMAT | FLAG_ACCESS,
	.combinations = currency_newset_combis,
};

/* a2lets currency del ... */
static const char currency_del_usage [] = "domain A2DOMAIN currency CURRENCY"
	;

static const uint32_t currency_del_combis [] = {
	/* Deletion requires both coordinates. */
	FLAG_DOMAIN | FLAG_CURRENCY, FLAG_DOMAIN | FLAG_CURRENCY,
	/* end marker */
	0
};

static const struct cmdparse_grammar currency_del_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		"currency",	// CURRENCY,
		NULL,		// BALANCE,
		NULL,		// OWNER,
		NULL,		// ACTIVATION,
		NULL,		// NEWUSER,
		NULL,		// OTHER,
		NULL,		// FORMAT,
		NULL,		// VALUE
		NULL,		// REQUEST,
		NULL,		// TXNID,
		NULL,		// DESCR,
		NULL,		// ACCESS,
	},
	.listwords = 0,
	.combinations = currency_del_combis,
};


/* a2lets currency get ... */
static const char currency_get_usage [] = "domain A2DOMAIN [currency CURRENCY]"
	;

static const uint32_t currency_get_combis [] = {
	/* Fetching only requires the domain. */
	FLAG_DOMAIN, FLAG_DOMAIN,
	/* end marker */
	0
};

static const struct cmdparse_grammar currency_get_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		"currency",	// CURRENCY,
		NULL,		// BALANCE,
		NULL,		// OWNER,
		NULL,		// ACTIVATION,
		NULL,		// NEWUSER,
		NULL,		// OTHER,
		NULL,		// FORMAT,
		NULL,		// VALUE
		NULL,		// REQUEST,
		NULL,		// TXNID,
		NULL,		// DESCR,
		NULL,		// ACCESS,
	},
	.listwords = 0,
	.combinations = currency_get_combis,
};

/* a2lets balance new ... */
static const char balance_new_usage [] = "domain A2DOMAIN currency CURRENCY\n"
	"newuser USERNAME activation ACTIVITY [access RULE]"
	;


static const uint32_t balance_new_combis [] = {
	/* Require the two basic coordiates. */
	FLAG_DOMAIN | FLAG_CURRENCY, FLAG_DOMAIN | FLAG_CURRENCY,
	/* Require the newuser and activation. */
	FLAG_NEWUSER | FLAG_ACTIVATION, FLAG_NEWUSER | FLAG_ACTIVATION,
	/* To create a new balance: Neither balance nor owner. */
	FLAG_BALANCE | FLAG_OWNER, 0,
	/* Do not require access.  If absent, set NULL/default rule. */
	/* end marker */
	0
};

static const struct cmdparse_grammar balance_new_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		"currency",	// CURRENCY,
		NULL,		// BALANCE,
		NULL,		// OWNER,
		"activation",	// ACTIVATION,
		"newuser",	// NEWUSER,
		NULL,		// OTHER,
		NULL,		// FORMAT,
		NULL,		// VALUE
		NULL,		// REQUEST,
		NULL,		// TXNID,
		NULL,		// DESCR,
		"access",	// ACCESS,
	},
	.listwords = FLAG_ACCESS,
	.combinations = balance_new_combis,
};

/* a2lets balance add ... */
static const char balance_add_usage [] = "domain A2DOMAIN currency CURRENCY\n"
	"( balance BALID | owner USERNAME )\n"
	"newuser USERNAME"
	;


static const uint32_t balance_add_combis [] = {
	/* Require the two basic coordiates. */
	FLAG_DOMAIN | FLAG_CURRENCY, FLAG_DOMAIN | FLAG_CURRENCY,
	/* Require the newuser. */
	FLAG_NEWUSER, FLAG_NEWUSER,
	/* To add another userid:   Either balance or owner, not both. */
	FLAG_BALANCE | FLAG_OWNER, FLAG_BALANCE,
	FLAG_BALANCE | FLAG_OWNER, FLAG_OWNER,
	/* end marker */
	0
};

static const struct cmdparse_grammar balance_add_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		"currency",	// CURRENCY,
		"balance",	// BALANCE,
		"owner",	// OWNER,
		"activation",	// ACTIVATION,
		"newuser",	// NEWUSER,
		NULL,		// OTHER,
		NULL,		// FORMAT,
		NULL,		// VALUE
		NULL,		// REQUEST,
		NULL,		// TXNID,
		NULL,		// DESCR,
		NULL,		// ACCESS,
	},
	.listwords = 0,
	.combinations = balance_add_combis,
};

/* a2lets balance get ... */
static const char balance_get_usage [] = "domain A2DOMAIN currency CURRENCY\n"
	"( balance BALID | owner USERNAME )"
	;


static const uint32_t balance_get_combis [] = {
	/* Require the two basic coordiates. */
	FLAG_DOMAIN | FLAG_CURRENCY, FLAG_DOMAIN | FLAG_CURRENCY,
	/* To fetch one record:   Either balance or owner, not both. */
	FLAG_BALANCE | FLAG_OWNER, FLAG_BALANCE,
	FLAG_BALANCE | FLAG_OWNER, FLAG_OWNER,
	/* end marker */
	0
};

static const struct cmdparse_grammar balance_get_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		"currency",	// CURRENCY,
		"balance",	// BALANCE,
		"owner",	// OWNER,
		NULL,		// ACTIVATION,
		NULL,		// NEWUSER,
		NULL,		// OTHER,
		NULL,		// FORMAT,
		NULL,		// VALUE
		NULL,		// REQUEST,
		NULL,		// TXNID,
		NULL,		// DESCR,
		NULL,		// ACCESS,
	},
	.listwords = 0,
	.combinations = balance_get_combis,
};

/* a2lets balance set ... */
static const char balance_set_usage [] = "domain A2DOMAIN currency CURRENCY\n"
	"( balance BALID | owner USERNAME )\n"
	"activation ACTIVITY [access ACCESS]"
	;


static const uint32_t balance_set_combis [] = {
	/* Require the two basic coordiates. */
	FLAG_DOMAIN | FLAG_CURRENCY, FLAG_DOMAIN | FLAG_CURRENCY,
	/* Require the activation. */
	FLAG_ACTIVATION, FLAG_ACTIVATION,
	/* To identify one record:   Either balance or owner, not both. */
	FLAG_BALANCE | FLAG_OWNER, FLAG_BALANCE,
	FLAG_BALANCE | FLAG_OWNER, FLAG_OWNER,
	/* Do not require access.  Set to NULL/default if absent. */
	/* end marker */
	0
};

static const struct cmdparse_grammar balance_set_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		"currency",	// CURRENCY,
		"balance",	// BALANCE,
		"owner",	// OWNER,
		"activation",	// ACTIVATION,
		NULL,		// NEWUSER,
		NULL,		// OTHER,
		NULL,		// FORMAT,
		NULL,		// VALUE
		NULL,		// REQUEST,
		NULL,		// TXNID,
		NULL,		// DESCR,
		"access",	// ACCESS,
	},
	.listwords = FLAG_ACCESS,
	.combinations = balance_set_combis,
};

/* a2lets balance del ... */
static const char balance_del_usage [] = "domain A2DOMAIN currency CURRENCY\n"
	"( balance BALID | owner USERNAME )"
	;


static const uint32_t balance_del_combis [] = {
	/* Require the two basic coordiates. */
	FLAG_DOMAIN | FLAG_CURRENCY, FLAG_DOMAIN | FLAG_CURRENCY,
	/* To identify one record:   Either balance or owner, not both. */
	FLAG_BALANCE | FLAG_OWNER, FLAG_BALANCE,
	FLAG_BALANCE | FLAG_OWNER, FLAG_OWNER,
	/* end marker */
	0
};

static const struct cmdparse_grammar balance_del_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		"currency",	// CURRENCY,
		"balance",	// BALANCE,
		"owner",	// OWNER,
		NULL,		// ACTIVATION,
		NULL,		// NEWUSER,
		NULL,		// OTHER,
		NULL,		// FORMAT,
		NULL,		// VALUE
		NULL,		// REQUEST,
		NULL,		// TXNID,
		NULL,		// DESCR,
		NULL,		// ACCESS,
	},
	.listwords = 0,
	.combinations = balance_del_combis,
};

/* a2lets disbalance get ... */
static const char disbalance_get_usage [] = "domain A2DOMAIN currency CURRENCY\n"
	"( balance BALID | owner USERNAME )\n"
	"[ other USERNAME ]"
	;


static const uint32_t disbalance_get_combis [] = {
	/* Require the two basic coordiates. */
	FLAG_DOMAIN | FLAG_CURRENCY, FLAG_DOMAIN | FLAG_CURRENCY,
	/* Do not require the other username. */
	/* To identify one record:   Either balance or owner, not both. */
	FLAG_BALANCE | FLAG_OWNER, FLAG_BALANCE,
	FLAG_BALANCE | FLAG_OWNER, FLAG_OWNER,
	/* end marker */
	0
};

static const struct cmdparse_grammar disbalance_get_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		"currency",	// CURRENCY,
		"balance",	// BALANCE,
		"owner",	// OWNER,
		NULL,		// ACTIVATION,
		NULL,		// NEWUSER,
		"other",	// OTHER,
		NULL,		// FORMAT,
		NULL,		// VALUE
		NULL,		// REQUEST,
		NULL,		// TXNID,
		NULL,		// DESCR,
		NULL,		// ACCESS,
	},
	.listwords = 0,
	.combinations = disbalance_get_combis,
};


/* a2lets consume new ... */
static const char consume_new_usage [] = "domain A2DOMAIN currency CURRENCY\n"
	"( balance BALID | owner USERNAME )\n"
	"other USERNAME [ request REQID ]\n"
	"value TOKENS descr DESCR"
	;

static const uint32_t consume_new_combis [] = {
	/* Require the two basic coordiates. */
	FLAG_DOMAIN | FLAG_CURRENCY, FLAG_DOMAIN | FLAG_CURRENCY,
	/* To identify one record:   Either balance or owner, not both. */
	FLAG_BALANCE | FLAG_OWNER, FLAG_BALANCE,
	FLAG_BALANCE | FLAG_OWNER, FLAG_OWNER,
	/* The request identity is optional.
	 */
	/* Require the other username, value and descr. */
	FLAG_OTHER | FLAG_VALUE | FLAG_DESCR,
	FLAG_OTHER | FLAG_VALUE | FLAG_DESCR,
	/* end marker */
	0
};

static const struct cmdparse_grammar consume_new_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		"currency",	// CURRENCY,
		"balance",	// BALANCE,
		"owner",	// OWNER,
		NULL,		// ACTIVATION,
		NULL,		// NEWUSER,
		"other",	// OTHER,
		NULL,		// FORMAT,
		"value",	// VALUE
		"request",	// REQUEST,
		NULL,		// TXNID,
		"descr",	// DESCR,
	},
	.listwords = FLAG_DESCR,
	.combinations = consume_new_combis,
};


/* a2lets consume add|sub ... */
static const char consume_addsub_usage [] = "domain A2DOMAIN currency CURRENCY\n"
	"( balance BALID | owner USERNAME )\n"
	"transaction TXNID value TOKENS"
	;

static const uint32_t consume_addsub_combis [] = {
	/* Require the two basic coordiates. */
	FLAG_DOMAIN | FLAG_CURRENCY, FLAG_DOMAIN | FLAG_CURRENCY,
	/* To identify one record:   Either balance or owner, not both. */
	FLAG_BALANCE | FLAG_OWNER, FLAG_BALANCE,
	FLAG_BALANCE | FLAG_OWNER, FLAG_OWNER,
	/* The request identity is optional.
	 */
	/* Require the transaction identity and value change. */
	FLAG_TXNID | FLAG_VALUE,
	FLAG_TXNID | FLAG_VALUE,
	/* end marker */
	0
};

static const struct cmdparse_grammar consume_addsub_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		"currency",	// CURRENCY,
		"balance",	// BALANCE,
		"owner",	// OWNER,
		NULL,		// ACTIVATION,
		NULL,		// NEWUSER,
		NULL,		// OTHER,
		NULL,		// FORMAT,
		"value",	// VALUE
		NULL,		// REQUEST,
		"transaction",	// TXNID,
		NULL,		// DESCR,
		NULL,		// ACCESS,
	},
	.listwords = 0,
	.combinations = consume_addsub_combis,
};


/* a2lets consume get ... */
static const char consume_get_usage [] = "domain A2DOMAIN currency CURRENCY\n"
	"( balance BALID | owner USERNAME )\n"
	"[ other USERNAME ] [ request REQID ]"
	;

static const uint32_t consume_get_combis [] = {
	/* Require the two basic coordiates. */
	FLAG_DOMAIN | FLAG_CURRENCY, FLAG_DOMAIN | FLAG_CURRENCY,
	/* To identify one record:   Either balance or owner, not both. */
	FLAG_BALANCE | FLAG_OWNER, FLAG_BALANCE,
	FLAG_BALANCE | FLAG_OWNER, FLAG_OWNER,
	/* The request identity is optional.
	 */
	/* Do not require the other username. */
	/* end marker */
	0
};

static const struct cmdparse_grammar consume_get_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		"currency",	// CURRENCY,
		"balance",	// BALANCE,
		"owner",	// OWNER,
		NULL,		// ACTIVATION,
		NULL,		// NEWUSER,
		"other",	// OTHER,
		NULL,		// FORMAT,
		NULL,		// VALUE
		"request",	// REQUEST,
		NULL,		// TXNID,
		NULL,		// DESCR,
		NULL,		// ACCESS,
	},
	.listwords = 0,
	.combinations = consume_get_combis,
};


/* a2lets produce new|set ... */
static const char produce_newset_usage [] = "domain A2DOMAIN currency CURRENCY\n"
	"owner USERNAME\n"
	"[ other USERNAME ] request REQID\n"
	"value TOKENS descr DESCR offer OFFER"
	;

static const uint32_t produce_newset_combis [] = {
	/* Require the two basic coordiates. */
	FLAG_DOMAIN | FLAG_CURRENCY, FLAG_DOMAIN | FLAG_CURRENCY,
	/* To identify one record:   Require owner, not balance. */
	FLAG_OWNER, FLAG_OWNER,
	/* The other username   is optional.
	 */
	/* Require the reqid, value and descr. */
	FLAG_REQUEST | FLAG_VALUE | FLAG_DESCR | FLAG_OFFER,
	FLAG_REQUEST | FLAG_VALUE | FLAG_DESCR | FLAG_OFFER,
	/* end marker */
	0
};

static const struct cmdparse_grammar produce_newset_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		"currency",	// CURRENCY,
		NULL,		// BALANCE,
		"owner",	// OWNER,
		"offer",	// ACTIVATION==OFFER,
		NULL,		// NEWUSER,
		"other",	// OTHER,
		NULL,		// FORMAT,
		"value",	// VALUE
		"request",	// REQUEST,
		NULL,		// TXNID,
		"descr",	// DESCR,
		NULL,		// ACCESS,
	},
	.listwords = FLAG_DESCR,
	.combinations = produce_newset_combis,
};


/* a2lets produce del|get ... */
static const char produce_delget_usage [] = "domain A2DOMAIN currency CURRENCY\n"
	"owner USERNAME\n"
	"[ other USERNAME ] [ request REQID ] [ descr WORDS ]"
	;

static const uint32_t produce_delget_combis [] = {
	/* Require the two basic coordiates. */
	FLAG_DOMAIN | FLAG_CURRENCY, FLAG_DOMAIN | FLAG_CURRENCY,
	/* To identify one record:   Require owner, not balance. */
	FLAG_OWNER, FLAG_OWNER,
	/* end marker */
	0
};

static const struct cmdparse_grammar produce_delget_grammar = {
	.keywords = {
		"domain",	// DOMAIN,
		"currency",	// CURRENCY,
		NULL,		// BALANCE,
		"owner",	// OWNER,
		NULL,		// ACTIVATION,
		NULL,		// NEWUSER,
		"other",	// OTHER,
		NULL,		// FORMAT,
		NULL,		// VALUE
		"request",	// REQUEST,
		NULL,		// TXNID,
		"descr",	// DESCR,
		NULL,		// ACCESS,
	},
	.listwords = FLAG_DESCR,
	.combinations = produce_delget_combis,
};


/* a2lets domain ... */
static const struct cmdparse_action domain_syntax [] = {
	{ "new", OPCODE_NEW, &domain_newset_grammar, domain_newset_usage, domain_ops },
	{ "set", OPCODE_SET, &domain_newset_grammar, domain_newset_usage, domain_ops },
	{ "del", OPCODE_DEL, &domain_del_grammar,    domain_del_usage,    domain_ops },
	{ "get", OPCODE_GET, &domain_get_grammar,    domain_get_usage,    domain_ops },
		/* end marker */
	{ NULL, 0, NULL, NULL, NULL }
};

/* a2lets currency ... */
static const struct cmdparse_action currency_syntax [] = {
	{ "new", OPCODE_NEW, &currency_newset_grammar, currency_newset_usage, currency_ops },
	{ "set", OPCODE_SET, &currency_newset_grammar, currency_newset_usage, currency_ops },
	{ "del", OPCODE_DEL, &currency_del_grammar,    currency_del_usage,    currency_ops },
	{ "get", OPCODE_GET, &currency_get_grammar,    currency_get_usage,    currency_ops },
		/* end marker */
	{ NULL, 0, NULL, NULL, NULL }
};

/* a2lets balance ... */
static const struct cmdparse_action balance_syntax [] = {
	{ "new", OPCODE_NEW, &balance_new_grammar, balance_new_usage, balance_ops },
	{ "add", OPCODE_ADD, &balance_add_grammar, balance_add_usage, balance_ops },
	{ "get", OPCODE_GET, &balance_get_grammar, balance_get_usage, balance_ops },
	{ "set", OPCODE_SET, &balance_set_grammar, balance_set_usage, balance_ops },
	{ "del", OPCODE_DEL, &balance_del_grammar, balance_del_usage, balance_ops },
		/* end marker */
	{ NULL, 0, NULL, NULL, NULL }
};

/* a2lets disbalance ... */
static const struct cmdparse_action disbalance_syntax [] = {
	{ "get", OPCODE_GET, &disbalance_get_grammar, disbalance_get_usage, disbalance_ops },
		/* end marker */
	{ NULL, 0, NULL, NULL, NULL }
};

/* a2lets consume ... */
static const struct cmdparse_action consume_syntax [] = {
	{ "new", OPCODE_NEW, &consume_new_grammar, consume_new_usage, consume_ops },
	{ "add", OPCODE_ADD, &consume_addsub_grammar, consume_addsub_usage, consume_ops },
	{ "sub", OPCODE_SUB, &consume_addsub_grammar, consume_addsub_usage, consume_ops },
	{ "get", OPCODE_GET, &consume_get_grammar, consume_get_usage, consume_ops },
		/* end marker */
	{ NULL, 0, NULL, NULL, NULL }
};

/* a2lets produce ... */
static const struct cmdparse_action produce_syntax [] = {
	{ "new", OPCODE_NEW, &produce_newset_grammar, produce_newset_usage, produce_ops },
	//TODO// { "set", OPCODE_SET, &produce_newset_grammar, produce_newset_usage, produce_ops },
	//TODO// { "del", OPCODE_DEL, &produce_delget_grammar, produce_delget_usage, produce_ops },
	{ "get", OPCODE_GET, &produce_delget_grammar, produce_delget_usage, produce_ops },
		/* end marker */
	{ NULL, 0, NULL, NULL, NULL }
};

/* a2lets ... */
static const struct cmdparse_class a2lets_syntax [] = {
	{ "domain",     domain_usage,     domain_syntax     },
	{ "currency",   currency_usage,   currency_syntax   },
	{ "balance",    balance_usage,    balance_syntax    },
	{ "consume",    consume_usage,    consume_syntax    },
	{ "produce",    produce_usage,    produce_syntax    },
	{ "disbalance", disbalance_usage, disbalance_syntax },
		/* end marker */
	{ NULL, NULL, NULL }
};


/*
 *
 * PLUGIN FUNCTIONS FOR SQLITE3: MAYDO/3
 *
 */



/* Reference the SQLite3 API functions.  Filled by sqlite3_maydo_entry().
 */
static const sqlite3_api_routines *sqlite3_api = NULL;


/* Access Control testing with boolean function maydo(authn_user,rule,minrights).
 *
 * The REMOTE_USER is retrieved as 1st argument to make this deterministic.
 *
 * This code may be used as part of queries, to select who may see what.
 */
static void sqlite3_maydo (sqlite3_context *ctx, int argc, sqlite3_value **argv) {
	//
	// Initialisation
	bool retval = false;
	char *errmsg = NULL;
	int minrights = 0;
	a2sel_t *authn_user = NULL;
	bool free_user = false;
	//
	// Check for sanity
	assert (argc == 3);
	//
	// Fetch the selector in TEXT or BLOB form (1st arg)
	// where the BLOB is assumed to represent a parsed a2sel_t
	switch (sqlite3_value_type (argv [0])) {
	case SQLITE_NULL:
		errmsg = "MAYDO got NULL in authn_user (1st arg)";
		goto fail;
	case SQLITE_TEXT:
		authn_user = sqlite3_malloc (sizeof (a2sel_t));
		memset (authn_user, 0, sizeof (a2sel_t));
		free_user = true;
		if (!a2sel_parse (authn_user, sqlite3_value_text (argv [0]), 0)) {
			errmsg = "MAYDO failed to parse authn_user (1st arg)";
			goto fail;
		}
		break;
	case SQLITE_BLOB:
		if (sqlite3_value_bytes (argv [0]) != sizeof (a2sel_t)) {
			errmsg = "MAYDO got wrong BLOB in authn_user (1st arg)";
			goto fail;
		}
		authn_user = (a2sel_t *) sqlite3_value_blob (argv [0]);
		break;
	default:
		errmsg = "MAYDO expects TEXT or BLOB in authn_user (1st arg)";
		goto fail;
	}
	//
	// Fetch the rule and treat it like a ruleset (2nd arg)
	const char *rule = sqlite3_value_text  (argv [1])    ;
	int rulesetlen   = sqlite3_value_bytes (argv [1]) + 1;
	//
	// Fetch the minrights in INTEGER or TEXT form (3rd arg)
	switch (sqlite3_value_type (argv [2])) {
	case SQLITE_NULL:
		errmsg = "MAYDO got NULL for minrights (3rd arg)";
		goto fail;
	case SQLITE_INTEGER:
		minrights = sqlite3_value_int64 (argv [2]);
		break;
	case SQLITE_TEXT:
		if (!access_parse ((char *) sqlite3_value_text (argv [2]), &minrights)) {
			errmsg = "MAYDO failed to parse TEXT in minrights (3rd arg)";
			goto fail;
		}
		break;
	default:
		errmsg = "MAYDO expects INTEGER or TEXT in minrights (3rd arg)";
		goto fail;
	}
	//
	// Process the rule to find Access Rights
	access_rights actrights = 0;
	if (!lets_rule_process (authn_user, rule, rulesetlen, &actrights)) {
		errmsg = "MAYDO failed to process rule (2nd arg)";
		goto fail;
	}
	//
	// Check if the Access Rights suffice
	retval = maydo (actrights, minrights);
	//
	// Return with success
	sqlite3_result_int (ctx, retval ? 1 : 0);
	return;
	//
	// Report errmsg and then return
fail:
	if (free_user) {
		sqlite3_free (authn_user);
		authn_user = NULL;
	}
	sqlite3_result_error (ctx, errmsg, -1);
}


/* Register the extension function as part of database opening.
 */
static int sqlite3_maydo_entry (sqlite3 *db, const char **errmsg, const struct sqlite3_api_routines *api) {
	//
	// Register the API routines (assuming not variable across databases)
	assert (api != NULL);
	sqlite3_api = api;
	//
	// Extend SQLite3 with functionity for Access Control
	int dberr = sqlite3_create_function (
			db,
			"maydo",
			3,	/* ...(authn_user,rule,minrights) */
			SQLITE_UTF8 | SQLITE_DETERMINISTIC | SQLITE_DIRECTONLY,
			NULL,	/* application-specific pointer */
			sqlite3_maydo, NULL, NULL);
	//
	// Check if an error occurred while registering the extension function
	if (dberr != SQLITE_OK) {
		log_error ("MAYDO extension rejected by database: %s\n",
				sqlite3_errmsg (db));
		wrap_status (500, "Database extension rejected");
	}
	return dberr;
}



/*
 *
 * MAIN PROGRAM
 *
 */



/* Main routine.  Accept request(s) and process them.
 *
 * There are three cases:
 *  - lets.fcgi  --> process as shell command
 *  - fd0 socket --> process as FastCGI
 *  - fd0 file   --> process as CGI
 */
int main (int argc, char *argv []) {
	//
	// Setup libraries
	access_init ();
	sqlite3_auto_extension ((void(*)(void)) sqlite3_maydo_entry);
	//
	// Open the SQLite3 database
	char *lets_db = getenv ("ARPA2_LETS_DB");
	if (lets_db == NULL) {
		lets_db = LETS_DB;
	}
	int dberr = sqlite3_open_v2 (
				lets_db,
				&db,
				SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOFOLLOW,
				NULL);
	if (dberr != SQLITE_OK) {
		log_error ("Database did not open: %s\n",
				sqlite3_errmsg (db));
		wrap_status (500, "Database did not open");
		goto db_fail;
	}
	//
	// Split between (Fast)CGI or shell command
	int namelen = strlen (argv [0]);
	bool fcgi = (namelen > 5) && (0 == strcmp (argv [0] + namelen - 5, ".fcgi"));
	if (fcgi) {
		//
		// Redirect stderr to a log file
		FILE *new_stderr = fopen ("/tmp/a2lets.log", "a");
		if (new_stderr != NULL) {
			fclose (stderr);
			stderr = new_stderr;
		}
		//
		// One-shot CGI or main loop for FastCGI
		while (0 == wrap_accept ()) {
			//
			// While debugging, produce plaintext output
			#ifdef DEBUG
			wrap_printf ("Content-Type: text/plain\n\n");
			#endif
			//
			// Construct additional arguments
			int   fcgi_argc = 0;
			char *fcgi_argv [5];
			char *domain = getenv ("ARPA2_DOMAIN");
			if (domain != NULL) {
				fcgi_argv [fcgi_argc++] = "domain";
				fcgi_argv [fcgi_argc++] = domain;
			}
			char *curry = getenv ("ARPA2_LETS_CURRENCY");
			if (curry != NULL) {
				fcgi_argv [fcgi_argc++] = "currency";
				fcgi_argv [fcgi_argc++] = curry;
			}
			fcgi_argv [fcgi_argc] = NULL;
			//
			// Parse CGI arguments and run a command for it
			bool txnok = txn_begin (db);
			txnok = txnok && cmdparse_class_action_cgi (fcgi_argc, fcgi_argv, a2lets_usage, a2lets_syntax);
			if (!txnok) {
				log_error ("Transaction failed: SQLite3 reports %s", sqlite3_errmsg (db));
fprintf (stderr, "Transaction failed: SQLite3 reports %s", sqlite3_errmsg (db));
				txn_rollback (db);
			} else if (txn_commit (db)) {
				txn_rollback (db);
				log_error ("Transaction commit failed: %s", sqlite3_errmsg (db));
			}
		}
	} else {
		//
		// One-shot command invocation
		if (!cmdparse_class_action (argc, argv, a2lets_usage, a2lets_syntax)) {
			log_error ("Command failed");
			wrap_status (400, "Request Failed");
		}
	}
	//
	// Close the SQLite3 database
db_fail:
	sqlite3_close (db);
	db = NULL;
	//
	// Cleanup libraries
	access_fini ();
	//
	// Exit without prejudice
	wrap_exit ();
}

