/* domain.c -- Switching code for the API to ARPA2 LETS.
 *
 * Domain Operations:
 *
 * a2lets domain new|del domain a2domain
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 * SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <sqlite3.h>

#include <arpa2/identity.h>
#include <arpa2/access.h>
#include <arpa2/util/cmdparse.h>

#include "shared.h"
#include "switch.h"




bool do_domain (sqlite3 *db, opcode_t opcode, struct cmdparser *prs) {
	bool ok = true;
	//
	// Static storage for prepared SQL
	static struct prepsql ps_new, ps_set, ps_get, ps_del_dom, ps_del_cur, ps_del_usr, ps_del_bal, ps_del_hst, ps_del_req;
	//
	// Find the authenticated user
	access_rights domxs = 0;
	a2sel_t authn_user;
	ok = ok && authenticate_user (&authn_user);
	//
	// Map the domain name to a domain identity
	int64_t domid = ok ? domain2domid (db, prs->VAL_DOMAIN, &domxs, &authn_user) : -1;
	if ((domid < 0) || (opcode == OPCODE_DEL)) {
		domxs = ACCESS_VISITOR;
		ok = ok && envvar_admin_rights (&authn_user, &domxs);
	}
	if (opcode == OPCODE_NEW) {
		ok = ok && (domid <  0);
	} else if (opcode == OPCODE_GET) {
		/* No constraints on domid */ ;
	} else {
		ok = ok && (domid >= 0);
	}
	//
	// Split into the various queries
	bool xs = true;
	switch (opcode) {
	case OPCODE_NEW:
		xs = xs && ((!ok) || maydo (domxs, ACCESS_ADMIN));
		ok = ok && xs;
		ok = have_prepsql (&ps_new, db, "INSERT INTO dommap (domain,access) VALUES (?1,?2)", "ss", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_new, prs->VAL_DOMAIN, prs->VAL_ACCESS));
		break;
	case OPCODE_SET:
		xs = xs && ((!ok) || maydo (domxs, ACCESS_CONFIG) || (maydo (domxs, ACCESS_ADMIN)));
		ok = ok && xs;
		ok = have_prepsql (&ps_set, db, "UPDATE dommap SET access=?2 WHERE domain=?1", "ss", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_set, prs->VAL_DOMAIN, prs->VAL_ACCESS));
		break;
	case OPCODE_DEL:
		/* This deletes an object and all references; does not require AUTOINCREMENT */
		xs = xs && ((!ok) || maydo (domxs, ACCESS_DELETE) || maydo (domxs, ACCESS_ADMIN));
		ok = ok && xs;
		ok = have_prepsql (&ps_del_req, db, "DELETE FROM request WHERE curid IN (SELECT curid FROM curmap WHERE domid=?)", "i", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_del_req, domid));
		ok = have_prepsql (&ps_del_hst, db, "DELETE FROM history WHERE curid IN (SELECT curid FROM curmap WHERE domid=?)", "i", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_del_hst, domid));
		ok = have_prepsql (&ps_del_bal, db, "DELETE FROM balance WHERE curid IN (SELECT curid FROM curmap WHERE domid=?)", "i", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_del_bal, domid));
		ok = have_prepsql (&ps_del_usr, db, "DELETE FROM usrmap WHERE domid=?", "i", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_del_usr, domid));
		ok = have_prepsql (&ps_del_cur, db, "DELETE FROM curmap WHERE domid=?", "i", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_del_cur, domid));
		ok = have_prepsql (&ps_del_dom, db, "DELETE FROM dommap WHERE domid=?", "i", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_del_dom, domid));
		break;
	case OPCODE_GET:
		xs = xs && ((!ok) || maydo (domxs, ACCESS_READ) || maydo (domxs, ACCESS_ADMIN));
		ok = ok && xs;
		ok = have_prepsql (&ps_get, db,
				"SELECT"
				"  domain,"
				"  '+lets@'||domain AS url,"
				"  access\n"
				"FROM"
				"  dommap\n"
				"WHERE"
				/* Note: Admin option prepared when domid is -1 */
				"  (domid = ?1 OR ?1=-1)",
					"i", "sss") && ok;
		if (ok) {
			table_prepsql (&ps_get, bindstep_prepsql (&ps_get, domid), "[,,");
		}
		break;
	default:
		ok = false;
	}
	//
	// Return the result
	if (!xs) {
		wrap_status (403, "ARPA2 Access Denied");
	}
	return ok;
}

