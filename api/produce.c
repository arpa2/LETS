/* produce.c -- Switching code for the API to ARPA2 LETS.
 *
 * Producer Operations:
 *
 * a2lets produce new|set domain a2domain
 *	currency currency owner username
 *	[ other username ] [ request reqid ]
 *	value tokens descr comment
 *
 * a2lets produce del|get domain a2domain
 *	currency currency owner username
 *	[ request reqid ] [ descr partial ]
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 * SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <time.h>

#include <sqlite3.h>

#include <arpa2/identity.h>
#include <arpa2/access.h>
#include <arpa2/util/cmdparse.h>

#include "shared.h"
#include "switch.h"



bool do_produce (sqlite3 *db, opcode_t opcode, struct cmdparser *prs) {
	bool ok = true;
	//
	// Static storage for prepared SQL
	static struct prepsql ps_new, ps_set, ps_del, ps_get;
	//
	// Find the authenticated user
	access_rights domxs = 0;
	access_rights curxs = 0;
	access_rights balxs = 0;
	a2sel_t authn_user;
	ok = ok && authenticate_user (&authn_user);
	//
	// Map the domain name to a domain identity
	int64_t domid = domain2domid (db, prs->VAL_DOMAIN, &domxs, &authn_user);
	ok = ok && (domid >= 0);
	//
	// Map the domain and currency to a currency identity
	int64_t curid = ok ? domaincurrency2curid (db, prs->VAL_DOMAIN, prs->VAL_CURRENCY, &curxs, &authn_user) : -1;
	ok = ok && (curid >= 0);
	//
	// (a) If the owner's username is given, derive the ownui from that
	int64_t ownui = -1;
	int64_t balid = -1;
	if (prs->VAL_OWNER != NULL) {
		/* Note: ownui < 0 propagates with balid < 0 */
		ownui = domainuser2usrid (db, prs->VAL_DOMAIN, prs->VAL_OWNER, -1);
		balid = usridcurid2balid (db, ownui, curid, true, &balxs, &authn_user);
	}
	//
	// (b) Alternatively, parse the balance identity
	if (prs->VAL_BALANCE != NULL) {
		balid = string2xxxid (prs->VAL_BALANCE);
	}
	//
	// In any case, we now want to have a balance
	ok = ok && (balid >= 0);
	//
	// Find the other user's usrid and balid
	int64_t othui = domainuser2usrid (db, prs->VAL_DOMAIN, prs->VAL_OTHER, -1);
	int64_t othbi = usridcurid2balid (db, othui, curid, true, NULL, NULL);
	//
	// Find the optional request identity, set -1 if not given
	// Map to a string representation for use as a SQLite3 literal
	int64_t reqid = -1;
	if (prs->VAL_REQUEST != NULL) {
		reqid = string2xxxid (prs->VAL_REQUEST);
		ok = ok && (reqid >= 0);
	}
	const char *request = (reqid >= 0) ? prs->VAL_REQUEST : NULL;
	//
	// Find the value as a number of tokens -- if required, demand > 0
	int64_t amt = 0;
	if (prs->VAL_VALUE != NULL) {
		amt = string2xxxid (prs->VAL_VALUE);
		ok = ok && (amt > 0);
	}
	//
	// Sample a value for offer, or use a default
	//TODO// Constrain old offer setting
	bool need_admin = false;
	const char *offer = "N";
	if (prs->VAL_OFFER != NULL) {
		switch (prs->VAL_OFFER [0]) {
		case 'N':
		case 'n':
			offer = "N";
			break;
		case 'U':
		case 'u':
			need_admin = true;
			/* Continue as for 'Y' */
		case 'Y':
		case 'y':
			offer = "Y";
			break;
		case 'C':
		case 'c':
			offer = "C";
			break;
		case 'B':
		case 'b':
			need_admin = true;
			offer = "B";
			break;
		default:
			ok = false;
		}
		ok = ok && (prs->VAL_OFFER [1] == '\0');
	}
	//
	// If a descr is given, make it partial in a LIKE pattern
	int descrlen = (prs->VAL_DESCR == NULL) ? 0 : strlen (prs->VAL_DESCR);
	char descr2like [descrlen + 3];
	char *descr2drop;
	if (prs->VAL_DESCR != NULL) {
		sprintf (descr2like, "%%%s%%", prs->VAL_DESCR);
		descr2drop = prs->VAL_DESCR;
	} else {
		strcpy (descr2like, "%");
		descr2drop = "NULL";
	}
	//
	// Split into the various queries
	bool xs = true;
	switch (opcode) {
	case OPCODE_NEW:
		xs = xs && ((!ok) || maydo (balxs, ACCESS_WRITE));
		ok = ok && xs;
		ok = have_prepsql (&ps_new, db, "INSERT INTO request (reqid,prod_usrid,cons_usrid,offer,curid,value,descr) VALUES (?,?,?,?,?,?,?)", "iiisiis", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_new, reqid,ownui,othui,offer,curid,amt,prs->VAL_DESCR));
		break;
#if 0
	case OPCODE_SET:
		xs = xs && ((!ok) || maydo (balxs, ACCESS_WRITE));
		ok = ok && xs;
		break;
#endif
	case OPCODE_GET:
		ok = have_prepsql (&ps_get, db,
				"SELECT"
				"  ?1 AS domain,"
				"  ?2 AS currency,"
				"  IFNULL(prod.username,'') AS producer,"
				"  IFNULL(cons.username,'') AS consumer,"
				"  reqid,"
				"  offer,"
				"  value,"
				"  descr AS description\n"
				"FROM"
				"  request req\n"
				"LEFT JOIN usrmap prod ON prod.usrid = req.prod_usrid\n"
				"LEFT JOIN usrmap cons ON cons.usrid = req.cons_usrid\n"
				"LEFT JOIN balance bal ON prod.balid = bal.balid\n"
				/* Prepared for admin mode when balid = -1 */
				"WHERE (req.prod_usrid = ?3 OR req.cons_usrid = ?3 OR ?3 = -1)"
				"  AND (req.cons_usrid = ?4 OR req.prod_usrid = ?4 OR ?4 = -1 OR req.cons_usrid IS NULL)"
				"  AND (req.curid = ?5)"
				"  AND (req.reqid = ?6 OR ?6 = -1)"
				"  AND (req.descr LIKE ?7)"
				"  AND (maydo (?8,IFNULL(bal.access,'%R ~@'||?1),?9))\n"
				"ORDER BY domain, currency, producer, consumer, reqid",
					"ssiiiisbi", "ssssisis") && ok;
		table_prepsql (&ps_get, bindstep_prepsql (&ps_get,
						prs->VAL_DOMAIN,
						prs->VAL_CURRENCY,
						ownui,
						othui,
						curid,
						reqid,
						descr2like,
						&authn_user, sizeof (authn_user),
						ACCESS_READ),
				",,{{[,,,");
		break;
	case OPCODE_DEL:
		/* This uses manually defined reqid, so no need for AUTOINCREMENT */
		xs = xs && ((!ok) || maydo (balxs, ACCESS_WRITE));
		ok = ok && xs;
		ok = have_prepsql (&ps_del, db,
				"DELETE FROM request\n"
				"WHERE req.prod_usrid = ?1"
				"  AND (req.cons_usrid = ?2 OR (?2 = -1 AND req.cons_usrid IS NULL))"
				"  AND (req.curid = ?3)"
				"  AND (req.reqid = ?4 OR ?4 = -1)"
				"  AND (req.descr LIKE ?5)"
				"ORDER BY domain, currency, producer, consumer, reqid",
					"ssiiiis", "ssssisis") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_del,
						ownui,
						othui,
						curid,
						reqid,
						descr2drop));
		break;
	default:
		ok = false;
	}
	//
	// Return the result
	if (!xs) {
		wrap_status (403, "ARPA2 Access Denied");
	}
	return ok;
}

