/* currency.c -- Switching code for the API to ARPA2 LETS.
 *
 * Currency Operations:
 *
 * a2lets currency new|set|del domain a2domain
 *	currency currency [format fmtstring]
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 * SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <sqlite3.h>

#include <arpa2/identity.h>
#include <arpa2/access.h>
#include <arpa2/util/cmdparse.h>

#include "shared.h"
#include "switch.h"




bool do_currency (sqlite3 *db, opcode_t opcode, struct cmdparser *prs) {
	bool ok = true;
	//
	// Static storage for prepared SQL
	static struct prepsql ps_new, ps_set, ps_get, ps_get_acl, ps_del_cur, ps_del_bal, ps_del_usr, ps_del_hst, ps_del_req;
	//
	// Find the authenticated user
	access_rights domxs = 0;
	access_rights curxs = 0;
	a2sel_t authn_user;
	ok = ok && authenticate_user (&authn_user);
	//
	// Map the domain name to a domain identity
	//TODO// Only required for OPCODE_NEW, otherwise a safety check
	int64_t domid = ok ? domain2domid (db, prs->VAL_DOMAIN, &domxs, &authn_user) : -1;
	ok = ok && (domid >= 0);
	//
	// Map the domain and currency to a currency identity
	int64_t curid = -1;
	if (prs->VAL_CURRENCY != NULL) {
		curid = ok ? domaincurrency2curid (db, prs->VAL_DOMAIN, prs->VAL_CURRENCY, &curxs, &authn_user) : -1;
	}
	if (opcode == OPCODE_GET) {
		ok = ok && ((prs->VAL_CURRENCY == NULL) || (curid >= 0));
	} else if (opcode == OPCODE_NEW) {
		ok = ok && (curid <  0);
	} else {
		ok = ok && (curid >= 0);
	}
	//
	// Split into the various queries
	bool xs = true;
	switch (opcode) {
	case OPCODE_NEW:
		xs = xs && ((!ok) || maydo (domxs, ACCESS_CREATE) || maydo (domxs, ACCESS_ADMIN));
		ok = ok && xs;
		ok = have_prepsql (&ps_new, db, "INSERT INTO curmap (domid,currency,format,access) VALUES (?,?,?,?)", "isss", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_new, domid, prs->VAL_CURRENCY, prs->VAL_FORMAT, prs->VAL_ACCESS));
		break;
	case OPCODE_SET:
		xs = xs && ((!ok) || maydo (curxs, ACCESS_CONFIG));
		ok = ok && xs;
		ok = have_prepsql (&ps_set, db, "UPDATE curmap SET format=IFNULL(?1,format), access=?2 WHERE curid=?3", "ssi", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_set, prs->VAL_FORMAT, prs->VAL_ACCESS, curid));
		break;
	case OPCODE_DEL:
		/* This deletes an object and all references; does not require AUTOINCREMENT */
		xs = xs && ((!ok) || maydo (curxs, ACCESS_DELETE) || maydo (curxs, ACCESS_ADMIN));
		ok = ok && xs;
		ok = have_prepsql (&ps_del_req, db, "DELETE FROM request WHERE curid = ?", "i", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_del_req, curid));
		ok = have_prepsql (&ps_del_hst, db, "DELETE FROM history WHERE curid = ?", "i", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_del_hst, curid));
		ok = have_prepsql (&ps_del_usr, db, "DELETE FROM usrmap  WHERE balid IN (SELECT balid FROM balance WHERE curid = ?)", "i", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_del_usr, curid));
		ok = have_prepsql (&ps_del_bal, db, "DELETE FROM balance WHERE curid = ?", "i", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_del_bal, curid));
		ok = have_prepsql (&ps_del_cur, db, "DELETE FROM curmap WHERE curid=?", "i", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_del_cur, curid));
		break;
	case OPCODE_GET:
		// Check for access to OPCODE_SET but do not make it fatal;
		// it is used to modify visibility of the "access" policy rule
		xs = xs && (curid >= 0) && ((!ok) || maydo (curxs, ACCESS_CONFIG));
		ok = have_prepsql (&ps_get, db,
				"SELECT"
				"  ?1 AS domain,"
				"  currency,"
				"  format,"
				"  '+lets+'||currency||'@'||?1 AS url\n"
				"FROM"
				"  curmap\n"
				/* Note: Administrator option prepared using curid==-1 */
				"WHERE (curid=?3 OR ?3=-1)",
					"ssi", "ssss") && ok;
		ok = have_prepsql (&ps_get_acl, db,
				"SELECT"
				"  ?1 AS domain,"
				"  currency,"
				"  format,"
				"  '+lets+'||currency||'@'||?1 AS url,"
				"  access\n"
				"FROM"
				"  curmap\n"
				/* Note: Administrator option prepared using curid==-1 */
				"WHERE (curid=?3 OR ?3=-1)",
					"ssi", "sssss") && ok;
		if (ok) {
			int dbstate = bindstep_prepsql (xs ? &ps_get_acl : &ps_get, prs->VAL_DOMAIN, prs->VAL_CURRENCY, curid);
			table_prepsql (xs ? &ps_get_acl : &ps_get, dbstate, xs ? "[,,,," : "[,,,");
		}
		// Do not complain about access, since this was not followed up on
		xs = true;
		break;
	default:
		ok = false;
	}
	//
	// Return the result
	if (!xs) {
		wrap_status (403, "ARPA2 Access Denied");
	}
	return ok;
}


