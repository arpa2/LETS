/* wrapio.h -- FastCGI switching code for the API to ARPA2 LETS.
 *
 * Generic (Fast)CGI handling, basically FCGI_Accept() and FCGI_vprintf().
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 * SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
 */


/* Content types as enumerated values.
 */
enum wrap_content_type {
	/* uninitialised */
	CONTENT_UNKNOWN = 0,
	/* actual values */
	CONTENT_PLAIN,
	CONTENT_CSV,
	CONTENT_HTML,
	CONTENT_JSON,
	/* end marker */
	CONTENT_COUNT
};


/* Accept a FastCGI or CGI run.
 */
int wrap_accept (void);


/* Provide Content-Type.  For CONTENT_UNKNOWN, negotiate with Accept:
 */
enum wrap_content_type wrap_content_type (enum wrap_content_type ctype);


/* Set a status code and an optional formatted error message.
 * The status code is used for HTTP, the error message
 * is for local reporting.
 */
void wrap_status (int status, const char *fmt, ...);


/* Formatted print on the desired output channel, which may be
 * stdout or get sent away via FastCGI, as appropriate.
 *
 * Print is wrapped so we can hide FastCGI redefinitions.
 */
int wrap_printf (char *fmt, ...);


/* Exit the program, deriving the exit code from reported HTTP status.
 */
void wrap_exit (void);

