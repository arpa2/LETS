/* disbalance.c -- Switching code for the API to ARPA2 LETS.
 *
 * Disbalance Operations:
 *
 * a2lets disbalance get domain a2domain
 *	currency currency ( balance balid | owner username )
 *	other username
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 * SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <time.h>

#include <sqlite3.h>

#include <arpa2/identity.h>
#include <arpa2/access.h>
#include <arpa2/util/cmdparse.h>

#include "shared.h"
#include "switch.h"



bool do_disbalance (sqlite3 *db, opcode_t opcode, struct cmdparser *prs) {
	bool ok = true;
	//
	// Static storage for prepared SQL
	static struct prepsql ps_get;
	//
	// Find the authenticated user
	access_rights domxs = 0;
	access_rights curxs = 0;
	access_rights balxs = 0;
	a2sel_t authn_user;
	ok = ok && authenticate_user (&authn_user);
	//
	// Map the domain name to a domain identity
	int64_t domid = domain2domid (db, prs->VAL_DOMAIN, &domxs, &authn_user);
	ok = ok && (domid >= 0);
	//
	// Map the domain and currency to a currency identity
	int64_t curid = ok ? domaincurrency2curid (db, prs->VAL_DOMAIN, prs->VAL_CURRENCY, &curxs, &authn_user) : -1;
	ok = ok && (curid >= 0);
	//
	// (a) If the owner's username is given, derive the ownid from that
	int64_t ownid = -1;
	int64_t balid = -1;
	if (prs->VAL_OWNER != NULL) {
		/* Note: ownid < 0 propagates with balid < 0 */
		ownid = domainuser2usrid (db, prs->VAL_DOMAIN, prs->VAL_OWNER, -1);
		balid = usridcurid2balid (db, ownid, curid, true, &balxs, &authn_user);
	}
	//
	// (b) Alternatively, parse the balance identity
	if (prs->VAL_BALANCE != NULL) {
		balid = string2xxxid (prs->VAL_BALANCE);
	}
	//
	// In any case, we now want to have a balance
	ok = ok && (balid >= 0);
	//
	// Find the other user's balance, if given
	int64_t othid = -1;
	if (prs->VAL_OTHER != NULL) {
		othid = usridcurid2balid (db,
				/* Note: returned value < 0 propagates to othid < 0 */
				domainuser2usrid (db,
					prs->VAL_DOMAIN,
					prs->VAL_OTHER, -1),
				curid,
				true, NULL, NULL);
		ok = ok && (othid >= 0);
	}
	//
	// Split into the various queries
	switch (opcode) {
	case OPCODE_GET:
		ok = have_prepsql (&ps_get, db,
				"SELECT"
				"  ?1 AS domain,"
				"  c.currency AS currency,"
				"  c.format AS format,"
				"  '+lets+'||c.currency||'+'||CAST(bm.balid AS VARCHAR(64)) AS moi,"
				"  '+lets+'||c.currency||'+'||CAST(bt.balid AS VARCHAR(64)) AS toi,"
				"  bt.balance - bm.balance AS disbalance\n"
				"FROM"
				"  balance bm,"
				"  balance bt,"
				"  curmap c\n"
				"WHERE c.curid = ?2"
				"  AND bm.curid = c.curid"
				"  AND bt.curid = c.curid"
				//TODO// Future extension: admins could use ownid==-1 as ?-3
				"  AND ( bm.balid = ?3 OR ?3 = -1)"
				"  AND ( bt.balid = ?4 OR ?4 = -1)"
				"  AND maydo (?5, bt.access, ?6)\n"
				"ORDER BY bm.balid ASC, bt.balid ASC",
					"siiibi", "sssssi") && ok;
		int dbstatus = ok ? bindstep_prepsql (&ps_get, prs->VAL_DOMAIN, curid, ownid, othid, &authn_user, sizeof (authn_user), ACCESS_READ) : SQLITE_ERROR;
		table_prepsql (&ps_get, dbstatus, ",,,{{,");
		break;
	default:
		ok = false;
	}
	//
	// Return the result
	return ok;
}

