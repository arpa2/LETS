/* balance.c -- Switching code for the API to ARPA2 LETS.
 *
 * Balance Operations:
 *
 * a2lets balance new|add|get|set|del domain a2domain
 *	currency currency [ balance balid | owner username ]
 *	[user username] [activation activity]
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 * SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <sqlite3.h>

#include <arpa2/identity.h>
#include <arpa2/access.h>
#include <arpa2/util/cmdparse.h>

#include "shared.h"
#include "switch.h"




bool do_balance (sqlite3 *db, opcode_t opcode, struct cmdparser *prs) {
	bool ok = true;
	//
	// Static storage for prepared SQL
	static struct prepsql ps_new, ps_mew, ps_add, ps_get, ps_set, ps_del, ps_del_admin, ps_del_gc;
	//
	// Find the authenticated user
	access_rights domxs = 0;
	access_rights curxs = 0;
	access_rights balxs = 0;
	a2sel_t authn_user;
	ok = ok && authenticate_user (&authn_user);
	//
	// We sometimes need the domain identity, and it is always a good check
	int64_t domid = domain2domid (db, prs->VAL_DOMAIN, &domxs, &authn_user);
	ok = ok && (domid >= 0);
	//
	//
	// Map the domain and currency to a currency identity
	int64_t curid = ok ? domaincurrency2curid (db, prs->VAL_DOMAIN, prs->VAL_CURRENCY, &curxs, &authn_user) : -1;
	ok = ok && (curid >= 0);
	// (a) If the owner's username is given, derive the ownid and balxs from that
	int64_t ownid = -1;
	int64_t balid = -1;
	if (prs->VAL_OWNER != NULL) {
		/* Note: ownid < 0 propagates with balid < 0 */
		ownid = domainuser2usrid (db, prs->VAL_DOMAIN, prs->VAL_OWNER, -1);
		balid = usridcurid2balid (db, ownid, curid, false, &balxs, &authn_user);
	}
	//
	// (b) Alternatively, parse the balance identity
	if (prs->VAL_BALANCE != NULL) {
		balid = string2xxxid (prs->VAL_BALANCE);
		balxs = balid2balxs (db, balid, &authn_user);
	}
	//
	// We should now have a proper balance identity, except when creating a new one
	if (opcode == OPCODE_NEW) {
		ok = ok && (balid <  0);
	} else {
		ok = ok && (balid >= 0);
	}
	//
	// Ensure that a new user identity does not yet exist
	if (prs->VAL_NEWUSER != NULL) {
		int64_t newid = domainuser2usrid (db, prs->VAL_DOMAIN, prs->VAL_NEWUSER, -1);
		ok = ok && (newid < 0);
	}
	//
	// Sample a value for activity, or use a default
	bool need_admin = false;
	const char *activity = "N";
	const char *oldactvy  = "";
	if (prs->VAL_ACTIVATION != NULL) {
		switch (prs->VAL_ACTIVATION [0]) {
		case 'N':
		case 'n':
			activity = "N";
			oldactvy = "Y";
			break;
		case 'U':
		case 'u':
			need_admin = true;
			/* Continue as for 'Y' */
		case 'Y':
		case 'y':
			activity = "Y";
			oldactvy = need_admin ? "B" : "N";
			break;
		case 'B':
		case 'b':
			need_admin = true;
			activity = "B";
			oldactvy = "YN";
			break;
		default:
			ok = false;
		}
		ok = ok && (prs->VAL_ACTIVATION [1] == '\0');
	}
	//
	// Split into the various queries
	bool xs = true;
	switch (opcode) {
	case OPCODE_NEW:
		/* Active new users are setup with an average balance */
		xs = xs && ((!ok) || maydo (curxs, ACCESS_CREATE));
		ok = ok && xs;
		ok = have_prepsql (&ps_new, db,
				"INSERT INTO balance (curid,activity,access,balance)\n"
				"VALUES ("
				"  ?1,"
				"  ?2,"
				"  ?3,"
				"  CASE ?2"
				"    WHEN 'Y' THEN ("
				"      SELECT IFNULL (AVG (others.balance), 0)"
				"      FROM balance others"
				"      WHERE others.activity = 'Y'"
				"    )"
				"    ELSE 0"
				"  END)",
					"is", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_new, curid, activity, prs->VAL_ACCESS));
		balid = sqlite3_last_insert_rowid (db);
		ok = ok && (balid >= 0);
		balxs = ACCESS_CONFIG;
		/* Continues into OPCODE_ADD: */
	case OPCODE_ADD:
		xs = xs && ((!ok) || maydo (balxs, ACCESS_CONFIG));
		ok = ok && xs;
		ok = have_prepsql (&ps_add, db, "INSERT INTO usrmap (balid,domid,username) VALUES (?,?,?)", "iis", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_add, balid, domid, prs->VAL_NEWUSER));
		break;
	case OPCODE_GET:
		//TODO// We may add SQLite function maydo() to demand %R per record
		ok = have_prepsql (&ps_get, db,
				"SELECT"
				"  balid,"
				"  '+lets+'||?2||'+'||CAST(?3 AS VARCHAR(64))||'@'||?1 AS url,"
				"  balance,"
				"  CASE activity"
				"    WHEN 'Y' THEN 'Yes'"
				"    WHEN 'N' THEN 'No'"
				"    WHEN 'B' THEN 'Blocked'"
				"    ELSE          'Unknown'"
				"  END AS activity\n"
				"FROM"
				"  balance\n"
				/* Prepare for admin operation without ownid */
				"WHERE balid=?3 OR ?3=-1",
					"ssi", "isis") && ok;
		int dbstate = ok ? bindstep_prepsql (&ps_get, prs->VAL_DOMAIN, prs->VAL_CURRENCY, balid) : SQLITE_ERROR;
		table_prepsql (&ps_get, dbstate, "[,,,");
		break;
	case OPCODE_SET:
		/* Resume or suspend activity, add or subtract average on change */
		xs = xs && ((!ok) || (need_admin ? maydo (curxs, ACCESS_ADMIN) : maydo (balxs, ACCESS_CONFIG)));
		ok = ok && xs;
		ok = have_prepsql (&ps_set, db,
				"UPDATE"
				"  balance AS b\n"
				"SET"
				"  activity=?1,"
				"  access=IFNULL(?5,b.access),"
				"  balance=b.balance + CASE"
				"    WHEN ?1= 'Y' AND b.activity <> 'Y'"
				"    THEN IFNULL (+AVG (others.balance), 0)"
				"    WHEN ?1<>'Y' AND b.activity =  'Y'"
				"    THEN IFNULL (-AVG (others.balance), 0)"
				"    ELSE 0"
				"  END\n"
				"FROM"
				"  balance others\n"
				"WHERE b.curid=?2"
				"  AND b.balid=?3"
				"  AND others.curid=?2"
				"  AND others.activity = 'Y'"
				"  AND others.balid <> b.balid"
				"  AND INSTR (?4,b.activity) > 0",
					"siiss", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (&ps_set, activity, curid, balid, oldactvy, prs->VAL_ACCESS));
		ok = ok && (sqlite3_changes (db) > 0);
		break;
	case OPCODE_DEL:
		/* Complex conditions for deletion to avoid reboot on low balance.
		 * Admins are assumed mindful; balance del is implicitly accepted.
		 * Active balances >= average of all [other] active balances.
		 * Inactive balances >= 0, the resting form of at-least-average.
		 * Note: May be relaxed: Remove aliases but keep one on low balances
		 */
		/* This deletion facility requires AUTOINCREMENT */
		xs = xs && ((!ok) || maydo (curxs, ACCESS_ADMIN));
		bool xs_admin = xs;
		xs = xs_admin || maydo (balxs, ACCESS_DELETE);
		ok = ok && xs;
		ok = have_prepsql (&ps_del_admin, db,
			"DELETE FROM usrmap"
			"  WHERE usrid=?",
					"i", "") && ok;
		ok = have_prepsql (&ps_del, db,
			"DELETE FROM usrmap"
			"  WHERE usrid=?"
			"  AND ("
			"    SELECT moi.balance >= IIF ("
			"        moi.activity = 'Y',"
			"        AVG (actives.balance),"
			"        0)"
			"    FROM balance moi, balance actives"
			"    WHERE moi.balid=usrmap.balid"
			"      AND actives.curid=moi.curid"
			"      AND actives.activity = 'Y'"
			"    GROUP BY moi.balid)",
					"i", "") && ok;
		ok = have_prepsql (&ps_del_gc, db,
			"DELETE FROM balance"
			"  WHERE 0 = ("
			"    SELECT COUNT (*)"
			"    FROM usrmap"
			"    WHERE usrmap.balid = balance.balid)",
					"", "") && ok;
		ok = ok && (SQLITE_DONE == bindstep_prepsql (
				xs_admin ? &ps_del_admin : &ps_del,
				ownid));
		ok = ok && (sqlite3_total_changes (db) > 0);
		if (ok) {
			bindstep_prepsql (&ps_del_gc);
		}
		break;
	default:
		ok = false;
	}
	//
	// Return the result
	if (!xs) {
		wrap_status (403, "ARPA2 Access Denied");
	}
	return ok;
}


