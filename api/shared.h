/* shared.h -- Includes for shared code for the API to ARPA2 LETS.
 *
 * Routines for repeating SQLite3 queries and Access Control.
 *
 * Note the need to treat the domain separately for the currency
 * and the user; although mostly the same, the domains may differ
 * due to authentication with Realm Crossover, but the database
 * only deals with them after mapping to an Actor Identity, which
 * must share the currency's domain.
 *
 * Prepared objects are stored internally, and cleaned up on_exit().
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 * SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
 */


#include <stdint.h>
#include <stdbool.h>

#include <arpa2/rules.h>
#include <arpa2/rules_db.h>



/* The structure that holds a prepared statement.
 *  - isis is a string of arguments, 'i' for int64 and 's' for string;
 *  - osos is a string of results,   'i' for int64 and 's' for string;
 *  - db   is the database pointer;
 *  - stmt is the SQLite3 prepared statement.
 */
struct prepsql {
	const char *isis;
	const char *osos;
	sqlite3 *db;
	sqlite3_stmt *stmt;
};



/* Parse a hexadecimal character to its value; return -1 if bad.
 */
int hex2value (char inchar);


/* Print directly on stdout.
 */
int stdio_vprintf (const char *fmt, va_list ap);


/* Retrieve a Service Key from an environment variable.
 *  - svckey receives the output value
 *  - envvar is the name of the environment variable
 * Return success.
 */
bool envvar2svckey (rules_dbkey svckey, const char *envvar);


/* Check for whitelisting in Communication Access.  This will attempt
 * to load envvar ARPA2_SERVICEKEY_comm, but fallback to the default
 * if it is not given.  The actor_out may be filled if non-NULL, as
 * defined for access_comm().
 *
 * Return true on success, meaning confirmed whitelist access.
 */
bool access_lets_comm (a2id_t *moi, a2id_t *toi, a2act_t *actor_out);


/* Check for the desired Access Rights in the LETSystem Access Name.
 * Three forms are possible,
 *  - lets@domain
 *  - lets+CUR@domain
 *  - lets+CUR+balid@domain
 * The arguments can cut short the form as desired:
 *  - minimal is minimum Access Rights (may be 0 to just query);
 *  - domain  is the domain scoping the currency;
 *  - CUR     is the short currency name (or NULL to stop here);
 *  - balid   is the balance id (or -1 to skip it).
 * Return depends on the match with rights:
 *  - 0 if the Access Rights do not include the minimal rights;
 *  - the found Access Rights with at least the minimal rights.
 */
access_rights access_lets_xsname (access_rights minimal, const char *domain,
				const char *CUR, int64_t balid);


/* Have a prepared statement.  Only active when psql->stmt is NULL.
 * Registers the prepared statement for cleanup on exit.
 * Return success.
 */
bool have_prepsql (struct prepsql *ps, sqlite3 *db, const char *sql, const char *isis, const char *osos);


/* Bind and step a prepared statement.  Take values from varargs, using the
 * isis format string to determine how many and whether each is "s"tring or
 * "i"nteger.
 *
 * Return the sqlite3_step() output, or an error if anything fails
 */
int bindstep_prepsql (struct prepsql *ps, ...);


/* Fetch an output value from an SQLITE_ROW in the prepared SQL statement.
 * This uses the osos format string.  Then take another step.
 *
 * Return the sqlite3_step() output, or an error if anything fails.
 */
int readstep_prepsql (struct prepsql *ps, ...);


/* Output a data table from a prepared SQLite3 statement.
 * The return code from the SQLite3 statement is passed in.
 *
 * Use output formatting as desired, with support for:
 *  - text/json  with nested arrays and dictionary objects
 *  - text/html  with <table><tr><th>...<td>...</table>
 *  - text/plain with CSV notation
 *
 * Header names will be used where appropriate.  Formatting
 * can indicate desired nesting, which is used where supported.
 * When the return code passed in is an error, no table is
 * produced, but a special form is used where appropriate.
 *
 * The following format is used in the table format string:
 *  - "[" to enter      array output
 *  - "{" to enter dictionary output
 *  - "," to cause      value output
 * Where each may be preceded by the column name if needed.
 */
void table_prepsql (struct prepsql *ps, int lasterr, char *fmt);


/* Begin a transaction.  Return success.
 */
bool txn_begin (sqlite3 *db);


/* Commit a transaction.  Return success.
 */
bool txn_commit (sqlite3 *db);


/* Rollback a transaction.  Return success.
 */
bool txn_rollback (sqlite3 *db);


/* Load the REMOTE_USER environment variable into the authn_user field.
 */
bool authenticate_user (a2sel_t *authn_user);


/* Check if the authenticated user may be a system administrator, following
 * the environment variable ARPA2_LETS_ADMIN_RULE.  This function returns true
 * only if that variable exists, and in that case it sets the access rights,
 * which may or may not imply system administrator rights for the given
 * authenticated user.
 * Syntax error in the rule is reported, returns true and sets ACCESS_VISITOR.
 */
bool envvar_admin_rights (const a2sel_t *authn_user, access_rights *envrights);


/* For a given authn_user, process a rule/setlen to retrieve rights.
 * Return false for processing failure or true for success.
 */
bool lets_rule_process (const a2sel_t *authn_user, const char *rule, int rulesetlen, access_rights *rights);


/* Do we have access to a certain operation?
 *
 * Check that the actual rights contain all of the minimal rights;
 * the actual rights can be determined via opt_rights outputs below;
 * the minimal rights are usually set as contextual information.
 *
 * The return value is a boolean, so usable for logical composition.
 */
static inline bool maydo (access_rights actual, access_rights minimal) {
	return (0 == (minimal & ~actual));
}


/* Lookup the domid for a named domain.
 * Optionally retrieve Access Rights for an authn_user.
 * Return -1 on failure.
 */
int64_t domain2domid (sqlite3 *db, const char *domain, access_rights *opt_rights, const a2sel_t *authn_user);


/* Lookup the usrid for a named domain and user.
 * Return -1 on failure.
 */
int64_t domainuser2usrid (sqlite3 *db, const char *domain, const char *username, int usernamelen);


/* Lookup the usrid for a named a2id.
 * Return -1 on failure.
 */
int64_t a2id2usrid (sqlite3 *db, a2id_t *id);


/* Lookup the curid for a named domain and currency.
 * Optionally retrieve Access Rights for an authn_user.
 * Return -1 on failure.
 */
int64_t domaincurrency2curid (sqlite3 *db, const char *domain, const char *CUR, access_rights *opt_rights, const a2sel_t *authn_user);


/* Lookup the balid for a given usrid and curid.
 * Inactive balances are only returned when the selection flag is false.
 * Optionally retrieve Access Rights for an authn_user.
 * Return -1 on failure.
 */
int64_t usridcurid2balid (sqlite3 *db, int64_t usrid, int64_t curid, bool only_active, access_rights *opt_rights, const a2sel_t *authn_user);


/* Lookup the Access Rights for a balance with the given balid.
 * Return no rights when an error occurs.
 */
access_rights balid2balxs (sqlite3 *db, int64_t balid, const a2sel_t *authn_user);


/* Parse a number to an int64_t identity value.
 * Return -1 on failure.
 */
int64_t string2xxxid (char *str);
