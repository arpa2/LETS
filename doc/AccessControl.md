# Access Control in ARPA2 LETS

> *LETSystems may be very open or they may constrain users to group exchanges.
> This level of variation is determined through Access Control.*

The ARPA2 project defines a
[variety of identities](http://internetwide.org/blog/2015/04/23/id-3-idforms.htm)
and it defines
[API calls](http://common.arpa2.net/md_doc_IDENTITY.html)
to handle users with aliases and pseudonyms, groups with members and even services.

Based on the identity notion, ARPA2 defines Access Control mechanisms through a
[Policy Rule notation](http://common.arpa2.net/md_doc_RULES.html)
that involves rights (such as `%RW` for reading and writing, which usually
maps to a meaning in a particular situation) for identities that match an
[Identity Selector](http://common.arpa2.net/md_doc_IDENTITY.html)
that is annotated after a `~` symbol in a Policy Rule.

Though an ARPA2 Rules DB is the standard form for lookup of Policy Rules, it is
just as well possible to embed them as strings in another context, where they
might popup locally.  Examples of the latter include an LDAP repository and also
the ARPA2 LETS.  The reason for using it here is that it aids users in building
their own currency, with their own rules, and thereby democratically control the
power over the ARPA2 LETSystem for any given domain.


## Delegation of Rights

![Graphical Representation of Access Rights in ARPA2 LETS](AccessControlGraph.svg)

The absolute control over the system is defined by a Policy Rule in the environment
variable `ARPA2_LETS_ADMIN_RULE`, which defines the rights to bring a new domain into
the LETSystem.  Without this first step, no LETS currencies can be added to a domain.
This will generally be a right of the hosting provider.

Note that the identity is assumed to be present in another environment variable,
`REMOTE_USER`, so there is full control from the commandline.  Also note that the
underlying SQLite3 database, found in `ARPA2_LETS_DB`, can also be controlled
directly to take control.  He who has commandline access controls the system.
The normal access method is through the web, of course, using commands of a
similar structure to the commandline.  The ACL is however enforced in both the
usage methods, with the same environment variables.  This is used in the testing
scripts, among others.

Domains can be setup with an `access` string that holds the Policy String dictating
who may create currencies (`%C` or `%A` in the domain policy or `%A` in the
`ARPA2_LETS_ADMIN_RULE` will do for that), who may `domain get` or `domain set`
this `access` rule and ultimately who may `domain del` to remove the domain.
The default Policy Rule for a domain is `%ODCF ~admin@[DOMAIN] %C ~@[DOMAIN]`
where `[DOMAIN]` should be read as the domain name at hand.  This means that the
user or group named `admin@[DOMAIN]` can change the policy, create a new currency
and ultimately delete the domain and anything underneath.
Independently of this, domain users/groups/... can create currencies (the democratic
default setting for the ARPA2 LETSystem). while others are mere visitors who only
enjoy good-for-nothing `%V` visitor rights.

Those who may create a currency can in turn set an `access` string with a Policy Rule
for that level of control.  Anyone may `get` the currency, those with `%F` or `%A`
rigths may `set` a new `access` string, those with `%D` or `%A` rights on the
currency may delete a currency and anything underneath it.  Those with `%C` rights
may create a balance.  When no `access` is provided, it defaults to
`%ODCXAF ~admin@[DOMAIN] %C ~@.`
so again special rights for the `admin` user or group, including a future option of
`%X` for executing currency misuse analysis.  Most importantly, anyone may create a
balance, including authenticated identities from another domain (through
[Realm Crossover](http://www.internetwide.org/blog/2019/11/26/all-about-protocols.html),
another ARPA2 Identity innovation).

Balances are currently considered public, though we might consider `%R` rights in
future versions of the system, so it is probably good setting up new policies with
that in mind.  The `get` and `set` commands apply to balance information but not the
actual value, because that is revealed with `disbalance get` which shows the difference
between the balance shown and that of the requesting identity.  Using `add`, it is
possible to add an alias.  Finally `consume new` and `produce new` create consumptions
and productions.

Consumption rights are well-designed.  The consumer creates the record and may at their
initiative increase the value, while the producer may choose to lower the value.  The
consumption may optionally reference a `produce` record which is more loosely defined
as a pattern, and for which Access Control is currently not yet established.


## Enabling the LETSystem under an ARPA2 Domain

  * Running a LETSystem under any given domain opens it for trading, even
    if just in a local currency, but that should be an explicit choice of
    any domain owner.  It is therefore an explicit action to allow the
    operation of the LETSystem under any domain, even when the software
    is made accessible under its web path.

  * The choice to support the LETSystem under domain `example.com` is made
    with the command `a2lets domain new domain example.com` and revoked with
    `a2lets domain del domain example.com`.  The latter destroys all
    currencies and all history!

  * The subcommand `a2lets domain get` lists domains, which is available
    to anyone setup in `ARPA2_LETS_ADMIN_RULE` with `%A` or `%R` privileges.
    When a domain is specified, flags other than `%A` may also come from the
    domain's `access` settings.

  * The subcommand `a2lets domain set` updates domain settings, which is
    available to anyone setup in `ARPA2_LETS_ADMIN_RULE` with `%F` or `%A`
    privileges.
    When a domain is specified, flags other than `%A` may also come from the
    domain's `access` settings.

  * The subcommands `a2lets domain new|del domain example.com`
    are available to administrators setup in `ARPA2_LETS_ADMIN_RULE`
    where they must have `%A` or, as alternative option for `del`,
    the `%D` privilege.
    When a domain is specified, flags other than `%A` may also come from the
    domain's `access` settings.

  * **The following is under evaluation, for now stick to the envvar:**
    The subcommands `a2lets domain new|del domain example.com`
    are available to the members of the
    [ARPA2 group](http://common.arpa2.net/md_doc_GROUP.html)
    named `admin@example.com` (and similar for other domain names),
    where they must have `%W` or `%F` or `%A` privileges.
    Within the InternetWide Architecture, this group normally holds the
    people overviewing and managing the domain name.  When the `admin@` group
    does not exist, the `a2lets domain` subcommands are not available.  To manage
    the group, use the `a2rule group` subcommand of `a2rule(1)`.

  * By default, any domain member can create new currencies under a domain;
    choose an explicit strategy this is not desired, using the `access` parameter
    to set domain access policy as described below.

  * Hosting providers may choose to initially setup their identity as member
    of `admin@example.com`, setup features such as the LETSystem in response
    to tickbox selections during domain setup, and only then delegate control
    by adding the real admins and removing their own identity.


## Examples

We tested these rights extensively.  The general plan is in the
[ACL Test Plan](../test/acl-testplan.md)
and this is worked out in code in
[CTest scripting](../test/CMakeLists.txt).
These provide the commands that may be fired at an `a2lets` instance,
either on the commandline or via the web, as well as the proper setup
for the `ARPA2_LETS_DB`, `ARPA2_LETS_ADMIN_RULE` and `REMOTE_USER`
environment variables.



<!--



```
   ----- 8< -------- 8< -------- 8< -------- 8< -------- 8< -----
```



## So far, so good

> *The thigs below here are not currently certain, as is the original idea
> of implementing Access Control via the ARPA2 Rules DB.  Please do not
> build around the things below, unless you need it and are willing to
> provide code and documentation, after discussing your needs.*

## Rights for a Domain in the LETSystem

  * Domains should enable the LETSystem with `a2lets domain new` and may
    subsequently be reconfigured with `a2lets domain set`.  Each
    domain has its own
    [Policy Rules](http://common.arpa2.net/md_doc_RULES.html)
    that detail who may do what to the domain currency, but it may not
    assign `%A` rights for the domain level.  Admin rights are taken
    from the `ARPA2_LETS_ADMIN_RULE` environment variable and what this
    sets is expanded with (all but `%A`) from a possible rule in a
    specified domain.

  * Policy rules for a domain can be set with the `access` parameter to
    `a2lets domain new` or `a2lets domain set`.  When absent, they are
    set to a default rule.

  * TODO: The following is not currently implemented:
    `%O` or `ACCESS_OWNER` marks who took ownership of the LETSystem for
    this domain.  This party takes the responsibility, as far as defined
    in policy, and must always be set.  It is initially set to the identity
    running `a2lets domain new` and will be replaced during
    `a2lets domain set` to the identity commanding that.

  * `%D` or `ACCESS_DELETE` is the right to run `a2lets domain del` on
    this domain.  Note that both creation and deletion are automatically
    granted to members of the domain's admin group.

  * `%C` or `ACCESS_CREATE` is the right to run `a2lets currency new|del`
    under this domain.  Note that normal currency deletion works through
    the acces setup for the currency itself.

  * `%F` or `ACCESS_CONFIGURE` is the right to change Policy Rules of
    this domain and its underlying currencies.  TODO:FUTURE: When this right is
    exercised, the `%O` value will also be used.

  * `%A` or `ACCESS_ADMIN` is the right to add new domains, to administer
    the domain as with `%ODCF` and to process data from a currency under
    this domain.
    Administrators may be assigned responsibilities under domain policy,
    that they must first accept.

  * Default policy rules setup (TODO: the domain creator, FORNOW:
    the userid `admin` under the domain) with `%ODCF` rights
    and users of the same domain with `%C` rights.

  * Domain owners may want to initially setup a currency `XES` for newly
    created domains, to give them an easy default start.  This is not
    done automatically, because it is often a good idea, but perhaps not
    always.


## Rights for a Domain Currency in the LETSystem

  * Currencies should be created with `a2lets currency new` and may
    subsequently be reconfigured with `a2lets currency set`.  Each
    currency has its own
    [Policy Rules](http://common.arpa2.net/md_doc_RULES.html)
    that detail who may do what to the domain currency.

  * Policy rules can be set with the `access` parameter to
    `a2lets currency new` or `a2lets currency set`.  The right to
    create a currency is set one level up as `%C` in either the
    domain access rule or the `ARPA2_LETS_ADMIN_RULE` environment
    variable, or as `%A` in the latter.

  * TODO: NOT IMPLEMENTED YET:
    `%O` or `ACCESS_OWNER` marks who took ownership of the LETSystem for
    this domain currency.  This party takes the responsibility, as far as defined
    in policy, and must always be set.  It is initially set to the identity
    running `a2lets currency new` and will be replaced during
    `a2lets currency set` to the identity commanding that.

  * `%D` or `ACCESS_DELETE` is the right to run `a2lets currency del`
    on this domain currency.  Note that creation relies on rights at
    the domain level.

  * `%C` or `ACCESS_CREATE` is the right to run `a2lets balance new`
    under this domain currency.  Note that balance deletion relies
    on the setup of the currency itself.

  * `%A` or `ACCESS_ADMIN` is the right to block and unblock a balance
    under this domain currency.  Administrators may be assigned
    this responsibilities under domain policy (TODO:, that they must first accept).
    Note that currency-level administration rights do not overlap with the
    domain-level administration rights as setup in the ARPA2_LETS_ADMIN_RULE.
    TODO: ALSO MOST OTHER RIGHTS?

  * `%F` or `ACCESS_CONFIGURE` is the right to change Policy Rules
    of this domain currency and its underlying balances.  With this
    right, the currency's current Policy Rule will also be shown;
    without this right, it is concealed.

  * Default policy rules setup the domain (TODO: currency creator FORNOW: admin user) with
    `%ODCXAF` rights and other domain users with `%C` rights.

Not currently implemented, but an option for the future:

  * `%X` or `ACCESS_EXECUTE` is the right to analyse Hot Money on
    the balances under this domain currency.

  * `%A` extension option: It also enables arbiter
    action, that is to change the value of a consumption record,
    presumably after negotiating between consumer and producer.

## Rights for a Domain Currency Balance in the LETSystem

  * Balances should be created with `a2lets balance new` while
    setting up the initial user identity.  They may be subsequently
    reconfigured with `a2lets balance set`.  Each currency has its own
    [Policy Rules](http://common.arpa2.net/md_doc_RULES.html)
    that detail who may do what to the domain currency balance.

  * Policy rules can be set with the `access` parameter to
    `a2lets balance new` or `a2lets balance set`.

  * `%O` or `ACCESS_OWNER` marks who took ownership of this balance.
    This party takes the responsibility, as far as defined
    in policy, and must always be set.  It is initially set to the identity
    running `a2lets balance new` and will be replaced during
    `a2lets balance set` to the identity commanding that.

  * `%D` or `ACCESS_DELETE` is the right to run `a2lets balance del`
    on this balance, after meeting the requirement that the balance
    is neutral.  Note that creation relies on rights at the domain
    currency level.

  * `%F` or `ACCESS_CONFIGURE` is the right to add user names by
    running `a2lets balance add` on this balance.  It also enables
    the right to alter the policy rules for this balance.  Finally,
    it enables toggling activation between `Y` and `N` but not to
    remove the `B` administrative block.

  * `%R` or `ACCESS_READ` is the right to query this balance as a
    disbalance relative to their own, as well as production records
    that hold offers, payment requests and so on.  This may be used
    to constrain the visibility of a balance, perhaps to a group.

  * `%W` or `ACCESS_WRITE` is the right to create consumption records
    with this balance in consumer role, or to create production
    records with this balance in producer role.  This may be used to
    grant others than the balance users to act for it, such as when
    they are members of a group.

  * Default policy rules setup `admin@example.com` and the balance creator
    with `%ODFRW` rights and other domain users with `%R` rights.



TODO:FROMHERE:EDIT



## Controlling which ARPA2 Domains offer the LETSystem

TODO: BOOTSTRAPPING.

Domains need to be setup in the ARPA2 LETSystem before they can
be used.  This involves the introduction of the domain with
`a2lets domain new` and, possibly at some later point, the
destruction of all data involved with all currencies with
`a2lets domain del`.

  * `%A` or `ACCESS_ADMIN` on the Access Name `+lets@example.com`
    is for generic administration; this involves the right to change
    Policy Rules for any LETSystem part under the domain.

  * When a domain is created, the hosting party should install a
    policy rule to permit domain adminstrators to use this right.  At
    their discretion, the LETSystem may be enabled under conditions
    such as a checkbox or a service level.

  * The rights may well be assigned to a Group with a dynamic
    list of members; when a plain user authenticates, they can
    authorise via an Actor Identity setup as a Group Member.
    The group may be a generic domain administrator's group, or
    it could be one dedicated to LETSystem management.


## Controlling the Currencies under an ARPA2 Domain

The most liberal use of a LETSystem allows any domain user to
create a currency at will.  The currency would then also be
accessible to other users.

If so desired, a limitation on permitted users may be imposed
with `a2lets currency new|set` option `access` by setting an
[ARPA2 Selector](http://common.arpa2.net/md_doc_IDENTITY.html)
that specifies which users are welcomed, defaulting to the
form `@example.com` to welcome all domain users.  Other forms
would be `cooks@example.com` to welcome Group Members such as
`cooks+john@example.com`.

It is often useful to setup a currency administrator, for such
tasks as changing policy rules and complete removal of the
currency and all administration underneath it.  This can be
done by setting up an ARPA2 Selector in the `admin` setting.
It ends up as the `%A` or `ACCESS_ADMIN` right.

It may also be useful to assign an arbiter to handle policy



## Access Rights to Management of the ARPA2 LETSystem

There are a few things for which Access Rights play a role:

  * Domains that support the ARPA2 LETSystem
  * Currencies available under a domain
  * Balances available for a domain currency
  * Users that share a domain currency balance
  * Production and consummption of value against domain currency

### Access Rights on LETSystem domains

  * `%C` or `ACCESS_CREATE` enables the creation of a domain currency

### Access Rights on LETSystem domain currencies

  * `%D` or `ACCESS_DELETE` enables the deletion of a domain currency
    and any data related to it

### Access Rights on LETSystem domain currency balances


### Domains that support the ARPA2 LETSystem


### Currencies available under a domain

### Balances available for a domain currency

### Users that share a domain currency balance

### Production and consummption of value against domain currency



> *Considering that we use the concept of Hot Money to allow privacy
> of currency accounts, we can employ constraints on access control.*

An early realisation in the design of ARPA2 LETS was that money being
communication (between independent supply and demand), it is sensible
to use the already-defined infrastructure for Communication Access,
by which users can indicate who may communicate with them.


## ARPA2 Communication Access

The essential form that ARPA2 Communication Access takes is one where
simple patterns can be used to match remote users and assign them to
the recipient's whitelist, blacklist or greylist.  There is another
option for honeypot behaviour, but protocols for which this makes no
sense consider it like blacklisting.

The same definitions for Communication Access apply to all
communication protocols, so the same identities can collaborate over
email, chat, telephony and also in ARPA2 LETS.  This means that a
greylisted entry would be rejected until something was done (possibly
in another protocol) to turn it to a whitelist entry.

Communication Access supports "Realm Crossover", to make it possible
to attach local currency to a foreign identity.  But it is only
possible to use Communication Access with local users, so to be able
to send and receive currency, a foreign identity must have a local
alias, such as a Group Member.  Communication Access can facilitate
that when properly setup for foreign identities.  For groups, such a
translation is standard, even for local identities.

The patterns of a "Remote Selector" include from general to the
(more overriding) concrete patterns:

  * `@.` will match any other party; this includes all domains,
    anywhere.

    Such entries can be used to blacklist or whitelist parties by
    default, since anything more concrete would override it.

  * `@example.com` matches any other party under the `example.com`
    domain.

    Such entries can be used to grant or revoke access to those
    domains by either whitelisting or blacklisting them.

  * `user@example.com` matches a specific other party.

    Such entries can be used to accept or reject specific users
    by either whitelisting or blacklisting them.

The tool to make Access Control changes is `a2rule(1)` for the `comm`
service; you can `add`, `set`, `get` or `del` entries in the ACL.
Since Communication Access is defined in the
[ARPA2Common project](https://gitlab.com/arpa2/arpa2common)
and
[documentation](http://common.arpa2.net/),
this is not further worked out here.


## ARPA2 Group Access

One useful pattern for ARPA2 Identity is the structure of Groups.
You can be a Group Member, and would end up with an alias that is
local to the group.  You can use this in Access Control settings,
and there is a pattern to select group members.

  * Your normal identity could be `johann@example.com`.

  * You might be in a group `cooks@example.com`

  * Your membership alias might be `cooks+john@example.com` and
    this will be used whenever you do things within the group.

  * Others might filter you with your membership alias specifically,
    but they might also have a (permissive) filter on
    `cooks+@example.com` to match all group members in one go.

This helps to integrate currencies into a group, although each
group member would normally make an individual choice to join.


## ARPA2 LETS Users

Users would do the following to enter an ARPA2 LETS currency:

 1. Create a balance for a currency under a username or alias
 2. Optionally add more (group membership) aliases to that balance
 3. Permit sufficient Communication Access for intended trading

After this, they can use the system, which is customarily bound
to `/lets/` under a domain (but may be locally different).


### Creating a new Balance

If you want to start using a domain currency under ARPA2 LETS,
you can use a web interaction to signup.  You need to
authenticate as a domain user, usually via HTTP-SASL.

In the process, you can indicate what alias you want to
introduce for a newly created balance.  Depending on your
taste, you may choose to use an alias over which you
commonlny communicate, or to have a separate one to
reduce the visibility of your balances.

There is no use for Access Control in this process, but
you need to have an ARPA2 Identity that controls the
targeted ARPA2 Identity from your login.

You would end up with completely distinct balances if you
did this more than once.  That is certainly possible, but
the next subsection explains how you can share the balance
across aliases.

You cannot detach a balance from the last alias until it is
zero.  This can be achieved by booking the balance to some
other account, for instance another of your own.


### Sharing an existing Balance

The action for sharing a balance is similar to creating a
new one, except that you should indicate that what you want
to share.  You authenticate as a domain user as before.

There is no use for Access Control in this process, but
you need to have an ARPA2 Identity that controls both the
new and old ARPA2 Identity from your login.

You would end up with the same balance that shows up under
the two aliases.  You can repeat this by sharing with
either to include a third alias, and so on.  They all
show the same balance, and are impacted by the same
changes made with respect to either of your aliases.

When you transfer currencies, your aliases will be kept
in the history.  This means that any privacy through
your use of aliases is not sacrificed in the history.


### User changes to Access Control

This is achieved with the tools for Communciation Access.
Anyone who can send you email is also permitted to spend
to you in the currencies that you adopted.  Nobody is
permitted to extract currency from you, of course.  They
also cannot know what currencies you have setup for yourself.

Take note that balances are public.  This is an intrinsic
part of LETSystems, and it is why you need to sign up
explicitly.  It is also why we allow you to use (different)
aliases and group memberships to scope your use of the
ARPA2 LETSystem.


### Protections against Abuse

Nobody can take funds from you, of course.  And you can
only book funds after login to the ARPA2 LETSystem.

But there is a more subtle problem.  Users might try to
play "central bank" by issuing large amounts of currency,
passing it on to another (more public) identity and use
it to buy anything they desire.  This leads to inflation
of the currency (and imposing labour on people or else
face the risk of debasing their currency may be considered
forced labour).  In classical LETSystems, this is resolved
by publishing all balances to all users.

The ARPA2 LETS offers more privacy by showing how "hot"
the money in one's balance is.  When you accept such
hot money in your balance, your balance will warm up a
little, but when you do it regularly it will heat up
considerably.

You should probably take care not to accept hot money in
return for favours.  The heat shown is relative to your
own balance, so it informs you that someone else is a
less suitable trading party for you.  But it may also be
someone who has difficulty getting on with the system.
Use the heat of money as an indication, but continue
using common sense.  Talk to the people if you are not
sure, and perhaps check what others' experiences are.

The heat in your balance will gradually go away if you
trade with other, cooler balances.  Lukewarm balances
are normal, and not a direct warning sign.  In general
however, heat is caused by the balance at the time that
new currency is created from it.  Hot money indicates a
consistent pattern of creating money while balances
drop.

Your new account will also appear somewhat hot in the
beginning.  That is a mild prejudice about new accounts.
The heat will wear off quickly if you do some trading
and if your initially put effort into the community and
by getting awarded for it.

These measures exist to make it unattractive to abuse
the system in "central bank" mode.  Anyone who uses the
system for a longer time should find no problem in a
balance that toggles between negative and positive.  It
is also completely fine if a balance is oscillating a
little below zero.  It is just the uncontroleld creation
of currency that needs to be avoided.


## ARPA2 LETS Administrators

The currencies of ARPA2 LETS do not *require* active administration,
though you can use Access Control to put in constraints that make
you do more.

By default, anyone may create a currency, because exchanges should be
democratic and open.  This means that no administrator would enforce
things that users do not want.

However, you can take control over things with a global environment
variable `ARPA2_LETS_ADMIN_RULE` that defines initial administrative
access.  To allow a user `john@example.com` as administrator, reader
and writer, you would for example set

```
ARPA2_LETS_ADMIN_RULE='%RWA ~john@example.com'
```


### Creating a new currency


Creation of a new currency comes down to creating it under the
ARPA2 LETSystem.  This requires two things:

  * The currency name must not yet exist under the targeted domain;
  * The person asking to do this must have creational access to
    the currency identity under Access Control.  **TODO:**RULES?

The identity of a currency is something like `+lets+XES@example.com`,
in which

  * `+lets` is the service name for ARPA2 LETS; it may be different,
    in some places, but this the normal name;
  * `XES` is the case-insensitive currency being created; this
    particular name is often created by default so you may have to
    choose another;
  * `example.com` is the domain that manages and scopes the created 
    currency.

Note that it makes no sense to speak of `XES` as a currency of its
own, as we commonly do with `JPY` or `XAU` (Japanese Yen and Gold
Ounces, respectively).  The currency is not global, but scoped by
a domain name, so make sure that the domain is known.  You might
use the currency identity `+lets+XES@example.com` as a form to do
just that.

The creator of the currency becomes the initial administrator of the
currency.  This places them in a position to alter Access Control for
the currency.  Useful standard profiles can be requested during the
creation of the currency.

The customary setup for `XES` is to make the domain administator
the initial administrator.

Note that ARPA2 LETS includes support for virtual hosting.  This
means that any domain can have any currency, and they would all be
different.  They also have separate administration and Access
Rights.


### Permissions for Users

Any ARPA2 Identity could setup an account with the given currency,
but this can be constrained under Access Control:  **TODO:**RULES?

  * To be able to destroy a currency, a user needs `D`elete access;
  * To be able to enter the currency, a user needs `W`rite access;
  * To be able to see the users of a currency, a user needs `R`ead access.

All these are set on the currency identity, so `+lets+XES@example.com`.
The Rules DB is used with the Access Type set to the `lets.uuid.arpa2.org`
UUID, `72594b3d-e29c-3f2a-81c9-42956ceb1347`.

**TODO:** Might it invite attacks if all remote users are initially welcome?
**TODO:** Is XES particularly vulnerable if it is world-accessible by default?


### Permissions for Heat (Flow) Mapping

An administrative control is required for making Heat (Flow) Maps of
a currency.  This helps to track where the currency is being abused,
and what balances might be blocked because they are structural abusers
or, more interestingly, part of a white-washing trail.


## ARPA2 LETS and its API

Look into the API documentation for details about the Access Rights
needed for particular operations.

-->
