# Design Document for ARPA2 LETS

> *Local Exchange Trading Systems rotate abundant money in a local
> environment.  Note that "local" does not have to be geographic
> in nature, it could just as well be a community.  Interestingly,
> comparisons are not absolute measurements of value, but relative.*

One might call this **IETS**, short for **InternetWide Exchange Trading System**.
Now, that's something to go for!

The "mkroot" container is named `internetwide/exchange`.  It runs
a database and a few FastCGI components operating on it.  And it
responds to the addition and removal of domains and users.  It also
taps into the ARPA2 Access Control system.


## Value and Tokens

Domain users would share a domain-specific currency.  The value of
one token in this currency could be defined, or kept informal.
Users book tokens back and forth to express their appreciation of
work done.

Interestingly, users can have a positive or negative number of
tokens.  This means they can spend freely, effectively creating
tokens as the need arises.  Their balance will be public, and
as a result it will be clear when people are abusive; this calls
for self-regulation.

According to
[The LETSystem Design Manual](https://manual.letsa.net/)
it is not useful to impose controls on the system, such as
minimal or maximal token counts, because that will reduce
the call on self-regulation — and on relativation of the
importance of such actions.  It is also considered harmful
to connect LETSystems, because it causes value to escape a
local system.  Instead, users could be active in many
domains, and have separate token balances in each.


## Giving as a Kind System

When values can go negative, it is easy to give; tokens are not
scarce.  This means that there is room for kindness.

Such a system may be used for feedback between people.  They
might give a "thumbs up" to what they consider the most valuable
benefitors.  The value is returning anyway, when giving is nicely
balanced.

Interestingly, there is no tracking of those who gave their
"thumbs up" if they do not leave comments.  All that remains is
the influence on balance of shifting tokens.  The parties who
are involved in the shift may well see their peer.


## Accounts and Identity

The ARPA2 system allows for aliases (visibly linked to a user
identity) and pseudonyms (invisibly linked to a user identity).
Since these offer multiple identities, these might also allow
for concealed token balances, causing less self-regulation by
other domain users who cannot see this scaling factor.  For
that reason, it is a good idea to tie the token balance to the
principal user.

Groups may have a balance of themselves, to be distributed
among its members or perhaps others.  Likewise, a service may
have an automated mechanism for measuring their use and perhaps
also awarding effort put in by others.  Again, these are the
principal identities, even when they represent groups or
services instead of users.

**TODO:** Map group members to the principal user.


## Absolution is Relative

New users start at zero.  When they leave the system, their
balance may be non-zero, but that hardly matters.  What does
matter is that payments are relative; they add the tokens to
one party and extract the same number of tokens from elsewhere.
(This is assuming that there is no accounting for the greater
value added by a service than put in by the executor, which is
actually how economies prosper.)

This zero-sum game, as it is usually called, is easy to explain
and represents a flow of money.  It is not so important where
the zero level is; *what matters is how token levels between
participants differ*.  Others may have worked harder, or less,
than the user looking at them.  This is what is useful to know,
because it indicates where to ask for service, and where to
offer it.  Now it no longer matters where the zero point is,
and so it does not matter whether users depart with a non-zero
amount.

It is not uncommon for new users to be started above zero,
but that again is somewhat arbitrary; it is a choice.  It may
be helpful to level off all domain users by taking off the
rounded average when a user leaves, so as to make zero starts
more neutral.  For the users themselves it does not matter.

The most honest system for new users may in fact be to start
them off at the average token balance, so they appear to be
somewhere in the middle.  This is the actual intention of new
users starting at a zero balance.  If this is the approach
taken, then it might make sense to rid the system of users
with extremely low token value and/or long-term inactivity,
either by ditching their accounts or not incorporating them
in the average, as that ought to apply to the active users
they might engage with.

Activity can be measured more subtly, by incorporating the
timestamp of the last payment made or received.  There may
be an exponential fallback, for instance.


## Naming a Currency

Currencies in an original LETSystem would be linked to a
local currency, but that is not meaningful in situations
with a global community.  It is up to the domain to choose
what currency to use, and whether it is local or not.

The identities of a token currency will be a service name,
such as `+token+xyz@domain.nep` where the token name would be
`XYZ` in this case.  A format string is defined elsewhere,
and has prefix and/or postfix elements.  If this is absent,
then `"XYZ %d"` is used as the format string.

There can be multiple currencies.  In fact, the `+token`
prefix could theoretically be set differently as well,
but "mkroot" will default to this particular choice.
Since there is no otion of exchange between currencies,
the result of multiple currencies may be confusing; but the
effective partitioning can be helpful in some situations.


## Database Structure

The database has a SQL structure, so as to support set
queries.  The tables are:

  * `idmap` to map pseudonyms to login users; the stripped
    user identities are used, without alias, member name
    or service arguments; without signature; but with the
    domain name.

  * `balance` maps login users and a currency name (just the
    simple string like `XYZ` above) to a number of tokens.
    It is also possible to iterate over currency names, and
    thereby retrieve a personal balance.

  * `payment` identifies transaction numbers with a domain
    name to a timestamp, payer and payee principal users, a
    currency name (`XYZ`) and number of tokens.  These are
    historic payments and are included in the `balance`.
    As a result, ancient transactions may be removed at any
    time.

**TODO:** Mapping group members to their principal user.


## Token Balance Interface

Queries for a token balance generally deliver a list of
entries.  Balances returned are relative to the authenticated
user asking for it, after reducing user names to the proper
principal user name form (and keeping the result internal).
Access control is applied to each query before the reduction.

There is an option for iterating group members.  This is only
permitted to those users with the right to see lists of group
member identities.  Others are confined to inquiries about a
concrete member identity.  Lists of member identities such as
`cooks+john+mary@domain.nep` are also supported, and they do
not count as iterations, but as individual inquiries.  The form
`cooks+-+john@domain.nep` iterates over the list and skips
`cooks+john`, so it does require group iteration rights.

The output is a JSON dictionary with currency names as keys,
and values set to an array with elements that are an array of
the querier's revealed identity, the queried identity and the
relative token balance in that currency:

```
{
  "example.com": {
    "XYZ": [ "XYZ %d",
      [ "cooks+john", "cooks",      +666 ],
      [ "cooks+john", "cooks+mary", +123 ],
      [ "cooks+john", "cooks+mike", -456 ]
    ],
    "ABC": [ "%d ABC",
      [ "cooks+john", "cooks+mary", +404 ],
      [ "johann",     "michael",    +654 ]
    ]
  }
}
```

Alternatively to JSON, a CSV table may be requested.  This may
in future be put to use for backup dumps form the live database:

```
DOMAIN,CURRENCY,FORMAT,USER,OTHER,BALANCE
"example.com","XYZ","XYZ %d","cooks+john","cooks",666
"example.com","XYZ","XYZ %d","cooks+john","cooks+mary",123
"example.com","XYZ","XYZ %d","cooks+john","cooks+mike",-456
"example.com","ABC","%d ABC","cooks+john","cooks+mary",404
"example.com","ABC","%d ABC","johann","michael,654
```

These are, in order of appearance:

 1. A group member seeing the relative group balance `XYZ +666`
 2. A group member seeing another member's relative balance `XYZ +123`
 3. A group member seeing another member's relative balance `XYZ -456`
 4. A group member seeing another member's relative balance `ABC +404`
 6. A principal user seeing another user's relative balance `ABC +654`

It is generally advisable for user interfaces to list the best choices
in terms of currency on top.  This means, the higher token values first
when asking for work to be done, and the lower token values first when
offering work.

A simple FastCGI utility can be asked to select a desired set of
relative balances, and Access Control is used to decide who may see
what balance.

Another FastCGI utitily can be used to book a positive number of tokens
in an existing currency from one user (with write permissions) to
another (whose balance may be read by the paying user, because otherwise
there would be no way to exercise control).

The FastCGI API uses the following environment variables and parameters:

  * `ARPA2_DOMAIN` is the envvar set to the domain name;
  * `REMOTE_USER` is the envvar with an authenticated user identity in
    `user@domain.name` form, maybe as an alias, pseudonym or group member;
  * `ARPA2_LETS_CURRENCY=XYZ` can avoid interpretation of `&cur=XYZ` parameter;
  * `?toi=cooks%2Bmary` is the user identity of the other side; this may be
    any visible identity, including but not limited to a group name
    (possibly attempting to iterate it), a group member or a list pattern;
    It is possible to list this parameter more than once for more output;
  * `&cur=XYZ` is the name of a currency to focus on;
    It is possible to list this parameter more than once for more output;
  * `&amt=123` is the number of tokens in a payment;
  * `&min=-100` and `&max=200` constrain the output to relative balances
    of at least the `min` and/or at most the `max` number of tokens;
  * `&order=desc` or `&order=asc` to order the relative token balances
     as descending or ascending, respectively;
  * `&page=10` and optionally `&skip=2` to return 10 entries and skip 2
     such pages.  The default `&skip=0`.  If no `&page` is mentioned, full
     output is returned.  **TODO:** Return a page count?
  * `&from=YYYY[-MM[-DD]]` and `&to=YYYY[-MM[-DD]]` are restrictions on the
    transaction timestamp.

Services may be listed in the LDAP directory, where they can be easily
searched.  They are not included in the FastCGI interface.  (Will we be
able to get this accepted, or will we need a web-wrapper after all?)


## Connecting LETSystems

**TODO:** Is this useful?  It causes a lot of trouble and may
not add much.


## Creating and Operating Queries in SQL

Create the tables:

```
CREATE TABLE IF NOT EXISTS dommap (
	domid INTEGER NOT NULL,
	domain VARCHAR(255) NOT NULL,
	PRIMARY KEY (domid));

CREATE TABLE IF NOT EXISTS curmap (
	curid INTEGER NOT NULL,
	domid INTEGER NOT NULL,
	currency VARCHAR(32) NOT NULL,
	format VARCHAR(32) NOT NULL,
	PRIMARY KEY (curid));

CREATE TABLE IF NOT EXISTS idmap (
	usrid INTEGER NOT NULL,
	domid  INTEGER NOT NULL,
	princ_id VARCHAR(64) NOT NULL,
	actor_id VARCHAR(64),
	PRIMARY KEY (usrid));

CREATE TABLE IF NOT EXISTS balance (
	curid INTEGER NOT NULL,
	usrid INTEGER NOT NULL,
	balance INTEGER NOT NULL,
	PRIMARY KEY (curid,usrid));

CREATE TABLE IF NOT EXISTS payment (
	txnid INTEGER NOT NULL,
	curid INTEGER NOT NULL,
	payer_usrid INTEGER NOT NULL,
	payee_usrid INTEGER NOT NULL,
	amount INTEGER NOT NULL,
	unixtime INTEGER NOT NULL,
	descr VARCHAR(250) NOT NULL,
	PRIMARY KEY (txnid));

# Testdata
#
INSERT INTO dommap (domain) VALUES ('example.com');
INSERT INTO dommap (domain) VALUES ('example.net');
INSERT INTO curmap (domid,currency) VALUES (1,'COM'),(2,'KOM');
INSERT INTO curmap (domid,currency) VALUES (2,'NET'),(2,'KOM');
INSERT INTO idmap VALUES (1,1, 'johann', 'cook+john');
INSERT INTO idmap VALUES (2,1, 'marietje', 'cook+maria');
INSERT INTO idmap VALUES (3,2, 'johannes', 'john');
INSERT INTO idmap VALUES (4,2, 'johannes', 'johanna');

# List currencies for a domain
#
SELECT d.domain,c.currency
FROM dommap d, curmap c
WHERE d.domid = c.domid;

# Find the principal user for 'cook+john' in domain 'example.com'
#
SELECT i.princ_id
FROM dommap d, idmap i
WHERE d.domid = i.domid
AND d.domain = 'example.com'
AND i.actor_id = 'cook+john';

# Testdata
#
INSERT INTO payment VALUES (1,1,1,2,123,1681943821,'Tikkie vooruit');
INSERT INTO payment VALUES (2,1,2,1,100,1681943827,'Tikkie terug');
INSERT INTO balance VALUES (1,1,-23);
INSERT INTO balance VALUES (1,2,23);

# Final balances for users of 'example.com'
#
SELECT d.domain, i.princ_id, i.actor_id, c.currency, b.balance
FROM dommap d, idmap i, curmap c, balance b
WHERE d.domain = 'example.com'
AND d.domid = c.domid
AND d.domid = i.domid
AND b.curid = c.curid
AND b.usrid = i.usrid;

# Find 'COM' transactions that belong to user 'johann' under 'example.com'
#
SELECT f.actor_id, t.actor_id, c.currency, p.amount, p.descr
FROM idmap f, idmap t, curmap c, payment p
WHERE p.curid = c.curid
AND f.usrid = p.payer_usrid
AND t.usrid = p.payee_usrid
AND c.currency = 'COM'
AND (f.princ_id == 'johann' OR t.princ_id == 'johann');

# Start a balance on a new currency KOM under example.com
# Then re-run the latter query and see them zeroes
#
INSERT INTO balance VALUES (2,1,0);
INSERT INTO balance VALUES (2,2,0);

# Careful :- This will be silently ignored if no rows match
#
UPDATE balance
SET balance=balance+3
WHERE usrid=2
AND curid=77;
```


## ARPA2 Access Control

We shall consider payment as a form of Communication Access.
This means that an Actor Identity may be found relative to the
other party.  Start off with input filtering as in
[AxeSMTP/in.c](https://gitlab.com/arpa2/AxeSMTP/-/blob/master/src/arpa2/in.c)
and continue with group processing as in
[AxeSMTP/group.c](https://gitlab.com/arpa2/AxeSMTP/-/blob/master/src/arpa2/group.c):

 1. Call `access_comm()` with `remote` set to the other party's
    ARPA2 Identity and `local` set to the `REMOTE_USER` as authenticated.
    Reap in `optout_actor` for the optional Actor Identity.

 2. Check with `a2act_isempty()` if an Actor Identity was provided.
    If not, use the `local` value as-is.

 3. Process black/white listing.  White listing allows payments.

 4. Test if the Actor Identity matches both the domain and group name.
    If not, this does not require group processing.  Note that the
    Actor Identity consists of the group name, `+` and the member name.
    Strip the member name and compare anything before that to match
    the `remote` address.  Do this using `a2act_parse()`.

 5. Use `group_hasmember()` to test if `remote` is known to the group.
    This returns `group_marks` which may include `GROUP_SEND` that
    enables sending to the group — we might interpret that as the right
    to iterate the group, maybe???

 6. Use `group_iterate()` to produce a list of group members.
    **TODO:** Only if permitted!
    If not permitted, compute the average balance of group members
    relative to the asking party's balance.  This will give an idea
    of one's own balance with respect to the group *but it does not
    support self-regulation among members of who should do what*.


## Copyright Notice

```
SPDX-License-Identifier: AGPL-3.0-or-later
SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
```
