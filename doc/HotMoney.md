# Hot Money in ARPA2 LETS

> *Hot Money is a concept that allow an ARPA2 LETS to combine two
> seemingly opposite goals, namely to support privacy while having
> the self-regulating effect that avoids abuse in a LETS.*

**This is currently an idea under consideration.**


## Combating Abuse in a LETSystem

The users in a LETSystem see each others' balances, so they can see
if some people drift off to very high or very low balances.  In the
ARPA2 LETS, we made a slight change so users see disbalance, which
does not interfere with this principle.  It may even make it easier
because the focus shifts from one's own balance (relative to zero)
to the balance of others.

The thing that all communities fear is leeching.  Users that benefit
from the community without giving back to it.  These people would
drift off to negative (dis)balances and it is usually a good idea to
help them return to the group, by offering them a task which earns
them a balance increase.

It is vital to this approach that the money stays inside the
community, without extraction to external parties.  When money moves
around in the same circles, it should balance out.  There is no
constant flow from a point of entry into the system to a point of
departure.  Money should circulate without piling up.

Piling up can actually be in a negative sense too, since LETSystems
create money on the fly.  This may have two underlying patterns that
are difficult to distinguish:

  * A person could find it difficult to give back to the community,
    for any reason;

  * A person could be producing free money, and using it to leech on
    the community.

The former should be helped to understand their value to society
(a truly social and humastic approach to debt) while the latter is
a pattern to avoid.  Note that an ability to export money to the
outside of a LETS would seriously motivate towards the latter.


## Privacy in Payments

Ideally, payment information should be private.  This is especially
true for domain-based systems, which play an online game involving
potentially foreign people.

Ideally, there would be no obligation to stick to just one account,
so that balance diversions can be tied to a person.  This may in
fact be impossible in online systems.  And if it is possible, it
involves a lot of tracking and tracing, up to the point that a
proof of human identity is required.  That is silly for a system
that tries to break open payments, allow lots of currenncies in
lots of domains.

It is virtually impossible to avoid registration by one person for
multiple accounts, especially if they have facilities that split
their identity into aliases and group memberships like the
ARPA2 Identity system.

*Oops...* we just killed the most important facility required of
a LETS, and that is to review disbalance between its members.
Administrators can see more, and software might see this too but
would than risk revealing private information about users on the
grounds of mere suspicion.  We see that going wrong everywhere.

*Should we let go of privacy, or just think harder?*


## Detection of Abundance in Money Creation

**TODO:** Redefine; hot money is created zealously; cold money
	is stashed zealously; it should roulate.  Use a transitive
	closure to dermine circular flow and compare to the degree
	that this is missing.
	Hot Money matters while being paid, because it is taken
	from outside the circulation;
	Cold Money matters while paying, because it moves away
	from the intended circulation;
	both are acrued habits that can be reversed with the right
	kind of action.

The thing we are trying to stop is the abundant creation of money.
That is, having one or more accounts that are continually drained
while they are concealed through privacy-supportive mechanisms.

Detecting such things would only be possible by tracing all the
payments made by a person, and seeing how little they do in
return.  But even that may not be true if the person uses the same
account in many places, and earns in one group while spending in
another; privacy may result in undetectable behaviour that is an
overall advantage to the community using a currency.

There is one bit that is necessarily part of creational abundance
that makes it leech off of a community, and that is taking without
giving.  Creation of money is not a problem if it is compensated
by later return of money in the same account.

We can detect this pattern by looking at the (dis)balance of an
account at the time that money was created.  Consistent leeching
causes an ever-widening gap.  This property might be stuck on
the money and taken with it on its ride through the community.

Money created from a low balance might be considered Hot Money,
and its heat would transfer to the places it went, and if this
were a consistent pattern then those places would also heat up.
Most places however, would be a mixture of hot and cold, and
mixed down to a boring luke-warm money.  Transactions could be
said to have some heat, and it would be possible to use simple
statistics to see where the hottest traces are.

*This is a bit like electrons.  They have a given elementary
change that never changes.  But they also have heat.  While
they travel through an electronics circuit their charge does
what it is supposed to do, while a side-effect is that the
electrons carry heat in and out of devices and spread their
heat.*

Note how this works as a bulk property of an account, alongside
the bulk property of the money stored in it.  And it may be
used to select the next party to trade with, avoiding the
handling of Hot Money and thereby tainting one's own reputation.
This is a self-regulating mechanism that can work without seeing
all the transactions, because the heat of a user's account can
summarise about their past.

With this in place, we can allow any form of privacy.  We can
have people create on account, or multiple, even for the same
currency, as they see fit.  They can book money between their
own accounts, they may hide some of those accounts for some of
the community's users, and so on.  There is no longer a penalty
for supporting privacy.

*Why did governments not think of this while legislating against
money laundering at the expense of privacy?*


## Detection of Abundance in Account Creation

To circumvent that zealous creation of money from an account
results in noticeably hot money, it would be possible to open
a large number of accounts, and start each from scratch.  In
short, this should not be beneficial.

New accounts may add heat as a result of their fresh status.
This heat has no influence when taking in currency, but it is
a hindrance to money creation by newcomers.  The heat of fresh
accounts drops when money is added to the account, as well as
when money is created from it.  The addition of money will
generally be in return for services to the community, showing
the new user how to be useful.

Such initial community service does not scale, thus making it
impossible to massively create new accounts for the purpose of
siphoning off money.  Note however that the impairment of new
human users is mild, and the small amount of initial heat wears
off quickly.  The extra heat from new users are likely to be
tolerated by others in the community, especially when they are
communicative, and their injected heat is likely to dissipate
quickly in the community.  In this case the community size is
a factor determining the scalability, both for the number of
newcomers that can arrive at one time and in terms of the
speed at which the heat dissipates.

The sum of expontial decay and rise is a constant, when the
old and new levels are exchanged.  This can be used to set the
parameters for the fresh account penalty on money creation.

This fresh account treatment need only be a worst-case method.
Exemptions may be made in several situations, where the risk
of abundant account creation or the abundant creation of
money from an account can be averted.  The degree to which the
money from a fresh account is considered "too hot to handle"
may also be derived from the number of accounts created over a
period of time.


## Designing the Solution for ARPA2 LETS

*Let's move to practical matters!*

## Definition of Hot Money

> **Hot Money** is the money that was created at a low balance.

This refers to an absolute balance, which we normally try to avoid,
but as long as we are careful about it the reading can translate to
disbalace.  Internally, we do still work with absolute balances.

We need a measure for "how hot" money is, because there may be
hot as well as cold or luke-warm money.  These subjective terms
are best formulated as numbers.

> **Heat of Money** is the sum of balances per created unit of money.
> This heat spreads evenly over the units in an account.

So, the lower the balance sinks, the hotter its money becomes.
It works like the area of a triangle, which rises evermore quickly
when a slice is added on the long side.  The area is this triangle,
spread evenly over the width of its base, ends up being half its
long side.


## Definition of Hot Youngsters

**TODO:** A measure of doing this at a very young age.
Perhaps by dividing money creation by the total amount traded.
This would be added to the heat of creating currency.


### Principle: Depth times Width

Look at Depth times Width; where width is the number of
balances and Depth is how far they have dropped.  This means
that it is equivalent to have N accounts at depth M or 1
account at depth NxM.  What you take out is what shows up as
your heat.

When this measure maps to other levels, consider taking the
width into account.

When cooling down does the opposite, there is no burden over
hot money on reasonable behaviour, while it does pile up in
those who tap from sources that are always and only negative.

Heat can be like an offset to the balance that shows how far
off the total money flow is.

How to measure the width?  Maybe scatter the paying balid
and set a bit in a 64-bit field to see if it overlaps.
The hitrate drops: 64/64, 63/64, 62/64, ... and for each
of these steps there is a number of occurrences (a float),
namely 64/64, 64/63, 64/62, ... to indicate the width of an
increment in the number of bits.  This can be a simple
lookup array.  **Note:** This does not work, it is too slow.

Another idea might be to inspect the history, which is bound
to be a slow process.


### Another Try: Depth times Width

Let the heat collect the tokens obtained, from anywhere.
Let it be the SUM, not the AVG, of the token depth in its
input.  Then, many modest leechers add up to noticeable
heat.  The trick is to look at the step in balances, not
the step in heat, as that is like an integral (another
dimension, in a way).

One giant leecher does have a stronger impact.  This is
due to the growing disparity between the sending and
receiving balance.  When using SUM, it would be gentler
to consider this more modestly.  That makes it kindness
to recognise an older account for milder heat buildup.

There could be a limited number (one?) of "central banks"
that are accepted.

**Note:** It'd work but I don't like it.  Most of this
quickly becomes subjective and non-linear.


## Conclusion: Depth times Width

The above shows that it is rather difficult to do this
accurately, and it also takes a lot of effort to do it
moderately well.

The Width aspect is required, and leads to addition.
The Depth aspect leads to accumulation only when there
is no Width, and getting the Depth into the Width is
notoriously difficult and not likely to succeed in
avoiding abusive patterns.

Why, then, not focus on Width alone?  That is, not
caring where the currency comes from, be it from few
account or many, but just look at the fact that it was
created to fund another account?

This means that Heat is the number of currency tokens
that were pumped into an account through creation,
rather than through earning.  And it differs from the
balance by not contemplating spending but only looking
at incoming money.

Heat then asks *how much money landed in this account
through creation?* and when this is equal to the
account balance it indicates a just-created spending
account; when it continuously creates money to spend
elsewhere it accumulates and shows up as a leecher.

Money that lands in an account in return for services
rendered may lower the Heat, so that the balance and
Heat follow the same line under normal behaviour.
This can help to prominently set leeches aside, by a
Heat that differs violently from their balance.  This
needs a little more work though.

**Conclusion:** Drop the Depth aspect, and consider
only how many currency tokens were created for the
use in the account.


## Definition of Hot Trasactions

> **Hot Transactions** transfer hot money and leave traces of their
> heat both where they arrive and as a temperature passing by.

Again, this is subjective; there can be cold or luke-warm transactions
as well, and we should best assign numbers to this idea.

> **Heat of a Transaction** is the heat of the units of money involved
> in a transaction, multiplied by the number of units of money.

The heat-per-unit does not change on the paying side.  The recipient
however, would mix the money into their account, and its heat may
spread evenly over the total number of units of money.

A cold payer would spread a low temperature with their transaction,
and that could lower the overall temperature of the receiving account
after the transaction.  When trading long enough in a small community,
the end result would be an even spread of the temperature throughout
the community.  Those who do not participate but who merely leech
should be easily detectable.

**TODO:Concern:**
Could a leecher conceal his actions by taking payments from a very
cool account?  Is this a problem?  It is a form of adding to the
community, albeit that the coolness could motivate an easy way out.

**TODO:Concern:**
Could a leecher spread his heat by paying to a large number of
others?  No, as that would only lower their balance.  This would
be disadvantageous for their own future money creation, because
their account would be lower than possible for their intentions.


## Drawing up Heat Maps

Heat is a momentary measurement, and may not give all the information
that guides the best decisions.  It may be useful to draw up diagrams
with heat in colours, to indicate where things are going wrong.

Heat Maps could follow a time line and show per period what the heat
of transactions was; perhaps the extremes or the average could help
to understand what was going on.  Especially useful about suc diagrams
would be the visibility of a trend towards heat or coolness.

**TODO:Choice:**
It probably makes no sense to include information about cold values,
even if it probably makes sense to compute them.


## Drawing up Heat Flow Maps

Given the heat attached to transactions, it would be possible to
trace money and show where the heat came from.  It seems useful to
keep the heat records in the history, but allow various modes of
analysis on top of that.  The most extensive ones might service
someone to analyse any problems experienced with a currency.

The most useful analysis is perhaps a Heat Flow Map.  This would
group historic movements by their from and to accounts, and show
the average heat, weighed by the amount transferred.  The user
identities are less informative in a heat flow map, as that could
be easily evaded by any user.  The balances however, collect the
heat that describes the origin of their money, and they would be
more difficult to hide.

The interesting part about a heat flow map is that it computes
the transfer heat as a result of the difference in the from/to
balances.  So it really shows where currency is posted from a
deep account balance and deposited into a higher one that is
to be made to look good.

The value of a heat flow map is that it can trace back where
things went wrong, and going back it would concentrate around
a limited number of balances.


## Sorting Query Results

The ARPA2 LETS returns balances in an order that shows the most humane
choices first; producers should prefer to earn from the higher balances,
and consumers should prefer to appreciate lower balances.  The same
goes for disbalances, after subtracting one's own balance.  Listings
of balances are therefore ordered by descending and ascending balances,
respectively.

The logic of hot money suggests a different approach.
**TODO:Choice:** Find an approach that makes sense.

