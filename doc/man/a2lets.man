.TH A2LETS 8 "April 2023" "ARPA2.net" "System Administration Commands"
.SH NAME
a2lets \- Local Extrange Trading System with ARPA2 Identity & Access
.SH SYNOPSIS
.PP
.B a2lets.fcgi
.PP
.B a2lets
domain new|del|set domain \fIa2domain\fR
.RS
[ access \fIrule\fR ]
.RE
.PP
.B a2lets
domain get
.RS
[ domain \fIdomain\fR ]
.RE
.PP
.B a2lets
currency new|set|del domain \fIa2domain\fR
.RS
currency \fIcurrency\fR [ format \fIfmtstring\fR ]
.br
[ access \fIrule\fR ]
.RE
.PP
.B a2lets
balance new domain \fIa2domain\fR  currency \fIcurrency\fR
.RS
newuser \fIusername\fR activation \fIactivity\fR
.br
[ access \fIrule\fR ]
.RE
.PP
.B a2lets
balance add domain \fIa2domain\fR  currency \fIcurrency\fR
.RS
( balance \fIbalid | owner \fIusername\fR )
.br
newuser \fIusername\fR
.RE
.PP
.B a2lets
balance set domain \fIa2domain\fR  currency \fIcurrency\fR
.RS
( balance \fIbalid | owner \fIusername\fR )
.br
[ activation \fIactivity\fR ] [ access \fIrule\fR ]
.RE
.PP
.B a2lets
balance get|del domain \fIa2domain\fR  currency \fIcurrency\fR
.RS
( balance \fIbalid | owner \fIusername\fR )
.RE
.PP
.B a2lets
consume new domain \fIa2domain\fR currency \fIcurrency\fR
.RS
( balance \fIbalid\fR | owner \fIusername\fR )
.br
other \fIusername\fR [ request \fIreqid\fR ]
.br
value \fItokens\fR descr \fIcomment\fR
.RE
.PP
.B a2lets
consume add|sub domain \fIa2domain\fR currency \fIcurrency\fR
.RS
( balance \fIbalid\fR | owner \fIusername\fR )
.br
transaction \fItxnid\fR value \fItokens\fR
.RE
.PP
.B a2lets
consume del|get domain \fIa2domain\fR currency \fIcurrency\fR
.RS
( balance \fIbalid\fR | owner \fIusername\fR )
.br
other \fIusername\fR request \fIreqid\fR
.RE
.PP
.B a2lets
produce new|set domain \fIa2domain\fR currency \fIcurrency\fR
.RS
owner \fIusername\fR
.br
[ other \fIusername\fR ] request \fIreqid\fR
.br
value \fItokens\fR descr \fIcomment\fR activation \fIactivity\fR
.RE
.PP
.B a2lets
produce del|get domain \fIa2domain\fR currency \fIcurrency\fR
.RS
owner \fIusername\fR
.br
[ other \fIusername\fR ] [ request \fIreqid\fR ] [ descr \fIdescr-part\fR ]
.RE
.PP
.B a2lets
disbalance get domain \fIa2domain\fR  currency \fIcurrency\fR
.RS
( balance \fIbalid\fR | owner \fIusername\fR )
.br
[ other \fIusername\fR ]
.RE
.SH DESCRIPTION
The
.B a2lets
program generally operates a Local Exchange Trading System, to allow
lively service connections through currencies that reside under a
domain name.  A default currency is XES, the stone or ein Stein, which
may have a different interpretation for domain (like virtual hosting).
.PP
Access to this facility is liberal, and these systems are known to
liberate due to the plentyful nature of currencies.  These systems
are designed to keep currency in local rotation, in this case under
a domain name, so it returns to the people who spend it, provided
that they are active community players.
.PP
Users authenticate with ARPA2 Identity, which means that they can
use Realm Crossover.  This allows them to run an Identity Provider
under a domain of their own choosing, and access a currency in
another that may or may not be related to their domain.  This allows
users from around the Internet to form beneficial communities.
As defined below and in
.IR a2rule (1),
there is a refined system of Access Control which, in the case of
ARPA2 LETS, enables a choice of democratic controls.
.PP
When run as
.BR a2lets.fcgi ,
the command runs as a CGI or FastCGI program.  In this mode, it
processes requests that directly resemble the commandline forms,
with keywords and their values mapped to URL Parameters, and the
class (like
.BR balance )
and action (like
.BR add )
are provided as \fBPATH_INFO\fR that continues after the CGI
program's path, as specified in RFC 3875.
.PP
As an example translation to CGI parameters, consider this commandline:
.PP
.B a2lets balance add domain example.com currency XES user john
.PP
This command is equivalent to a CGI or FastCGI call to
.B a2rule.fcgi
with environment variables provided from a web server:
.PP
.BR PATH_INFO=' /balance/add '
.br
.BR QUERY_STRING=' currency=XES&user=john '
.\" .PP
.\" However, if the "static" web server configuration defines the
.\" currency then that becomes superfluous in the CGI parameters,
.\" which may then be simplified:
.\" .PP
.\" .BR PATH_INFO=' /balance/add '
.\" .br
.\" .BR QUERY_STRING=' user=john '
.\" .br
.\" .BR ARPA2_LETS_CURRENCY=' XES '
.PP
The required and optional parameters depend on the class and action
passed in the \fBPATH_INFO\fR CGI parameter; see below or use
commandline interaction for details.
.PP
.SH "OPERATIONS"
Every operation takes the form of a class and action.  Each is a keyword
that may be cut short when completing them is unambiguous.  The form
shown here is the commandline form, which maps into a \fBPATH_INFO\fR
variable for (Fast)CGI mode.  Some entries below describe more than one
action, separated by a vertical bar.
.TP
.B domain new|set|get|del
Add or remove a domain name that can support currencies.  No currencies
are introduced by \fBnew\fR, but \fBdel\fR will remove any that existed,
including any related history.  Use \fBset\fR to alter the policy \fIrule\fR
as specified below under ACCESS CONTROL, resetting to default if no \fBrule\fR
is supplied.  Use \fBget\fR to retrieve information about one or all domains.
.TP
.B currency new|set|del
Add, modify or remove a currency under a domain name.  Removal of a currency
clears all related history for that currency.  The \fIfmtstring\fR explains
how currency values are to be displayed.  The policy \fIrule\fR is can be
set as specified below under ACCESS CONTROL.
.TP
.B balance new
Creates an unshared balance for a given currency under a domain name
and assigns it to the \fBnewuser\fR.  You must initialise \fBactivation\fR
for the new account, and may change it later with \fBbalance set\fR.
.TP
.B balance add
Shares an existing balance by also assigning it to \fBnewuser\fR.
.TP
.B balance del
Removes \fBowner\fR from sharing the balance.  Even when the last user
is removed, the balance will be retained for historic referencing.
.TP
.B balance set
Update the \fBactivation\fR status of a (shared) balance, affecting all
users who share this balance.  Update the \fBaccess\fR policy \fIrule\fR
as specified below under ACCESS CONTROL.
.TP
.B consume new|add|sub
Record a new consumption; add to or subtract from the value traded before.
Consumption would be called a payment in fiat currency trading, as it lowers
the consumer's balance while it increases the producer's balance.  The later
modifications may be made by a consumer giving an extra reward with \fBadd\fR
or by a producer giving a discount with \fBsub\fR; these may also be used by
an arbiter.  Consumption records cannot be individually removed, but bulk
operations may do so at will, since the balances hold their impact as well.
.TP
.B produce new|set|del|get
Create, update, remove or review an offer by a producer.  Such offers may be
individualised to one \fBother\fR or they may be generic offers.  The provided
\fBreqid\fR may be referenced in a \fBconsume new\fR operation if so desired.
.TP
.B disbalance get
List the balance difference of one \fBother\fR user or all, for a given
domain currency.  There is no mechanism for retrieving absolute balances,
because this is not informative in a LETSystem.
.PP
.SH "PARAMETERS"
Following a class and action, there can be any number of parameters.
The commandline provides interactive clues about which are required and
which are optional for a given combination.  The parameters are provided
as name and value words on the commandline, or they may be standard query
parameters in (Fast)CGI mode.  Name keywords may be cut short when
completing them is unambiguous.
.TP
.B domain
This is the domain name hosting currencies.  Since it is explicit, there can
be any number of domains in a given setup.
.TP
.B access
This sets a policy \fIrule\fR as defined by the
.IR a2rule (1)
program, as specified under ACCESS CONTROL below.
.TP
.B currency
This is the \fIcurrency\fR hosted under a given \fIdomain\fR.  Since it is explicit, there
can be any number of currencies under any domain.  A suggested default currency
is \fBXES\fR, but this is not part of the
.IR a2lets (1)
program.
.TP
.B format
This is a display string for the integer number of tokens in a domain \fIcurrency\fR.
It should contain \fB%d\fR to mark where the number is to be inserted, and should
otherwise contain the currency name or symbol, usually either as a prefix or postfix
to the number, in UTF-8 encoding.
.TP
.B balance
This is a \fIbalid\fR, or balance identity, to indicate the balance on which an
action works.  Usually an alternative to the more intuitive \fBowner\fR parameter.
.TP
.B owner
This is a \fIusername\fR that is supposed to be sharing in a balance.
Usually a more intuitive alternative than specifying the \fBbalance\fR parameter.
.TP
.B other
This is a \fIusername\fR that is supposed to be the other party involved in an action.
.TP
.B activation
This is a balances' activation status, from \fBY\fR for active, \fBN\fR for inactive, or
\fBB\fR for blocked by an administrator.  Balances need to be active for trading, but
they will show up as part of historic information in either state.
Inactive balances are frozen in terms of their distance from the average of all active
balances for the \fIcurrency\fR.  It is polite to inactive a balance when it suspends its
trading for a prolonged period, and make it active again when trading is resumed.  It is
also possible for bulk operational tools to trigger inactive states when no trading is
taking place from a balance.
.TP
.B value
This is the number of \fIcurrency\fR tokens that is used during an action.
.TP
.B descr
This is a description string for human consumption.
.TP
.B request
This is a reference number or \fIreqid\fR, which is usually more suitable automated consumption.
.TP
.B transaction
This number \fItxnid\fR is automatically assigned during \fBconsume new\fR, and may be used
as a unique reference to the created record.
.PP
.SH "ENVIRONMENT VARIABLES"
The proper execution of the programs depends on a number of
environment variables, namely:
.TP
.B QUERY_STRING
Needed in (Fast)CGI mode, with the customary meaning.
.TP
.B HTTP_ACCEPT
Needed in (Fast)CGI mode, with the customary meaning.
.TP
.B REMOTE_USER
Needed in (Fast)CGI mode, ad it must hold a
.IR user @ domain.name
address of an authenticated user.  The normal practice in ARPA2
web authentication is to rely on HTTP-SASL, preferrably with a
backend that supports Realm Crossover, that is, identities
facilitated through foreign servers owned by arbitrary other
Internet domains; this system is designed to reduce user
dependency on a few omnipresent bulk providers.
.TP
.B ARPA2_DOMAIN
Needed in (Fast)CGI mode to hold the domain name to work under.
Note that a domain name is not necessarily the same as a
(virtual) hostname, especially in the HTTP protocol.
The commandline equivalent argument is
\fBdomain\fR \fIa2domain\fR.
.TP
.B ARPA2_LETS_DB
Can be used to override the path of the database file.  If not
provided, the database defaults to \fB/var/lib/arpa2/lets/LETS.db\fR
in normal mode, or to \fB/tmp/LETS.db\fR in DEBUG build mode.
.TP
.B ARPA2_LETS_CURRENCY
Needed in (Fast)CGI mode to hold the ARPA2 LETS Currency to
work under.  It is still possible to provide a \fIcurrency\fR
value, but it must then match this environment variable.
.TP
.B ARPA2_SERVICEKEY_lets
Needed for operations that rely on Access Control, both in
(Fast)CGI mode and on the commandline.  This defines the
Service Key for ARPA2 LETS Access, as may be retrieved from
.IR a2rule (1).
.TP
.B ARPA2_SERVICEKEY_comm
Needed for operations that rely on Access Control, both in
(Fast)CGI mode and on the commandline.  This defines the
Service Key for Communication Access, as may be retrieved from
.IR a2rule (1).
.PP
.SH "ACCESS CONTROL"
There are default Access Rights at all levels, but at each level it is
possible to use the \fBaccess\fR setting with a policy \fIrule\fR that
replaces the default.
.PP
The words in these policies follow the general format of the
.IR a2rule (1)
program, but more specifically Access Rights are set with the
.B %XYZ
format and these apply to the following
.BI ~ user @ domain.name
selector words.  Note the initial character for the two kinds of
words.  When multiple selectors apply, then the most specific
one is set; when multiple selectors apply at the most specific level,
then their Access Rights all apply.
.PP
Access Rights are defined at the levels of a domain, currency and
balance.  The specific letters in the Access Rights have a meaning
that varies between these levels.  Some operations on a level may
be alternatively permitted with a setting on the higher level.
Some domain operations can be overridden by members of the
.BI admin@ domain.name
group.  This is normally resolved by a group query to the
.IR a2rule (1)
RuleDB, but this behaviour may be overridden by granting
.BR %A " or " ACCESS_ADMIN
rights to desired ARPA2 Selectors in a single Policy Rule set in
environment variable
.BR ARPA2_LETS_ADMIN_RULE .
This bootstrapping model protects from remote exploits, but relies
solely on filesystem security for local security.
.PP
While still evolving, the meaning of flags at the various levels of
Access Control are defined in the documentation file
\fIAccessControl.md\fR of the LETSystem.
Future extensions may override the balance with an overriding
setting on a userid.
.PP
.\" Access Control to ARPA2 LETS consists of two forms:
.\" .TP
.\" .B Communication Access
.\" is used to determine whether a user may interact with another
.\" user's balance.  The user names are leading in this process,
.\" and generally the desired status is whitelisting.  Without
.\" this, the \fBdisbalance\fR, \fBconsume\fR and \fBproduce\fR operations
.\" fail.
.\" .TP
.\" .B LETS Access
.\" is used to determine whether a user may manipulate LETS-named
.\" objects, in the forms
.\" .RS
.\" .TP
.\" .BI +lets@ a2domain
.\" for the full currency set under a domain including creating new ones,
.\" .TP
.\" .BI +lets+ currency @ a2domain
.\" for access to a domain-bound currency and creation rights for new balances,
.\" and
.\" .TP
.\" .BI +lets+ currency + balid @ a2domain
.\" for access to an identified balance for a given currecy.
.\" .RE
.\" .PP
.\" The following lists the Access Rights defined for LETS Access.
.\" Names of the form \fB%\fIFLAGS\fR can be used on the
.\" .IR a2rule (1)
.\" commandline, while the form \fBACCESS_\fIRIGHT\fR can be used
.\" in source code of programs,
.\" .PP
.\" .TP
.\" .BR %C " for " ACCESS_CREATE
.\" On a currency, this confers the right to create a new balance.
.\" .br
.\" On the whole LETSystem, this confers the right to create a
.\" new currency.
.\" .TP
.\" .BR %D " for " ACCESS_DELETE
.\" On a balance, this confers the right to delete that balance
.\" if it is zeroed.
.\" .br
.\" On a currency, this confers the right to delete any zeroed
.\" balance.
.\" .br
.\" On the whole LETSystem, this confers the right to delete
.\" any whole currency, without requiring all-zero balances.
.\" .TP
.\" .BR %O " for " ACCESS_OWNER
.\" On a balance, this indicates responsbility for keeping it
.\" functioning, and closing it off properly; it confers the
.\" right to attach user identities to an account, and to
.\" detach them.
.\" .br
.\" On a currency, this indicates ownership; the combination
.\" \fB%DOA\fR confers the right to remove the entire currency,
.\" without requiring all-zero balances.
.\" .TP
.\" .BR %K " for " ACCESS_KNOW
.\" On a balance, this confers the right to see the transactions
.\" in which it participated as a producer or consumer.  This
.\" includes information about hot transactions.
.\" .br
.\" On a currency, this confers the right to see the balances,
.\" including information about hot money.
.\" .TP
.\" .BR %R " for " ACCESS_READ
.\" On a currency, this confers the right to see user identities
.\" attached to its balances.  Without this right, user identities
.\" will be concealed and balances are named in their general
.\" \fB+lets+\fIcurrency\fB+\fIbalance\fB@\fIa2domain\fR form.
.\" .TP
.\" .BR %W " for " ACCESS_WRITE
.\" On a balance, this confers the right to move currency out
.\" of it.  In addition, Communication Access to the recipient
.\" must be whitelisted by the recipient.
.\" .br
.\" On a currency, this confers the right to move funds between
.\" its balances.  This could be used to extract fees (which is not
.\" advised), to distribute an expense (which may be agreeable
.\" if an action was taken on the community's behalf), or to
.\" zero a problematic balance (and compensate it with another)
.\" so it may be deleted.
.\" .TP
.\" .BR %A " for " ACCESS_ADMIN
.\" On a currency, this confers the right to apply the combined
.\" Access Rights to operate on any balance.  This means that
.\" an administrator can obtain an overview, manage accounts on
.\" behalf of users, and even override users.  When it is easy
.\" to create new currencies, as is customary under ARPA2 LETS,
.\" then any such power is subject to democratic control.
.\" .PP
.\" A common setting on a new currency would be \fB%ACDWRKO\fR for
.\" the creator and \fB%DKRWO\fR on any newly created balances.
.\" In addition, creation \fB%C\fR may be granted on any currency
.\" with a Remote Selector in \fBuser\fR that could welcome anyone
.\" \fB@.\fR or just the domain users \fB@\fIa2domain\fR or a
.\" specific group \fIa2group\fB@\fIa2domain\fR.  It is also
.\" advised to freely permit the creation of new currencies,
.\" with \fB%C\fR on \fB+lets@\fIa2domain\fR.
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
For any discussion, including about bugs, please use the
.IR https://gitlab.com/arpa2/arpa2common/
project page for ARPA2 Common.  We use its issue tracker
to track bugs and feature requests.
.PP
Keywords may generally be cut short to a prefix if this does not lead to
ambiguity in their completion; but such ambiguity may change with newer
versions of the software, and are ill-advised across a protocol such as
with (Fast)CGI or operations scripts.  Such requesters should use the
full forms for keywords to be future-proof.
.SH COPYRIGHT
.PP
Copyright \(co 2023 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turn receives
funding from various sources.  Part of this work was funded
through a grant of SIDNfonds, a public interest fund geared
towards a better Internet for all, and paid by administration
fees for .nl domain names.  Other parts were funded by
NLnet, a public interest fund geared towwards open source
excellence, and by the NGI Pointer fund of the European
Union.
.SH "SEE ALSO"
.IR a2rule "(1), " AccessControl.md .
.PP
ARPA2 LETS code can be found on
.IR https://gitlab.com/arpa2/LETS/ .
Many of its concepts continue on the work on
ARPA2 Common, which is documented on
.IR http://common.arpa2.net/
with code on
.IR https://gitlab.com/arpa2/arpa2common/ .
