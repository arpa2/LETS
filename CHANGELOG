# CHANGELOG for LETS

Release v1.2 (2024-06-11)
- Executable a2lets, FastCGI variant and shell scripts

Release v1.1 (2024-06-11)
- Install the a2lets binary ;-)
- Fix to the doc/AccessControl.md document (hide old cruft)

Release v1.0 (2024-06-11)
- Resolve the tension between account privacy and self-regulation
- Definition and Integration of the Hot Money concept
- Integration of ARPA2 Identity & Access Control
- Let go of the ARPA2 Rules DB and instead store Policy Rule strings

Release v0.9 (2023-07-09)
- Core functionality, goal shifts to trial deployment and debugging
- Redesigned the API as one integrated binary with shared common code
- Full-blown parser, CGI compatible with commandline
- Table output in CSV, HTML or (nested-structured) JSON
- Introduce ARPA2 LETS Access Names '+lets[+CUR[+123]]@domain.name'
- For removed usernames, present balance localname as '+lets+CUR+123'
- When a2lets[.fcgi] is built for x86_64 and stripped, it is < 64 kB
- Created an incremental test framework
- Work remaining: Access Control and Heat Detection

Release v0.8 (2023-04-22)
- Fixed confused/wrong roles of local/remote in Communication Access
- Restructured SQL idmap + balance --> balance + usrmap
- Introduced activity and admin in SQL curmap
- FastCGI scripts: Add historydig; rename activity --> activation

Release v0.7 (2023-04-22)
- FastCGI fixed, was failing; also resolved .fcgi <--> .cgi mishaps
- API programs now work in an included Nginx test configuration
- The test configuration involves variables that are too fixed for real use
- Included a rather crude, static webpage for demonstration use

Release v0.6 (2023-04-22)
- Made open source: Affero GPL 3.0 (though shalt export thy service sources)
- Build separate .cgi and .fcgi programs
- Introduced ARPA2 Communication Access Control
- Consistently renamed project to arpa2lets / ARPA2 LETS
- Modified installation structures to reflect naming

Release v0.5 (2023-04-21)
- Added installation scripting and INSTALL.md text
- Added QuickStart.md for playful interaction

Release v0.4 (2023-04-21)
- Rename balance.fcgi --> disbalance.fcgi
- Rename payment.fcgi --> appreciate.fcgi
- Adding activity.fcgi

Release v0.3 (2023-04-21)
- First working version of api/payment.c
- Added CHANGELOG file
- Considering name changes for the API

Release v0.2 (2023-04-20)
- First working version of api/balance.c

Release v0.2 (2023-04-20)
- Checkin to allow initial building in "mkhere"

