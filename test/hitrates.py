#!/usr/bin/env python3
#
# hitrate.py -- Determine the #inserts to reach #bits set.
#
# Assume setting a scattared bit as a result of data insertions.  The
# first is certain to set a bit, the next has a mildly lower chance,
# and so on.  The inverse of this chance is the number of insertions
# until the next bit that flips.  These add up to the number of bits
# that are set at some point.
#
# Reasoning backwards, a number of bits set gives an estimate of the
# number of insertions, and this may count as a width to the number
# of suppliers of hot money into a balance.
#
# Experiments show: This rises very slowly, with a modest sprint at
# the end.  Adding bits mostly increases the slow part, so there is
# not much influence on the total.  We need the opposite...
# 
# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl


bitcount = 256

total = 0.0
for b in range(bitcount):
	total += bitcount / (bitcount - b)
	print ("Bit #%2d has a hitrate of %2d/%2d = %6.3f -- total = %7.3f = %3d" % (b,bitcount,(bitcount-b),bitcount/(bitcount-b), total, int (round (total))))

