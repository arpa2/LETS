# Testplan ACL

While working, edit doc/AccessControl.md

Default test settings (taken from HOWTO.md):

```
export REMOTE_USER=john@example.com
export ARPA2_DOMAIN=example.com
export ARPA2_LETS_DB=/tmp/LETS.db
export ARPA2_LETS_ADMIN_RULE='%OADCF ~john@example.com'
```

These may be overruled with cmdline prefixes for specific tests.
The tests will collect in here, to drive future automated testing.

Errors go to the console with CMake setting `LOG_STYLE=LOG_STYLE_STDERR`.

Tests below are marked with:

- TODO if they are deferred for now
- DONE if they were manually tested (close to the checked-in version)
- AUTO if they have been automated
- FAIL if they currently produce incorrect results


## Origin of Rules

Currently not testing against the Rules DB, only the ARPA2_LETS_ADMIN_RULE envvar and LETS database access rules.


## Domains (DONE)

Handy test utilities:

```
domadd () { ./a2lets domain new domain orvelte.nep ; }
domdel () { ./a2lets domain del domain orvelte.nep ; }
domget () { ./a2lets domain get ; }
```

New domain (cmdline):  **DONE**

- Permitted for users with %A in ARPA2_LETS_ADMIN_RULE

	ARPA2_LETS_ADMIN_RULE='%A ~john@example.com' ./a2lets domain new domain orvelte.nep

- Not permitted with fewer permissions

	ARPA2_LETS_ADMIN_RULE='%BCDEFGHIJKLMNOPQRSTUVWXYZ ~john@example.com' ./a2lets domain new domain orvelte.nep
	ARPA2 Access Denied
	Request Failed

- Not permitted for other users

	REMOTE_USER=mary@example.com ./a2lets domain new domain orvelte.nep
	Request Failed

Remove domain (cmdline):  **DONE**

- Permitted for users with %A or %D in ARPA2_LETS_ADMIN_RULE

	ARPA2_LETS_ADMIN_RULE='%A ~john@example.com' ./a2lets domain del domain orvelte.nep

	ARPA2_LETS_ADMIN_RULE='%D ~john@example.com' ./a2lets domain del domain orvelte.nep

- Not permitted with fewer permissions

	ARPA2_LETS_ADMIN_RULE='%BCEFGHIJKLMNOPQRSTUVWXYZ ~john@example.com' ./a2lets domain del domain orvelte.nep
	ARPA2 Access Denied
	Request Failed

- Not permitted for other users

	REMOTE_USER=mary@example.com ./a2lets domain del domain orvelte.nep
	ARPA2 Access Denied
	Request Failed

Listing domains (cmdline):  **DONE**

- Permitted for users with %A or %R in ARPA2_LETS_ADMIN_RULE

	ARPA2_LETS_ADMIN_RULE='%R ~john@example.com' ./a2lets domain get
	domain,url,access
	"example.com","+lets@example.com",null

	ARPA2_LETS_ADMIN_RULE='%A ~john@example.com' ./a2lets domain get
	domain,url,access
	"example.com","+lets@example.com",null

- Not permitted with fewer permissions

	ARPA2_LETS_ADMIN_RULE='%ABCDEFGHIJKLMNOPQSTUVWXYZ ~john@example.com' ./a2lets domain get
	Request Failed

	ARPA2_LETS_ADMIN_RULE='%BCDEFGHIJKLMNOPQRSTUVWXYZ ~john@example.com' ./a2lets domain get
	Request Failed

- Not permitted for other users

	REMOTE_USER=mary@example.com ./a2lets domain get
	Request Failed

Setting domain properties:  **DONE**

- Permitted for users with %A or %F in ARPA2_LETS_ADMIN_RULE

	ARPA2_LETS_ADMIN_RULE='%A ~john@example.com' ./a2lets domain set domain orvelte.nep

	ARPA2_LETS_ADMIN_RULE='%F ~john@example.com' ./a2lets domain set domain orvelte.nep

- Not permitted with fewer permissions

	ARPA2_LETS_ADMIN_RULE='%BCDEGHIJKLMNOPQRSTUVWXYZ ~john@example.com' ./a2lets domain set domain orvelte.nep access '%F ~mary@example.com'
	ARPA2 Access Denied
	Request Failed

- Not permitted for other users

	REMOTE_USER=mary@example.com ./a2lets domain set domain orvelte.nep access '%F ~mary@example.com'
	ARPA2 Access Denied
	Request Failed

## Currencies

Assumed history (as in utilities below):
	xesdel
	xxxdel

	./a2lets domain set domain example.com access '%ODCF ~john@example.com'

Testing utilities:

```
try() { "$@" > /dev/null 2>&1 ; }

xesget() { ./a2lets currency get domain example.com currency XES ; }
xesadd() { ./a2lets currency new domain example.com currency XES format 'XES %d' access '%ODCXAF ~john@example.com %C ~@example.com' ; }
xesdel() { ./a2lets currency del domain example.com currency XES ; }

xxxget() { ./a2lets currency get domain example.com currency XXX ; }
xxxadd() { ./a2lets currency new domain example.com currency XXX format 'XXX %d' access '%ODCXAF ~mary@orvelte.nep %C ~@orvelte.nep' ; }
xxxdel() { ./a2lets currency del domain example.com currency XXX ; }
```

New currency:  **DONE**

- Based on %A rights in ARPA2_LETS_ADMIN_RULE or %C rights in ARPA2_LETS_ADMIN_RULE or in domxs

	try xesdel ; ARPA2_LETS_ADMIN_RULE='%A ~john@example.com' xesadd

	try xesdel ; ARPA2_LETS_ADMIN_RULE='%C ~john@example.com' xesadd

- Based on %C rights for any userid from example.com under NULL/default access rule

	try xesdel ; ARPA2_LETS_ADMIN_RULE='%Q ~john@example.com' ./a2lets currency new domain example.com currency XES format 'XES %d' access '%ODCXAF ~john@example.com'

- Not based on any other rights in ARPA2_LETS_ADMIN_RULE (for a non-default user, so another domain)

	try xesdel ; ARPA2_LETS_ADMIN_RULE='%BDEFGHIJKLMNOPQRSTUVWXYZ ~other@example.com' REMOTE_USER=other@example.com ./a2lets currency new domain example.com currency XES format 'XES %d' access '%ODCXAF ~john@example.com'
	ARPA2 Access Denied
	Request Faile

Delete currencies:

- Based on %D rights assigned by default to the admin user  **DONE**

	try xesadd ; ./a2lets currency del domain example.com currency XES
	try xxxadd ; REMOTE_USER=mary@orvelte.nep ./a2lets currency del domain example.com currency XXX

- Not for other users of the same domain (who only have %C)  **DONE**

	try xxxadd ; ./a2lets currency del domain example.com currency XXX
	ARPA2 Access Denied
	Request Failed

Show and list currencies (without access control, maybe add it later for the access rule):

- List one particular currency (without access)  **DONE**

	try xesadd; try xxxadd ; ARPA2_LETS_ADMIN_RULE= ./a2lets currency get domain example.com currency XES
	domain,currency,format,url,access
	"example.com","XES","XES %d","+lets+XES@example.com","%ODCXAF ~john@example.com %C ~@example.com"

- List one particular currency (with access)  **DONE**

	try xesadd; try xxxadd ; ARPA2_LETS_ADMIN_RULE='%F ~@.' ./a2lets currency get domain example.com currency XES
	domain,currency,format,url,access
	"example.com","XES","XES %d","+lets+XES@example.com","%ODCXAF ~john@example.com %C ~@example.com"

- List all known currencies (without access)  **DONE**

	try xesadd; try xxxadd ; REMOTE_USER=nobody@somewhere.else ARPA2_LETS_ADMIN_RULE= ./a2lets currency get domain example.com


Change the policy rule for a currency:  **DONE**

- Allowed to the currency %F holder (not for the %A holder, not tested)

	try xesdel ; try xesadd ; ./a2lets currency set domain example.com currency XES format 'XES %d' access '%FAXCDO ~john@example.com'

- Not allowed to others in the domain (only %C)  **DONE**

	try xesdel ; try xesadd ; REMOTE_USER=other@example.com ./a2lets currency set domain example.com currency XES format 'XES %d' access '%FAXCDO ~john@example.com'
	ARPA2 Access Denied
	Request Failed

- Not allowed to others outside the domain (no %F)  **DONE**

	try xesdel ; try xesadd ; REMOTE_USER=mary@orvelte.nep ./a2lets currency set domain example.com currency XES format 'XES %d' access '%FAXCDO ~john@example.com'
	ARPA2 Access Denied
	Request Failed


## Balances

Tools for testing, for owner `john` and alias `john+cook`:

```
johnadd() { ./a2lets balance new domain example.com currency XES newuser john activation Y ; }
maryadd() { ./a2lets balance new domain example.com currency XES newuser mary activation Y ; }
cookadd() { ./a2lets balance add domain example.com currency XES owner   john newuser john+cook ; } 
johndel() { ./a2lets balance del domain example.com currency XES owner john ; }
marydel() { ./a2lets balance del domain example.com currency XES owner mary ; }
cookdel() { ./a2lets balance del domain example.com currency XES owner cook ; }
johnpay() { ./a2lets consume new domain example.com currency XES owner john other mary value 5 descr tx ; }
marypay() { ./a2lets consume new domain example.com currency XES owner mary other john value 5 descr ok ; }
maryact() { ./a2lets balance set domain example.com currency XES owner mary activation "$1" ; }
johnact() { ./a2lets balance set domain example.com currency XES owner john activation "$1" ; }
```

Create a balance (cmdline):  **DONE**

- New user should succeed

	try xesdel ; xesadd ; johnadd

- User can add an alias for himself

	try xesdel ; xesadd ; johnadd ; cookadd

- Alias must reference existing user

	try xesdel ; xesadd ; cookadd
	Request Failed

- Existing username not permitted

	try xesdel ; xesadd ; johnadd ; johnadd
	Request Failed

- Existing alias not permitted

	try xesdel ; xesadd ; johnadd ; cookadd ; cookadd
	Request Failed

- User cannot add an alias for someone else

	try xesdel ; xesadd ; REMOTE_USER=mary@orvelte.nep johnadd
	ARPA2 Access Denied
	Request Failed

Destroy a balance (cmdline):

- Non-existing user should fail  **DONE**

	try xesdel ; xesadd ; johndel
	Request Failed

- Currency admin should always succeed, even on non-zero balance

	try xesdel ; xesadd ; johnadd ; maryadd ; johndel

	try xesdel ; xesadd ; johnadd ; maryadd ; marydel

	try xesdel ; xesadd ; johnadd ; maryadd ; johnpay ; johndel

	try xesdel ; xesadd ; johnadd ; maryadd ; johnpay ; marydel

- User can destroy their own zero balance:  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com marydel

- User cannot destroy another's balance:  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com johndel
	ARPA2 Access Denied
	Request Failed

- Admin can destroy another's zero balance:  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; marydel

- User cannot destroy their own non-zero balance  **DONE**
  **TODO:** Now "non-zero" is "negative", so it'd call for marypay and not johnpay

	try xesdel ; xesadd ; johnadd ; maryadd ; johnpay ; REMOTE_USER=mary@example.com marydel
	Request Failed

- User can destroy their corrected-to-zero balance  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; johnpay ; REMOTE_USER=mary@example.com marypay ; REMOTE_USER=mary@example.com marydel



Block / Unblock a balance (cmdline):  **DONE**

- Currency admin should succeed  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; maryact B

	try xesdel ; xesadd ; johnadd ; maryadd ; maryact B ; maryact U

- Non-admin should fail on themselves  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com maryact B
	ARPA2 Access Denied
	Request Failed

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com maryact U
	ARPA2 Access Denied
	Request Failed

- Non-admin should fail on others  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com johnact B
	ARPA2 Access Denied
	Request Failed

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com johnact U
	ARPA2 Access Denied
	Request Failed

- Maintain average balance as ???  **MAYBE:NOTYET**

- User change of their Y/N state exception: **MAYBE:NOTYET** if no other balance exists

- User can change their balance's Y/N state  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com maryact N
	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com maryact N ; REMOTE_USER=mary@example.com maryact Y
	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com maryact N ; REMOTE_USER=mary@example.com maryact Y ; REMOTE_USER=mary@example.com maryact N

- Admin cannot change a balance's Y/N state directly  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; maryact N
	ARPA2 Access Denied
	Request Failed

	try xesdel ; xesadd ; johnadd ; maryadd ; maryact Y
	ARPA2 Access Denied
	Request Failed

- User cannot change other's balance's Y/N state  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com johnact N
	ARPA2 Access Denied
	Request Failed

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com johnact Y
	ARPA2 Access Denied
	Request Failed


## Balance Alias

Create a userid for a balance:

- User can create a balance alias for themselves  **DONE**

	try xesdel ; xesadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets balance add domain example.com currency XES owner mary newuser maria

- Admin cannot create a balance alias for others  **MAYBE:NOTYET**

Destroy a userid for a balance:

- User can destory a non-last balance  **DONE**

	try xesdel ; xesadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets balance add domain example.com currency XES owner mary newuser maria ; REMOTE_USER=mary@example.com ./a2lets balance del domain example.com currency XES owner maria

	try xesdel ; xesadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets balance add domain example.com currency XES owner mary newuser maria ; REMOTE_USER=mary@example.com ./a2lets balance del domain example.com currency XES owner mary

	try xesdel ; xesadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets balance add domain example.com currency XES owner mary newuser maria ; REMOTE_USER=maria@example.com ./a2lets balance del domain example.com currency XES owner maria

	try xesdel ; xesadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets balance add domain example.com currency XES owner mary newuser maria ; REMOTE_USER=maria@example.com ./a2lets balance del domain example.com currency XES owner mary

- User can destroy their last at-least-neutral balance  **DONE**

	try xesdel ; xesadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets balance del domain example.com currency XES owner mary

	try xesdel ; xesadd ; johnadd ; maryadd ; ./a2lets consume new domain example.com currency XES owner john other mary value 10 descr Ten ; REMOTE_USER=mary@example.com ./a2lets balance del domain example.com currency XES owner mary

	try xesdel ; xesadd ; johnadd; maryadd ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other john value 10 descr Ten ; ./a2lets consume new domain example.com currency XES owner john other mary value 10 descr Ten ; REMOTE_USER=mary@example.com ./a2lets balance del domain example.com currency XES owner mary

- User can remove all but the last alias when balance is not at-least-neutral  **MAYBE:NOTYET**

  A prickly issue is the relation between aliases (possibly group members).
  Does the last one leaving the table have to pay the group's drinking tab?

- User cannot destroy their last below-neutral balance  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other john value 10 descr Ten ; REMOTE_USER=mary@example.com ./a2lets balance del domain example.com currency XES owner mary
	Request Failed

- Admin can destroy a balance alias for others (and perhaps the entire baalnce)  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other john value 10 descr Ten ; ./a2lets balance del domain example.com currency XES owner mary

- Admin can completely destroy a balance of others, with all aliases  **MAYBE:NOTYET**

  Current implementation is one usrid at a time, with garbage collection of the balance after the last.


## Payments

Create a payment (web):

- Account owners can pay to any [other] userid that is registered against a balance (other or same)  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other john value 10 descr Ten

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other mary value 10 descr Ten

- Non-account-owners cannot make payments from an account

	try xesdel ; xesadd ; johnadd ; maryadd ; ./a2lets consume new domain example.com currency XES owner mary other john value 10 descr Ten
	ARPA2 Access Denied
	Request Failed

- Inactive and blocked balances cannot make payments  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com maryact N ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other john value 10 descr Ten
	Request Failed

	try xesdel ; xesadd ; johnadd ; maryadd ; maryact B ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other john value 10 descr Ten
	Request Failed

- Inactive and blocked balances cannot take payments  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com maryact N ; ./a2lets consume new domain example.com currency XES owner john other mary value 10 descr Ten
	Request Failed

	try xesdel ; xesadd ; johnadd ; maryadd ; maryact B ; ./a2lets consume new domain example.com currency XES owner john other mary value 10 descr Ten
	Request Failed

- Distinction between userid of a balance  **MAYBE:NOTYET**
  **TODO:** Show userids instead of test-varied numerics
  **TODO:** Other way around

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets balance add domain example.com currency XES owner mary newuser maria ; REMOTE_USER=maria@example.com ./a2lets consume new domain example.com currency XES owner maria other john value 10 descr Ten ; sqlite3 "$ARPA2_LETS_DB" 'select * from history'
	1|2|2|1|394|392|10|1714144236|Ten|

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets balance add domain example.com currency XES owner mary newuser maria ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other john value 10 descr Ten ; sqlite3 "$APRA2_LETS_DB" 'select * from history'
	1|2|2|1|405|404|10|1714144292|Ten|


Update a payment (web):

- Payee can lower the amount  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other john value 10 descr Ten ; TXNID=$(sqlite3 /tmp/LETS.db "select txnid from history where descr='Ten'") ; ./a2lets consume sub domain example.com currency XES owner john transaction $TXNID value 5

- Payee cannot lower the amount below 0 **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other john value 10 descr Ten ; TXNID=$(sqlite3 /tmp/LETS.db "select txnid from history where descr='Ten'") ; ./a2lets consume sub domain example.com currency XES owner john transaction $TXNID value 11
	Request Failed

- Payee cannot increase the amount  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other john value 10 descr Ten ; TXNID=$(sqlite3 /tmp/LETS.db "select txnid from history where descr='Ten'") ; ./a2lets consume add domain example.com currency XES owner john transaction $TXNID value 5
	Request Failed

- Payer can increase the amount  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other john value 10 descr Ten ; TXNID=$(sqlite3 /tmp/LETS.db "select txnid from history where descr='Ten'") REMOTE_USER=mary@example.com ./a2lets consume add domain example.com currency XES owner mary transaction $TXNID value 5

- Payer cannot lower the amount  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other john value 10 descr Ten ; TXNID=$(sqlite3 /tmp/LETS.db "select txnid from history where descr='Ten'") REMOTE_USER=mary@example.com ./a2lets consume sub domain example.com currency XES owner mary transaction $TXNID value 5
	Request Failed

- Payee can revert the payment ???  **MAYBE:NOTYET**

- Payer cannot revert the payment  **MAYBE:NOTYET**

## Offers

Create an offer:

- Anyone can create a generic offer paying out to themselves  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=john@example.com ./a2lets produce new domain example.com currency XES owner john request 123 value 45 descr ForAll offer Y

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=john@example.com ./a2lets produce new domain example.com currency XES owner john request 123 value 45 descr ForAll offer N

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets produce new domain example.com currency XES owner mary request 123 value 45 descr ForAll offer Y

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets produce new domain example.com currency XES owner mary request 123 value 45 descr ForAll offer N

- Nobody can create a generic offer paying out to another  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=john@example.com ./a2lets produce new domain example.com currency XES owner mary request 123 value 45 descr ForAll offer Y
	ARPA2 Access Denied
	Request Failed

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=john@example.com ./a2lets produce new domain example.com currency XES owner mary request 123 value 45 descr ForAll offer N
	ARPA2 Access Denied
	Request Failed

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets produce new domain example.com currency XES owner john request 123 value 45 descr ForAll offer Y
	ARPA2 Access Denied
	Request Failed

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets produce new domain example.com currency XES owner john request 123 value 45 descr ForAll offer N
	ARPA2 Access Denied
	Request Failed

- Anyone can create a user-specific offer paying out to themselves  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=john@example.com ./a2lets produce new domain example.com currency XES owner john other mary request 123 value 45 descr ForYou offer Y

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=john@example.com ./a2lets produce new domain example.com currency XES owner john other mary request 123 value 45 descr ForYou offer N

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets produce new domain example.com currency XES owner mary other john request 123 value 45 descr ForYou offer Y

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets produce new domain example.com currency XES owner mary other john request 123 value 45 descr ForYou offer N

- Nobody can create a user-specific offer paying out to another  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=john@example.com ./a2lets produce new domain example.com currency XES owner mary other john request 123 value 45 descr ForYou offer Y
	ARPA2 Access Denied
	Request Failed

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=john@example.com ./a2lets produce new domain example.com currency XES owner mary other john request 123 value 45 descr ForYou offer N
	ARPA2 Access Denied
	Request Failed

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets produce new domain example.com currency XES owner john other mary request 123 value 45 descr ForYou offer Y
	ARPA2 Access Denied
	Request Failed

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=mary@example.com ./a2lets produce new domain example.com currency XES owner john other mary request 123 value 45 descr ForYou offer N
	ARPA2 Access Denied
	Request Failed

- Anyone can pay in response to an    active generic offer

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=john@example.com ./a2lets produce new domain example.com currency XES owner john request 123 value 45 descr ForAll offer Y ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other john request 123 value 45 descr ForAllSoForMe

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=john@example.com ./a2lets produce new domain example.com currency XES owner john request 123 value 45 descr ForAll offer Y ; REMOTE_USER=john@example.com ./a2lets consume new domain example.com currency XES owner john other john request 123 value 45 descr ForAllSoForMyself

- The targeted user  can    pay in response to an    active user-specific offer  **DONE**

	try xesdel ; xesadd ; johnadd ; maryadd ; REMOTE_USER=john@example.com ./a2lets produce new domain example.com currency XES owner john other mary request 123 value 45 descr ForYou offer Y ; REMOTE_USER=mary@example.com ./a2lets consume new domain example.com currency XES owner mary other john request 123 value 45 descr ForYouAlone

- Non-targeted users cannot pay in response to an    active user-specific offer  **NOTYET:MAYBE**
- Nobody can pay in response to an  inactive generic offer  **NOTYET:MAYBE**
- Nobody can pay in response to an  inactive generic offer  **NOTYET:MAYBE**
- The targeted user  cannot pay in response to an  inactive user-specific offer  **NOTYET:MAYBE**
- Non-targeted users cannot pay in response to an  inactive user-specific offer  **NOTYET:MAYBE**
- Nobody can pay in response to a    blocked generic offer  **NOTYET:MAYBE**
- Nobody can pay in response to a    blocked generic offer  **NOTYET:MAYBE**
- The targeted user  cannot pay in response to a    blocked user-specific offer  **NOTYET:MAYBE**
- Non-targeted users cannot pay in response to a    blocked user-specific offer  **NOTYET:MAYBE**

The ones marked **NOTYET:MAYBE** in the above are not implemented, because the idea is that only active offers (set to Y) should be shown.  These would be suggestively pre-filled form fields based on the offer, which is why the `a2lets consume` commands above neither constrain nor abbreviate with data from an offer.  Rules do apply to owner access to toggle between Y/N and admin access to toggle an offer with B/U.  See below!

- Anyone with access to a balance can list offers that are not targeted at other users:  **DONE**
  **NOTYET:MAYBE** Currently rather broad (possible lack of privacy)
                   - Note that blocked balances cannnot take in payments, maybe not list those either?
                   - The owner lists the consumer who asks for the data

	./a2lets produce get domain example.com currency XES owner john


Edit an offer:

- The owner can edit an offer  **NOTYET:MAYBE**
- The owner can remove an offer  **NOTYET:MAYBE**
- The owner can activate Y or deactivate N an offer  **NOTYET:MAYBE**
- The admin can block B or unblock U an offer  **NOTYET:MAYBE**

