# Testing a2lets

Normal runtime environments supply environment variables, through an envvar
when the FastCGI program is started or as part of the environment passed in
over FastCGI.  This is a different setup from testing.

To test `a2lets` from a commandline test, a few things must be arranged.
This is also done in the `RunTest.cmake` script but explained here.

  * `ARPA2_LETS_DB` must be set to the SQLite3 database to work on.
  * `REMOTE_USER` must be set to the (presumably authenticated) user identity.
  * `ARPA2_LETS_ADMIN_RULE` must be set to an Access Rule
    that assigns rights to remote users, notably `%ARW` for the
    ability to be system admin, the read admin info and to write admin info.
  * `ARPA2_DOMAIN` may optionally be supplied instead of the `domain`
    parameter on the commandline or as a HTTP query parameter.

If these things are not set properly, you should find helpful hints in the
logfiles, so be sure to run `syslogd` and read the logs.  Alternatively, setup
the ARPA2 exception system `<arpa2/except.h>` such that it prints on `stderr`,
by choosing `LOG_STYLE=LOG_STYLE_STDERR` in the CMake settings.

## Example Settings

These usually work nicely for commandline testing:

```
export REMOTE_USER=john@example.com
export ARPA2_DOMAIN=example.com
export ARPA2_LETS_DB=/tmp/LETS.db
export ARPA2_LETS_ADMIN_RULE='%ARWD ~john@example.com'
```
