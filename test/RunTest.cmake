# Run a single test of the ARPA2 LETSystem
#
#   SPDX-License-Identifier: AGPL-3.0-or-later
#   SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl


# The list of table names that may be tested (after sorting the lines).
#
set (A2LETS_TABLES dommap curmap usrmap balance history request)


# Debug message about what test is being run
#
message (DEBUG "Running test ${A2LETS_TEST} to follow after ${A2LETS_PREV}...")


# Compare tables as part of a test.  Contents are sorted and must than be equal.
#
function (testcmp TEST PREV)
	set (_FAIL FALSE)
	#
	# Settings
	set (_DB "${BINDIR}/${TEST}.db")
	#
	# Create table files (which can be copied to update the tests)
	foreach (_TABLE IN LISTS A2LETS_TABLES)
		execute_process (
			COMMAND sqlite3 "${_DB}" "UPDATE history SET unixtime=1688920000+txnid*10")
		execute_process (
			COMMAND sqlite3 "${_DB}" ".dump --data-only ${_TABLE}"
			OUTPUT_FILE "${BINDIR}/${TEST}-${_TABLE}"
			RESULTS_VARIABLE _RES)
		if (NOT _RES EQUAL 0)
			message (FATAL_ERROR "Failed to dump test/${TEST}-${_TABLE}")
		endif ()
	endforeach (_TABLE)
	#
	# Perform the tests if source versions exist
	foreach (_TABLE IN LISTS A2LETS_TABLES)
		if (EXISTS "${SRCDIR}/${TEST}-${_TABLE}")
			set (_MSG "Table differs")
			set (_CMP "${SRCDIR}/${TEST}-${_TABLE}")
		elseif (EXISTS "${BINDIR}/${PREV}-${_TABLE}")
			set (_MSG "Table changed")
			set (_CMP "${BINDIR}/${PREV}-${_TABLE}")
		else ()
			set (_MSG "Table ignored")
			set (_CMP "/dev/null")
		endif ()
		file (STRINGS "${BINDIR}/${TEST}-${_TABLE}" _IST )
		file (STRINGS "${_CMP}"                                       _SOLL)
		list (SORT _IST )
		list (SORT _SOLL)
message (STATUS "_IST:  ${_IST}" )
message (STATUS "_SOLL: ${_SOLL}")
		if (NOT "${_IST}" STREQUAL "${_SOLL}")
			execute_process (COMMAND echo "${_MSG}: test/${TEST}-${_TABLE}")
			set (_FAIL TRUE)
		endif ()
	endforeach (_TABLE)
	#
	# Possibly break off comparison
	if (_FAIL)
		message (FATAL_ERROR "Failure in test ${TEST}")
	endif (_FAIL)
endfunction ()


# Copy the previous database as a basis for the new one
#
execute_process (COMMAND "${CMAKE_COMMAND}" -E copy "${A2LETS_PREV}.db" "${A2LETS_TEST}.db")


# Run the SQLite3 command for each of the a2lets(1) word sequences
#
math(EXPR ARGC ${CMAKE_ARGC}-1)
foreach(ARGI RANGE 15 ${ARGC})
	# Run each of the _ARG while expanding them as CMake lists
	#
	separate_arguments (_ARG UNIX_COMMAND "${CMAKE_ARGV${ARGI}}")
	execute_process (
		COMMAND cmake -E env
			"ARPA2_LETS_DB=${A2LETS_TEST}.db"
			"REMOTE_USER=${REMOTE_USER}"
			"ARPA2_LETS_ADMIN_RULE=${ARPA2_LETS_ADMIN_RULE}"
				../a2lets ${_ARG}
		RESULTS_VARIABLE _RES)
	if (NOT _RES EQUAL 0)
		message (FATAL_ERROR "exit(${_RES}) from a2lets ${_ARG}")
	endif ()
endforeach()

# Test the tables to match the expectations
#
testcmp ("${A2LETS_TEST}" "${A2LETS_PREV}")

#
# We will return successfully if we got this far
