# QuickStart for ARPA2 LETS

> *Impatient?*

The software builds a commandline program `a2lets` that gives helpful
information on command forms; manual page `a2lets(1)` provides a lot
more detail.  The program aliases to `a2lets.fcgi` for use in FastCGI
or CGI-BIN mode, in direct relation to the command forms.

**Build the software.**
To build after installing dependencies:

```
git clone https://gitlab.com/arpa2/LETS LETS.src
mkdir LETS.build
cd LETS.build
cmake ../LETS.src
make
make install
```

**Setup a database.**
Create a SQLite3 database:

```
sqlite3 -init initdb.sql /var/lib/arpa2/lets/LETS.db
```

**Setup a domain and users.**
Simply `john` and `mary` at `example.com`
using the default "stone" currency, written
as XES:

```
a2lets domain new \
              domain example.com

a2lets currency new \
                domain example.com \
                currency XES

a2lets balance new \
               domain example.com currency XES \
               newuser john activation Y

a2lets balance new \
               domain example.com currency XES \
               newuser mary activation Y
```

**Environment variables.**
Setup the following values:

```
export REMOTE_USER=john@example.com
export ARPA2_DOMAINKEY_example_com=6ba987dcebac98b0f2677f5f0a4b2b588ec7cca6207e7c00a2744034ba9df255
export ARPA2_RULES_DIR=/tmp
```

The command examples below specify `domain` and `currency` parameters,
but these can be dropped when the environment defines variables
`ARPA2_DOMAIN=example.com` and/or `ARPA2_LETS_CURRENCY=XES`.
It is an error to specify both forms of a parameter, so as to avoid any
conflict between values.  This is especially useful in the FastCGI
interface, where the environment variables may be used for fixation.

**Access Rights.**
Setup the RulesDB for ARPA2 Communcation Access Control,
pressing enter for the database key:

```
a2rule comm set remote john@example.com local mary@example.com list white
```

## Playing Around

All set?  Then you're ready to go!

From now on, you can easily review the database contents
from another terminal:

```
sqlite3 /var/lib/arpa2/lets/LETS.db
sqlite> .dump
...
sqlite> .dump
...
sqlite> .quit
```

**Show a disbalance.**
To dump Mary's balance relative to your own:

```
a2lets disbalance get \
                  domain example.com currency XES \
                  owner john other mary
```

**Dig up history.**
To see historic transactions consumer role:

```
a2lets consume get \
                  domain example.com currency XES \
                  owner john other mary
```

**Consumption.**
In fiat money terms, this would be a payment, but in ARPA2 LETS we
represent this as administration of consumption:

```
a2lets consume new \
               domain example.com currency XES \
               owner john other mary \
               value 16 descr 'Raspberry Pie'
```

You (as John) might be surprised if you show the disbalance again.  Since
your balance went down while Mary's went up, you would see twice the
`value 16` difference.  But the history will show just that this results
from just one transaction.

Had you seen your balance, as you can do on the SQLite3 commandline,
you might fret over a negative balance.  But you should not, and that is
why disbalance between accounts is shown, rather than an absolute balance.
All balances add up to zero, so some are bound to go up while others go down.

**Inactivity.**
You might want to become inactive.  During that time, the disbalance
between you and others may shift, and you might want to conserve that
on average by going into inactive mode (with `N`) and reactivating
(with `Y`); as a special case, administrators may block your account
(with `B`) which enforces inactivity of your balance.

```
a2lets balance set \
               domain example.com currency XES \
               owner john \
               activation N
```


## Access Control

The ARPA2 Access Control system is only skimmed here, read more about
the general setup in `a2rule(1)` and details about access control for
management in `a2lets(1)`.   The basic ability to exchange payments
inherits general Communucation Access settings, which may be toggled
between `black` listing and `white` listing with commands like

```
a2rule comm set \
            remote john@example.com \
            local  mary@example.com \
            list black
```

You should find that you cannot run `a2lets consume`, `a2lets produce`
or `a2lets disbalance` commands as John towards Mary while she
has you on her black list.  As explained in the
[README](README.md),
we consider money as a form of communication, which explains why we
share the settings for Communication Access.


## Nginx

A rather crude setup for Nginx is included.  Do not run it on a live
webserver.  To start Nginx and the FastCGI components, run this from
the build directory:

```
./nginx/restart.ash
```

You can now access the Nginx server at http://[::1]:8088/lets and
add `/class/action` choices as well as `?param=value&param=value`
syntax to do the same things as using `a2lets` on the commandline.

The demonstration interface sets the domain to `example.com`, the
currency to `XES` and the authenticated user to `john@example.com`.
But since this user may be served under multiple aliases, it is
still required to provide balance selection parameters such as
`owner=john` or `balance=123`.

In (Fast)CGI mode, when debugging, any output for `stderr` appends
to `/tmp/a2lets.log` and the database opened defaults to
`/var/lib/arpa2/lets/LETS.db` if you did not override the `LETS_DB_PATH`
configuration setting for CMake and if you did not choose `DEBUG` mode,
in which case `/tmp/LETS.db` is the default path.

The `restart.ash` script does not restart Nginx every time, so
configuration file changes may not arrive properly.  Various settings
are currently made in the test configuration, which makes this a kludge,
but it is fit for play.


## Currency

Currencies are domain-local.  We installed `XES` by default, as a
homage to Albert Einstein and because of the relative nature of
the disbalance (our concept, built on top of other LETS splendour).

Like `XAU` is gold Au and `XAG` is silver Ag, we touted `XES` for
the element Einsteinium Es.  We pronounce a unit "ein Stein" in
German, "een steen" in Dutch, or "a stone" in English.  As a
postfix currency, the uses could be "Steine", "stenen" and
"stones".  Have fun making demonstration kits with that :-)


## Copyright Notice

```
SPDX-License-Identifier: AGPL-3.0-or-later
SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
```
