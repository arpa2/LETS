# Installation of ARPA2 LETS

> *Instructions to make this Local Exchange Trading System work on POSIX systems.*

## Building the software

Our software has **build-time dependencies**, namely:

  * `ash` is used for container-friendly shell scripts
  * `libsqlite3` and `-dev` is used for data storage
  * `libfcgi` and `-dev` is used to welcome connections from a web server, possibly running in another container
  * `com_err` is used to enhance error codes with custom values
  * [`ARPA2CM`](https://gitlab.com/arpa2/arpa2cm) is used as ARPA2 build collection
  * [`ARPA2 Common`](https://gitlab.com/arpa2/arpa2cm) is used for commandline and CGI request parsing, as well as for Identity and Access Control

Our software **builds with CMake**, using:

```
git clone https://gitlab.com/arpa2/LETS LETS.src
mkdir LETS.build
cd    LETS.build
cmake ../LETS.src
make
make install
```

The program `a2lets` is a shell command, and it doubles as a
FastCGI service when called with its `a2lets.fcgi` name.
When the latter is not called from a FastCGI context, it will
behave as a standard CGI-BIN program.


## Installation Components

The files with `.md` endings are documentation.
They will be installed in the `PREFIX/share/doc/arpa2lets` directory.

The files with `.ash` endings are shell scripts (for `ash`).
They will be installed in the `PREFIX/sbin` directory.

The files with `.fcgi` endings are FastCGI programs.
They will be installed in the `PREFIX/libexec/arpa2lets/` directory.

The files with `.sql` endings are SQL scripts.
They will be installed in the `PREFIX/libexec/arpa2lets/` directory.


## Installation under Nginx

The FastCGI program should be connected from Nginx, in a place
where HTTP client authentication takes place.  We recommend doing it
with HTTP-SASL, ideally with a backend supporting Realm Crossover.
This allows login with a foreign identity, as long as it maps to a
local Actor Identity such as a Group Member.

A demonstration setup has been included under
[`contrib/nginx`](contrib/nginx/arpa2lets.conf)
mostly for testing and demo; it runs a partial
website under `/lets`, and that would also be a
good place for live setups.  *This demonstration
code has not been scrutinised for security.*


## Installation under Apache

The FastCGI programs should be connected from Apache, in a place
where client authentication takes place.  We recommend doing it
with HTTP-SASL, preferrably with Realm Crossover.  This allows
login with a foreign identity, as long as it maps to a local
Actor Identity, such as a Group Member.


## Copyright Notice

```
SPDX-License-Identifier: AGPL-3.0-or-later
SPDX-FileCopyrightText: Copyright 2023, Rick van Rein, OpenFortress.nl
```
